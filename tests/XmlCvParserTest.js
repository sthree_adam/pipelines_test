var XmlCvParser		= require( '../lib/XmlCvParser' ),
	should 			= require( 'should' ),
	assert			= require( 'assert' ),
	sinon			= require( 'sinon' ),
	fs				= require( 'fs' );


suite( 'Unit Test for XmlCvParser', function() {

	var fakes,
		oConf = {
		get: function() {}
	};

	setup( function() {
		fakes = sinon.sandbox.create();
		XmlCvParser.setCurrentTime();
	} );

	teardown( function() { 
		fakes.restore();
		XmlCvParser.setCurrentTime();
	} );

	suite( 'getParsedCv', function( ) {

		var isValid = function( oParsed, oExpected, oError ) {
			should.not.exist( oError );

			assert.equal(oParsed.CareerStartDate, oExpected.CareerStartDate );
	    	assert.equal(oParsed.CurrentEmployer, oExpected.CurrentEmployer );
	    	assert.deepEqual(oParsed.RawEmployers, oExpected.Employers );
	    	assert.deepEqual(oParsed.Employers, oExpected.RawEmployers );
	    	assert.deepEqual(oParsed.JobTitles, oExpected.JobTitles );
	    	assert.equal(oParsed.CurrentJobTitle, oExpected.CurrentJobTitle );
	    	assert.deepEqual(oParsed.CurrentSkills, oExpected.CurrentSkills );
	    	assert.deepEqual(oParsed.Skills, oExpected.Skills );
	    	assert.deepEqual(oParsed.cv.replace( /\r/g, '' ), oExpected.cv.replace( /\r/g, '' ) );
		}

		test( 'Success - valid sovren cv', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2010/12/10' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/6.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/6.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - UTF', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2010/12/10' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/4.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/4.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - no current jobs', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2015/12/10' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/6.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/6_no_current_jobs.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - no jobs', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2010/12/10' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/0.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/0.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - ended with AnyDate tag', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2015/12/10' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/11.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/11.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - ended but in range', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2006/01/01' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/7.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/7.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - with two jobs having same start and end date', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '2014/04/11' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/12.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/12.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - no employment history', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date().getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/13.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/13.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );
		
		test( 'Success - valid sovren cv - duplicated start dates', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date().getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/15.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/15.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - without positions', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '01/08/2013' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/16.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/16.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Success - valid sovren cv - date in the future', function( done ) {
			var oConfMock = fakes.mock( oConf );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_skills' ).returns( 2 );
		    oConfMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 4 );
		    
		    var now = new Date( '01/08/2013' ).getTime();
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/14.xml' );
		    XmlCvParser.setCurrentTime( now );

		    var oExpected = require( __dirname + '/xml_cvs/expected/14.json' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	isValid( oParsed, oExpected, oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Failure - burning glass cv ', function( done ) {		    
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/5_burning_glass.xml' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	assert.equal( 'Wrong xml cv type!', oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

		test( 'Failure - broken sovren cv ', function( done ) {		    
		    var cv = fs.readFileSync( __dirname + '/xml_cvs/sovren/4_broken.xml' );

		    XmlCvParser.getParsedCv( cv, oConf, function( oParsed, oError ) {

		    	assert.equal( 'Broken xml!', oError );

		    	fakes.verify();
		    	done();
		    } );

		} );

	} );

} );