var fs = require( 'fs' );

if ( !fs.existsSync( __dirname + '/junit.xml') ) {
	process.exit(1);
}

var content = fs.readFileSync( __dirname + '/junit.xml', 'utf8' );

console.log( "---junit.xml---" );
console.log( new Buffer( content ).toString('base64') );
console.log( "---/junit.xml---" );

setTimeout( function() {
	console.log( "testing finished" );
}, 2000 );
