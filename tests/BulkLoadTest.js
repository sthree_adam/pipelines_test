var BulkLoad = require( '../lib/BulkLoad' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );

suite( 'Unit Test for BulkLoad class', function() {

	var fakes;
    var oArgumentParser;

	setup( function() {
		fakes 		= sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
		fakes.restore();
	} );
    

	suite( 'getExistingJobBatchId', function( ) {		
		test( 'success', function( ) {
			var oBulkLoad = new BulkLoad( oArgumentParser );
            fakes.mock( oArgumentParser )
                .expects( 'getArgumentValue' )
                .once()
                .withArgs( 'stepBatchArgumentName' )
                .returns( 'jobId,batchId' );
            
			assert.deepEqual(
                  oBulkLoad.getExistingJobBatchId( 'stepBatchArgumentName' )
                , ['jobId', 'batchId']
            );
            fakes.verify();
		} );

        test( 'success if no value for batch argument name', function( ) {
            var oBulkLoad = new BulkLoad( oArgumentParser );
            fakes.mock( oArgumentParser )
                .expects( 'getArgumentValue' )
                .once()
                .withArgs( 'stepBatchArgumentName' )
                .returns( null );

            assert.deepEqual(
                oBulkLoad.getExistingJobBatchId( 'stepBatchArgumentName' )
                , null
            );
            fakes.verify();
        } );
        
        test( 'throws exception if the argument value is not in the right format', function( ) {
            var oBulkLoad = new BulkLoad( oArgumentParser );
            fakes.mock( oArgumentParser )
                .expects( 'getArgumentValue' )
                .once()
                .withArgs( 'stepBatchArgumentName' )
                .returns( 'jobIdbatchId' );

            assert.throws(
                function(){oBulkLoad.getExistingJobBatchId( 'stepBatchArgumentName' )},
                Error
            );
            fakes.verify();
        } );
        
	} );

} );