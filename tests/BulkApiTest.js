var BulkApi	= require( '../lib/BulkApi' );
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var Queue           = require( 'promise-queue' );

suite( 'Unit Test for BulkApi class', function() {

	var fakes;
	var oLogger;
	var oConf;
	var oQueryConfig;

	setup( function() {
		fakes = sinon.sandbox.create();
		oLogger = { info: fakes.spy(), log: fakes.spy(), error: fakes.spy() };
		oConf = { get:function(sConf){
                switch(sConf) {
                    case 'salesforce': return {
                                                    tmpdir: "/tmp/",
                                                    bulkapi: {
                                                        pollfrequency:3,
                                                        timeout:5
                                                }};
                    case 'lookinglass': return {};                                                
                    default: throw new Error('Conf not mocked for ' + sConf);
                }
            }
        }
		oQueryConfig = { object: 'object', query: 'query', onRecord: function(){} };
	} );

	teardown( function() { 
		fakes.restore();
	} );

    var emptyBulkLoadMock = function (bVerifyFakes, fDone) {
        var oBulkLoadConfig = {init: function () {}, GetSteps: function () {}, getExistingJobBatchId: function () {return null;}, AllLoaded: function () {}};
        
        oBulkLoadConfig.AllLoaded = function () {
            if ( bVerifyFakes ) {
                fakes.verify();
                fDone();
            }
        }
        return oBulkLoadConfig;
    };

	suite( 'process', function( ) {
		test( 'Success Adds to queue one promise per each step plus an extra one at the end', function( done ) {
			var oQueue = new Queue();
    
			var aSteps = [
				  { source: 'salesforce' }
				, { source: 'custom' }
				, { source: 'salesforce' }
				, { source: 'custom' }
				, { source: 'mongo' }
				, { source: 'custom' }
				, { source: 'rabbitmongo' }
			];
    
			var oQueueMock = fakes.mock( oQueue );
			oQueueMock.expects('add').exactly(8).yields();
    
			var oBulkApi = new BulkApi( oConf, oLogger, oQueue);
		
			var oMockBulkApi = fakes.mock(oBulkApi);
			oMockBulkApi.expects('exit').never();
			oMockBulkApi.expects('query').once().withArgs( aSteps[0] ).returns(null);
			oMockBulkApi.expects('query').once().withArgs( aSteps[2] ).returns(null);
			oMockBulkApi.expects('customQuery').once().withArgs( aSteps[1] ).returns(null);
			oMockBulkApi.expects('customQuery').once().withArgs( aSteps[3] ).returns(null);
			oMockBulkApi.expects('customQuery').once().withArgs( aSteps[5] ).returns(null);
			oMockBulkApi.expects('mongoQuery' ).once().withArgs( aSteps[4] ).returns(null);
			oMockBulkApi.expects('rabbitQuery').once().withArgs( aSteps[6] ).returns(null);

            var oBulkLoadConfig = emptyBulkLoadMock(true, done);
            var oMockBL = fakes.mock( oBulkLoadConfig );
			oMockBL.expects('init').once().yields( null );
			oMockBL.expects('GetSteps').once().returns( aSteps );
			
			oBulkApi.process( oBulkLoadConfig );
    
		} );
        

        test( 'Goes straight to querying the batch when the jobId/batchId combination is supplied', function( done ) {
            var oBulkLoadConfig = emptyBulkLoadMock(true, done);
           
            var oBulkLoad = fakes.mock( oBulkLoadConfig );
            oBulkLoad.expects('init').once().yields( null );
            
            var newVar = {source: 'salesforce', batchArgName: 'batchArgName'};
            oBulkLoad.expects('GetSteps').once().returns( [
                  , newVar
            ] );
            oBulkLoad.expects('getExistingJobBatchId').once().withArgs('batchArgName').returns(['theJobId', 'theBatchId']);

           
            var oBulkApi = new BulkApi( oConf, oLogger, new Queue() );

            var oMockBulkApi = fakes.mock(oBulkApi);
            oMockBulkApi.expects('query').never();
            oMockBulkApi.expects('queryBatch').once()
                .withArgs(
                    'theJobId', 
                    'theBatchId', 
                    newVar,
                    sinon.match.any, 
                    null)
                .returns(null);
            
            
            oBulkApi.process( oBulkLoadConfig );
        } );
    
		test( 'Exits on unknown source', function( done ) {
			var oQueue = new Queue();
    
			var aSteps = [
				  { source: 'salesforce' }
				, { source: 'custom' }
				, { source: 'salesforce' }
				, { source: 'unknown' }
				, { source: 'mongo' }
				, { source: 'custom' }
				, { source: 'rabbitmongo' }
			];
    
			var oQueueMock = fakes.mock( oQueue );
			oQueueMock.expects('add').exactly(4).yields();
    
			var oBulkApi = new BulkApi( oConf, oLogger, oQueue);
    
			var oMockBulkApi = fakes.mock(oBulkApi);
			oMockBulkApi.expects('exit').once();
			oMockBulkApi.expects('query').once().withArgs( aSteps[0] ).returns(null);
			oMockBulkApi.expects('customQuery').once().withArgs( aSteps[1] ).returns(null);
			oMockBulkApi.expects('query').once().withArgs( aSteps[2] ).returns(null);

            var oBulkLoadConfig = emptyBulkLoadMock(false);
            var oMockBL = fakes.mock( oBulkLoadConfig );
			oMockBL.expects('init').once().yields( null );
			oMockBL.expects('GetSteps').once().returns( aSteps );
			
			oBulkApi.process( oBulkLoadConfig );
    
			setTimeout( function() {
				fakes.verify();
				done(); 
			}, 500 );
    
		} );
    
	} );

	suite( 'query', function( ) {
		test( 'Success When it sends the query to salesforce and queues the results to be downloaded', function( done ) {
    
			var oBulkApi = new BulkApi( oConf, oLogger, {} );
			var oBulkApiMock = fakes.mock( oBulkApi );
			oBulkApiMock.expects('exit').never();
    
			var oBatch = {'id':'batchId', 'execute':function(){}, 'on':function(){}, 'poll':function(){}};
			var oBatchMock = fakes.mock(oBatch);
			oBatchMock.expects('execute').once().withArgs('query');
    
			oBatchMock.expects('on').once().withArgs('queue').yields('');
			oBatchMock.expects('poll').once().withArgs(3000, 5000);
    
			oBatchMock.expects('on').once().withArgs('progress').yields({state:'inProgress'});
    
			oBatchMock.expects('on').once().withArgs('response').yields({'result':1});
    
			oBatchMock.expects('on').once().withArgs('error');
    
			var endBatch = {
				retrieve: function(){}
			};
    
			fakes.mock( endBatch ).expects( 'retrieve' ).once().yields( null, [ {id:'res1'}, {id:'res2'} ] );
    
			var oJob = { id: 'jobId', createBatch: function() {}, job: function(){}, close: function() {} };
			var oJobMock = fakes.mock(oJob);
			oJobMock.expects('createBatch').once().returns(oBatch);
			oJobMock.expects('close').once();
    
			oBulkApiMock.expects('queueSfRequest').once().withArgs( '/tmp/object.csv', 'jobId', endBatch, 'res1' );
			oBulkApiMock.expects('queueSfRequest').once().withArgs( '/tmp/object.csv', 'jobId', endBatch, 'res2' );
			oBulkApiMock.expects('queueFileRead').once().withArgs( '/tmp/object.csv', oQueryConfig ).yields();
    
			oBulkApi.oConn = {
				'bulk':{
					'createJob':function(){},
					'job':function(jobId){
						if(jobId === 'jobId')
						return {'batch':function(batchId){
							if(batchId === 'batchId'){
								return endBatch;
							}
						}}
				}
			}};
    
			oBulkApi.fs = {
				writeFileSync: function() {}
			}
    
			fakes.mock(oBulkApi.oConn.bulk).expects('createJob').withArgs('object', 'query').returns(oJob);
    
			oBulkApi.query( oQueryConfig );
    
			setTimeout( function() {
				oLogger.info.callCount.should.be.exactly(1);
				oLogger.error.callCount.should.be.exactly(0);
				oLogger.log.callCount.should.be.exactly(4);
				
				fakes.verify();
				done();
			}, 500 );
    
		} );
		
		test( 'Error Loggs error when salesforce emits error message', function( done ) {
			var oBulkApi = new BulkApi(oConf, oLogger, {});
			fakes.mock(oBulkApi).expects('exit').once();
    
			var oBatch = {'id':'batchId', 'execute':function(){}, 'on':function(){}, 'poll':function(){}};
			var oBatchMock = fakes.mock(oBatch);
			oBatchMock.expects('execute').once().withArgs('query');
    
			oBatchMock.expects('on').once().withArgs('queue');
			oBatchMock.expects('on').once().withArgs('progress');
    
			oBatchMock.expects('on').once().withArgs('error').yields();
    
    
			var oJob = {'id':'jobId','createBatch':function(){}, 'close':function(){}};
			var oJobMock = fakes.mock(oJob);
			oJobMock.expects('createBatch').once().returns(oBatch);
			oJobMock.expects('close').once();
    
			oBulkApi.oConn = {
				'bulk':{
					'createJob':function(){}
				}
			};
			fakes.mock(oBulkApi.oConn.bulk).expects('createJob').withArgs('object', 'query').returns(oJob);
    
    
			oBulkApi.query( oQueryConfig );
    
			setTimeout( function() {
				oLogger.error.callCount.should.be.exactly(1);
				
				fakes.verify();
				done();
			}, 500 );
    
		} );
		
		test( 'Error Logs error message when the batch retrieve returns an error', function( done ) {
			var oBulkApi = new BulkApi(oConf, oLogger, {});
			fakes.mock(oBulkApi).expects('exit').once();
    
			var oBatch = {'id':'batchId', 'execute':function(){}, 'on':function(){}, 'poll':function(){}};
			var oBatchMock = fakes.mock(oBatch);
			oBatchMock.expects('execute').once().withArgs('query');
    
			oBatchMock.expects('on').once().withArgs('queue');
			oBatchMock.expects('on').once().withArgs('progress');
			oBatchMock.expects('on').once().withArgs('response').yields({'result':1});
			oBatchMock.expects('on').once().withArgs('error');
    
			var endBatch = {
				retrieve: function(){},
			};
			fakes.mock(endBatch).expects('retrieve').once().yields("ERROR", null);
    
			var oJob = {'id':'jobId','createBatch':function(){},'job':function(){}, 'close':function(){}};
			var oJobMock = fakes.mock(oJob);
			oJobMock.expects('createBatch').once().returns(oBatch);
			oJobMock.expects('close').once();
    
			oBulkApi.oConn = {
				'bulk':{
					'createJob':function(){},
					'job':function(jobId){
						if(jobId === 'jobId')
						return {'batch':function(batchId){
							if(batchId === 'batchId'){
								return endBatch;
							}
						}}
				}
			}};
			fakes.mock(oBulkApi.oConn.bulk).expects('createJob').withArgs('object', 'query').returns(oJob)
    
			oBulkApi.query( oQueryConfig );
    
			setTimeout( function() {
				oLogger.error.callCount.should.be.exactly(1);
				
				fakes.verify();
				done();
			}, 500 );
    
		} );
	} );

    suite( 'rabbitQuery', function( ) {
        test( 'Successfully creates Promise to query Rabbit and then query Mongo with all the record ids', function( done ) {
            var findResult = {each:function(){}};
            
            var oMongoCollection = {find: function () {}} ;
            var mongoQuery = {Id:{$in:["recordId1","recordId2","recordId3"]}}
            fakes.mock( oMongoCollection ).expects('find').once().withArgs( mongoQuery ).returns( findResult );
                
            var oMongoServer = {getCollection:function(){}};
            fakes.mock(oMongoServer).expects('getCollection').once().withArgs( 'cache', 'mongoCollection').returns( oMongoCollection )
            
            var aMessage = ['recordId1', 'recordId2', 'recordId3'];
            
            var oRabbit = {getMessages: function(){}};
            fakes.mock(oRabbit).expects('getMessages').once().withArgs( 'rabbitQueue').yields( aMessage, function(){}, function(){} );
            
            var oBulkApi = new BulkApi(oConf, oLogger, {}, {}, oMongoServer, oRabbit);
            
            oQueryConfig.collection = 'mongoCollection';
            oQueryConfig.queue      = 'rabbitQueue';
            oBulkApi.rabbitQuery( oQueryConfig )

            setTimeout( function() {
                fakes.verify();
                done();
            }, 500 )
            
        });
    });

    suite( 'rabbitQuery', function( ) {
        test( 'Successfully creates Promise to query Rabbit and then query Mongo with records from a previous error', function( done ) {
            var findResult = {each:function(){}};
            
            var oMongoCollection = {find: function () {}} ;
            var mongoQuery = {Id:{$in:["recordId1","recordId2","recordId3"]}}
            fakes.mock( oMongoCollection ).expects('find').once().withArgs( mongoQuery ).returns( findResult );
                
            var oMongoServer = {getCollection:function(){}};
            fakes.mock(oMongoServer).expects('getCollection').once().withArgs( 'cache', 'mongoCollection').returns( oMongoCollection )
            
            var aMessage = { ids: ['recordId1', 'recordId2', 'recordId3'], message: 'This is an errorMessage'};
            
            var oRabbit = {getMessages: function(){}};
            fakes.mock(oRabbit).expects('getMessages').once().withArgs( 'rabbitQueue').yields( aMessage, function(){}, function(){} );
            
            var oBulkApi = new BulkApi(oConf, oLogger, {}, {}, oMongoServer, oRabbit);
            
            oQueryConfig.collection = 'mongoCollection';
            oQueryConfig.queue      = 'rabbitQueue';
            oBulkApi.rabbitQuery( oQueryConfig )

            setTimeout( function() {
                fakes.verify();
                done();
            }, 500 )
            
        });
    });
} );