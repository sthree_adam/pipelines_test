var Rollup			= require( '../lib/Rollup' ),
	should 			= require( 'should' ),
	assert			= require( 'assert' ),
	sinon			= require( 'sinon' );


suite( 'Unit Test for Rollup', function() {

	var fakes,
		oConf = {
			  config: 'rollup.template'
			, collections: {
				employer: 'employer_collection'
			  , override: 'override_collection'
			  }
		};

	setup( function() {
		fakes = sinon.sandbox.create();
	} );

	teardown( function() { 
		fakes.restore();
	} );

	suite( 'getkey', function( ) {

		test( 'test keys', function() {
			var roll = new Rollup( oConf );
			assert.equal( 'foobar'                  , roll.getKey( '  foo   bar                   '        ), 'Trimming/Stripping Multiple Whitespace' );
	        assert.equal( 'barclaysbank'            , roll.getKey( 'Barclays Bank plc'                     ), 'Trimming/Stripping Multiple Whitespace story 382 - 1' );
	        assert.equal( 'barclaysbank'            , roll.getKey( 'Barclays Bank PLC'                     ), 'Trimming/Stripping Multiple Whitespace story 382 - 2' );
	        assert.equal( 'foobar'                  , roll.getKey( '  fo"""o   bar!.% .()         '        ), 'Stripping all Punctuation' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar ltd          '    		    ), 'Stripping company suffix from end' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar ltd  plc     '  		        ), 'Stripping all company suffix from end' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar LTD  PLC     '  		        ), 'Stripping all company suffix from end even uppercase' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar lTd  PlC     '  		        ), 'Stripping all company suffix from end or mixed upper lower case' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar((lTd...--PlC)) :::... >>>> """  ::  ???????? !!!! :-) :-) :-)   ' ), 'Stripping all company suffix from end or mixed upper lower case with xtreme-punctuation' );
	        assert.equal( 'air'                     , roll.getKey( '  air france       '                   ), 'Stripping country suffix from end' );
	        assert.equal( 'air'                     , roll.getKey( '  air france uk      '                 ), 'Stripping all country suffix from end' );
	        assert.equal( 'air'                     , roll.getKey( '  air france (uk)      '               ), 'Stripping all country suffix from end with punctuiation' );
	        assert.equal( 'foobar'                  , roll.getKey( '  foo bar france ltd   '               ), 'Stripping country and company suffixes from end' );
	        assert.equal( 'foobarltden'             , roll.getKey( '  foo bar ltd en france   '            ), 'Does not strip a company if it is not at end of string' );
	        assert.equal( 'foobarfrancethe'         , roll.getKey( '  foo bar france "the ltd"   '         ), 'Does not strip a country if it is not at end of string' );
	        assert.equal( 'foobar'                  , roll.getKey( '  fo"""o   bar!.% .()  ltd france '    ), 'Stripping company and country suffixes from end' );
	        assert.equal( 'air'                     , roll.getKey( '  Air France Ltd. '                    ), 'Air france sripped back to air' );
	        assert.equal( 'airfränce'               , roll.getKey( '  Air Fränce Ltd. '                    ), 'testing differenciation between a character and its diacritic' );                
	        assert.equal( 'oùétaitதமிழ்'             , roll.getKey( ' "où était தமிழ் Ltd. '                ), 'mixed multibyte and diacritics string ' );
	        assert.equal( 'இந்ததமிழ்ஒருசொற்றொடர்' , roll.getKey( ' ! (இந்த தமிழ் ஒரு) சொற்றொடர் Ltd "' ), 'Tamil multibyte stripping' );
	        assert.equal( 'air'                     , roll.getKey( 'Air भारत LtD'                           ), 'stripping multibyte country out of multibyte string 2' );
	        assert.equal( 'airभारतltdsomething'      , roll.getKey( 'Air भारत Ltd something'                 ), 'stripping multibyte country out of multibyte string 3' );
	        assert.equal( 'kulüpçomak'              , roll.getKey( '(.kul"ü??p "ç..!(oma)k (AG!'           ), 'stripping multibyte country out of multibyte string 4' );
	        assert.equal( 'トヨタ日本'                      , roll.getKey( 'トヨタ日本'                                    ), 'Toyota Japan: Japan not stripped because no spaces in Kanji' );
	        assert.equal( 'toyota'                  , roll.getKey( 'Toyota 日本'                              ), 'Toyota Japan: Japan stripped because there is a space' );
	        assert.equal( 'fcuk'                    , roll.getKey( 'Fcuk'                                  ), 'do not strip what we dont want to strip' );
	        assert.equal( 'fcukltd'                 , roll.getKey( 'Fcukltd'                                ), 'do not strip what we dont want to strip 2' );
	        assert.equal( 'fcltd'                   , roll.getKey( 'Fcltd'                                  ), 'do not strip what we dont want to strip 3' );
	        assert.equal( 'fc'                      , roll.getKey( 'Fc.uk'                                  ), 'do strip what we do want to strip 1' );
	        assert.equal( 'fc'                      , roll.getKey( 'Fc uk'                                  ), 'do strip what we do want to strip 2' );
	        assert.equal( 'fc'                      , roll.getKey( 'Fc~uk'                                  ), 'do strip what we do want to strip 3' );
	        assert.equal( 'greengoddesspvtற்ltd'     , roll.getKey( '	green goddess (pvtற்ltd         '     ), 'do strip what we do want to strip 5' );      
	        assert.equal( 'greengoddess'            , roll.getKey( '	green goddess (pvt) ltd         '                                  ), 'do strip what we do want to strip 4' );       
	        assert.equal( 'green'                   , roll.getKey( '  green---in(france: )'                                  ), 'do strip what we do want to strip 6' );       
	        assert.equal( 'greenlifeoil'            , roll.getKey( '	greenlife oil(australia/uk/usa) usa,uk'                                  ), 'do strip what we do want to strip 7' );       
	        assert.equal( 'greenlinepcs'            , roll.getKey( '	green line pcs (egypt)  '                                  ), 'do strip what we do want to strip 8' );       
	        assert.equal( 'greenparking'            , roll.getKey( '	green parking(uk)ltd     '                                  ), 'do strip what we do want to strip 9' );       
	        assert.equal( 'greenwoodsca'            , roll.getKey( '	greenwoods (c.a.) s.a    '                                  ), 'do strip what we do want to strip 11' );       
	        assert.equal( 'grengineersi'            , roll.getKey( '	g.r.engineers (i)pvt.ltd          '                                  ), 'do strip what we do want to strip 12' );       
	        assert.equal( 'grengineersi'            , roll.getKey( '	g.r.engineers (i) pvt. ltd    '                                  ), 'do strip what we do want to strip 31' );       
	        assert.equal( 'grengineersi'            , roll.getKey( '	g.r.engineers (i).,(pvt)--.#~~~:ltd~~~@::..... ))) :-) :-)    ' ), 'do strip what we do want to strip 14' );       
	        assert.equal( 'grindexpumps'            , roll.getKey( '	grindex pumps (uk)      '                                  ), 'do strip what we do want to strip 15' );       
	        assert.equal( 'grindexpumps'            , roll.getKey( '	grindex pumps(uk/uk/uk)'                                  ), 'do strip what we do want to strip 16' );       
	        assert.equal( 'grindexpumps'            , roll.getKey( '	grindex pumps uk uk uk )'                                  ), 'do strip what we do want to strip 17' );       
	        assert.equal( 'grisignanovi'            , roll.getKey( '	grisignano (vi) italy'                                  ), 'do strip what we do want to strip 18' );       
	        assert.equal( 'gromwell'                , roll.getKey( '	gromwell --++  .... in------- (us)  ..  --  ::::  ltd  :::::...,,,  ...::::!!!  ! ' ), 'do strip what we do want to strip 19' );       
	        assert.equal( 'gnetsagrèce'             , roll.getKey( '    gnet sa (grèce)'                                  ), 'do strip what we do want to strip 10' );       
	        assert.equal( 'gnncludhiana'            , roll.getKey( '	gnnc ludhiana (india)'                                  ), 'do strip what we do want to strip 21' );       
	        assert.equal( 'toyota'                  , roll.getKey( "Toyota\tusa"                           ), 'test for tabs instead of spaces' );
	        assert.equal( 'ibm'                     , roll.getKey( "ibm ~ ltd~france~, ltd usa ( ~ltd~uk~) "),'test extended stripping functionality with ~ char' );  
	        assert.equal( 'toyota'                  , roll.getKey( "Toyota\t\r\nuk"                        ), 'test for tabs instead of spaces and cr lf removal' );
	        assert.equal( 'ibm'                     , roll.getKey( "I.B.M"                                 ), 'test punktuation variants 1' );
	        assert.equal( 'ibm'                     , roll.getKey( "I B\t       M   ()..."                 ), 'test punktuation variants 2' );
	        assert.equal( 'ibm'                     , roll.getKey( "I.B M"                                 ), 'test punktuation variants 3' );
	        assert.equal( 'ibm'                     , roll.getKey( "IB \t.M"                               ), 'test punktuation variants 4' );        
	        assert.equal( 'accenture'               , roll.getKey( "accenture france"                      ), 'test basic stripping functionality 1' );        
	        assert.equal( 'accenture'               , roll.getKey( "accenture Romania"                     ), 'test basic stripping functionality 2' );        
	        assert.equal( 'accenture'               , roll.getKey( "accenture GmbH in Düsseldorf"          ), 'test basic stripping functionality 3' );     
	        assert.equal( 'ibm'                     , roll.getKey( "ibm U.K."                              ), 'test extended stripping functionality 0' );     
	        assert.equal( 'ibm'                     , roll.getKey( "ibm(uk)"                               ), 'test extended stripping functionality 1' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm-uk"                                ), 'test extended stripping functionality 2' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm/uk"                                ), 'test extended stripping functionality 3' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm(ltd)"                              ), 'test extended stripping functionality 4' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm'ltd'"                              ), 'test extended stripping functionality 5' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm-uk-ltd"                            ), 'test extended stripping functionality 6' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm- U.K-ltd"                           ), 'test extended stripping functionality 7' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm U.K(ltd)"                          ), 'test extended stripping functionality 8' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm U.K (l.t.d)"                          ), 'test extended stripping functionality 9' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uK(ltd)"                          ), 'test extended stripping functionality 10' );        
	        assert.equal( 'ibm'                     , roll.getKey( "ibm U.K(LtD)"                          ), 'test extended stripping functionality 11' );        
	        assert.equal( 'ibm'                     , roll.getKey( "I.B.M U.K(ltd)"                          ), 'test extended stripping functionality 12' ); 
	        assert.equal( 'ibmük'                   , roll.getKey( "I.B.M ü.K(ltd)"                          ), 'test extended stripping functionality 12-ü1' ); 
	        assert.equal( 'ibmukü'                  , roll.getKey( "I.B.M U.K ü(ltd)"                          ), 'test extended stripping functionality 12-ü2' );         
	        assert.equal( 'ibmaccenture'            , roll.getKey( "ibm/accenture, uk/usa                      "                          ), 'test extended stripping functionality 12' );  
	        assert.equal( 'ibmaptivasupport'        , roll.getKey( "ibm aptiva support - usa & uk czech         "                          ), 'test extended stripping functionality 13' );  
	        assert.equal( 'ibmasic'                 , roll.getKey( "ibm asic (uk) ltd                         "                          ), 'test extended stripping functionality 14' );  
	        assert.equal( 'ibmatthe'                , roll.getKey( "ibm at the uk                             "                          ), 'test extended stripping functionality 15' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm (australia/uk)                        "                          ), 'test extended stripping functionality 16' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm bcs france in paris                   "                          ), 'test extended stripping functionality 17' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm bcs uk                                "                          ), 'test extended stripping functionality 18' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk ltd -                              "                          ), 'test extended stripping functionality 19' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm -                                     "                          ), 'test extended stripping functionality 20' );  
	        assert.equal( 'ibmcanon'                , roll.getKey( "ibm/canon uk                              "                          ), 'test extended stripping functionality 21' );  
	        assert.equal( 'ibmuklimuted'            , roll.getKey( "ibm uk limuted                            "                          ), 'test extended stripping functionality 22' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm corporation uk/                       "                          ), 'test extended stripping functionality 23' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm deutschland gmbh, bukarest            "                          ), 'test extended stripping functionality 24' );  
	        assert.equal( 'ibmford'                 , roll.getKey( "ibm-ford uk/germany                       "                          ), 'test extended stripping functionality 25' );  
	        assert.equal( 'ibmfor'                  , roll.getKey( "ibm for uk                                "                          ), 'test extended stripping functionality 26' );  
	        assert.equal( 'ibmforukborderagency'    , roll.getKey( "ibm for uk border agency                  "                          ), 'test extended stripping functionality 27' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm france/belgium/luxembourg             "                          ), 'test extended stripping functionality 28' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm gbs uk ltd                            "                          ), 'test extended stripping functionality 29' );  
	        assert.equal( 'ibmglobalaccount'        , roll.getKey( "ibm global account/uk                     "                          ), 'test extended stripping functionality 30' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm .../uk ltd                "                          ), 'test extended stripping functionality 31' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm-uk                       "                          ), 'test extended stripping functionality 32' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm india, france  china               "                          ), 'test extended stripping functionality 33' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm / ltd(france), ltd usa [ltd/uk] "                          ), 'test extended stripping functionality 34' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm,uk "                          ), 'test extended stripping functionality 35' );  
	        assert.equal( 'ibmtui'                  , roll.getKey( "ibm/tui-uk                                "                          ), 'test extended stripping functionality 36' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk                                    "                          ), 'test extended stripping functionality 37' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm-uk                             "                          ), 'test extended stripping functionality 38' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm u.k. /                "                          ), 'test extended stripping functionality 39' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm (uk)                     "                          ), 'test extended stripping functionality 40' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk                         "                          ), 'test extended stripping functionality 41' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk                              "                          ), 'test extended stripping functionality 42' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk/luxembourg                           "                          ), 'test extended stripping functionality 43' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk/france /luxembourg                    "                          ), 'test extended stripping functionality 44' );  
	        assert.equal( 'ibm'                     , roll.getKey( "ibm uk bcs limited                    "                          ), 'test extended stripping functionality 45' );  
		} );
	} );

	suite( 'getEmployer', function( ) {

		test( 'get from override', function( done ) {
			var sKey = "ibm uk                              ",

				oOverrideCollection = {
					findOne: function() {}
				}

			fakes.mock( oOverrideCollection ).expects( 'findOne' ).withArgs( { key: 'ibm' }, {} ).yields( null, { name: 'IBM' } );

			var oMongo = { getCollection: function() {} };
			fakes.mock( oMongo ).expects( 'getCollection' ).withArgs( 'rollup', oConf.collections.override ).returns( oOverrideCollection );
			
			var roll = new Rollup( oConf, oMongo );
			roll.getEmployer( sKey, function( sNewKey, oError ) {

				should.not.exist( oError );
				assert.equal( sNewKey, 'IBM' );

				fakes.verify();
				done();
			} );

		} );

		test( 'get from employer', function( done ) {

			var sKey = "ibm uk                              ",
				oOverrideCollection = { findOne: function() {} },
				oEmployerCollection = { findOne: function() {} };

			fakes.mock( oOverrideCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, {} );
			fakes.mock( oEmployerCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, { name: 'IBM' } );

			var oMongo = { getCollection: function() {} };
			var oFakeMongo = fakes.mock( oMongo );
			oFakeMongo.expects( 'getCollection' ).withArgs( 'rollup', oConf.collections.override ).returns( oOverrideCollection );
			oFakeMongo.expects( 'getCollection' ).withArgs( 'rollup', oConf.collections.employer ).returns( oEmployerCollection );

			var roll = new Rollup( oConf, oMongo );
			roll.getEmployer( sKey, function( sNewKey, oError ) {

				should.not.exist( oError );
				assert.equal( sNewKey, 'IBM' );

				fakes.verify();
				done();
			} );

		} );

		test( 'get raw', function( done ) {

			var sKey = "ibm uk                              ",
				oOverrideCollection = { findOne: function() {} },
				oEmployerCollection = { findOne: function() {} };

			fakes.mock( oOverrideCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, {} );
			fakes.mock( oEmployerCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, {} );

			var oMongo = { getCollection: function() {} };
			var oFakeMongo = fakes.mock( oMongo );
			oFakeMongo.expects( 'getCollection' ).withArgs( 'rollup', oConf.collections.override ).returns( oOverrideCollection );
			oFakeMongo.expects( 'getCollection' ).withArgs( 'rollup', oConf.collections.employer ).returns( oEmployerCollection );
			
			var roll = new Rollup( oConf, oMongo );
			roll.getEmployer( sKey, function( sNewKey, oError ) {

				should.not.exist( oError );
				assert.equal( sNewKey, sKey );

				fakes.verify();
				done();
			} );

		} );

		test( 'Error - override', function( done ) {

			var sKey = "ibm uk                              ",
				oOverrideCollection = { findOne: function() {} },
				oOverrideCollection1 = { findOne: function() {} };

			fakes.mock( oOverrideCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( 'Error!!!!', null );
			fakes.mock( oOverrideCollection1 ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, { name: 'Value' } );

			var oMongo = { getCollection: function() {}, reconnect: function() {} };
			var oFakeMongo = fakes.mock( oMongo );
			var oFakeCollection = oFakeMongo.expects( 'getCollection' ).exactly(2).withArgs( 'rollup', oConf.collections.override );
			oFakeCollection.onCall( 0 ).returns( oOverrideCollection );
			oFakeCollection.onCall( 1 ).returns( oOverrideCollection1 );

			oFakeMongo.expects( 'reconnect' ).withArgs( 'rollup' ).yields();

			var roll = new Rollup( oConf, oMongo );
			roll.getEmployer( sKey, function( sNewKey, oError ) {

				should.not.exist( oError );
				assert.equal( sNewKey, 'Value' );
				fakes.verify();
				done();
			} );

		} );

		test( 'Error - employer', function( done ) {

			var sKey = "ibm uk                              ",
				oOverrideCollection = { findOne: function() {} },
				oEmployerCollection = { findOne: function() {} },
				oEmployerCollection1 = { findOne: function() {} };

			fakes.mock( oOverrideCollection ).expects( 'findOne' ).exactly(2).withArgs( { key: 'ibm' }, {} ).yields( null, {} );
			fakes.mock( oEmployerCollection ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( 'Error!!!!', null );
			fakes.mock( oEmployerCollection1 ).expects( 'findOne' ).once().withArgs( { key: 'ibm' }, {} ).yields( null, { name: 'Value' } );

			var oMongo = { getCollection: function() {}, reconnect: function() {} };
			var oFakeMongo = fakes.mock( oMongo );
			oFakeMongo.expects( 'getCollection' ).exactly(2).withArgs( 'rollup', oConf.collections.override ).returns( oOverrideCollection );
			var oFakeCollection = oFakeMongo.expects( 'getCollection' ).exactly(2).withArgs( 'rollup', oConf.collections.employer );
			oFakeCollection.onCall( 0 ).returns( oEmployerCollection );
			oFakeCollection.onCall( 1 ).returns( oEmployerCollection1 );

			oFakeMongo.expects( 'reconnect' ).withArgs( 'rollup' ).yields();

			var roll = new Rollup( oConf, oMongo );
			roll.getEmployer( sKey, function( sNewKey, oError ) {

				should.not.exist( oError );
				assert.equal( sNewKey, 'Value' );
				fakes.verify();
				done();
			} );

		} );

	} );
} );