var ManagerBulkLoad	= require( '../lib/ManagerBulkLoad' );
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var config 			= { get: function() { return { collections: { manager: 'test_manager' }, queues: { manager: 'manager_queue' } }; } };

suite( 'Unit Test for ManagerBulkLoad class', function() {
	var fakes;
    var oArgumentParser;

	setup( function() {
		fakes = sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
	} );
    
    var mockArgumentParser = function(sBrand, sStep, sStop, sStartDate){
        oArgumentParser.getArgumentValue = function(arg){
            if(arg === 'step') return sStep;
            if(arg === 'stop') return sStop;
        };
        oArgumentParser.getBrandCode = function(){ return sBrand }
        oArgumentParser.getArgumentValueDate = function(arg){
            if(arg === 'startDate') return sStartDate;
        };
        return oArgumentParser;
    };

	suite( 'GetSteps', function( ) {		
		test( 'Topics step', function( ) {
			var oManagerBulkLoad = new ManagerBulkLoad( {}, {}, {}, config, mockArgumentParser(null, 'queryTopic', null, null) );
			var aSteps = oManagerBulkLoad.GetSteps();

			aSteps[0].onRecord({Id:123, Name:'PHP'});
			assert.equal(oManagerBulkLoad.oCodes[123], 'PHP');
		} );
		
		test( 'Manager Details step', function( ) {			
			var oManager = {
				Id: 'ManagerId',
				title:'Manager Title'
			};
            
            var oMongoServer = {AddToQueue: function(){}};
            fakes.mock( oMongoServer ).expects('AddToQueue').once().withArgs( oManager, 'cache', 'test_managerBrandCode_HA' );
            
			var oManagerBulkLoad = new ManagerBulkLoad( {}, oMongoServer, {}, config, mockArgumentParser('BrandCode_HA', 'queryObject', null, 'StartDate-0123') );
			
			oManagerBulkLoad.GetSteps()[0].onRecord( oManager );

			oManagerBulkLoad.GetSteps()[0].query.should.containEql('StartDate-0123');
			oManagerBulkLoad.GetSteps()[0].query.should.containEql('BrandCode_HA');

			fakes.verify();


			var oManagerBulkLoad = new ManagerBulkLoad( {}, {}, {}, config, mockArgumentParser('BrandCode_HA', 'queryObject', null, null) );		
			oManagerBulkLoad.GetSteps()[0].query.should.not.containEql('AND LastModifiedDate');

			
		} );

	} );

	suite( 'AllLoaded', function( ) {
		test( 'Calls onAllIndexed on lookingglass', function( ) {
			var fakes 		= sinon.sandbox.create();

			var oLookingGlass = {'onAllIndexed':function(){}};
			fakes.mock( oLookingGlass ).expects('onAllIndexed').once();
			var oManagerBulkLoad = new ManagerBulkLoad( oLookingGlass, {}, {}, config, oArgumentParser );

			oManagerBulkLoad.AllLoaded();
		} );
	} );

	suite( 'GetLookingGlassRecord', function( ) {
		test( 'Turns an object with salesforce fields into a looking glass ready object', function( done ) {
			var oInputObject = {
				  'Id': 'Id1'
				, 'Name': 'Name1'
				, 'Country__c': 'Country__c1'
				, 'Brand__c': 'Brand__c1'
				, 'Status__c': 'Status__c1'
				, 'Postcode__c': 'Postcode__c1'
				, 'Email': 'Email1'
				, 'Job_Title__c': 'Job_Title__c1'
				, 'Real_Title__c': 'Real_Title__c1'
				, 'Postcode_Location__Latitude__s': '10'
				, 'Postcode_Location__Longitude__s': '50'
				, 'Work_Phone__c': 'Work_Phone__c1'
				, 'Business_Name__c': 'Business_Name__c1'
				, 'Business_Industry__c': 'Business_Industry__c1'
				, 'Last_Contact__c': '2015-11-01'
				, 'Last_Email__c': '2014-05-01'
				, 'Last_Meeting__c': '2013/06/01'
				, 'CreatedDate': '2015-06-02'
				, 'Manager_Last_Placement__c': '2015-07-02'
				, 'Manager_Last_Interview__c': '2015-08-02'
				, 'Manager_Last_Job__c': '2015-09-02'
				, 'Owner_Alias__c': 'Owner_Alias__c1'
				, 'County__c': 'County__c1'
				, 'Manager_Placement_Count__c': 5
				, 'Manager_Interview_Count__c': 0
				, 'codes' : ['PHP', 'NODE']
				, 'LastModifiedDate': '2013-06-01'
                , 'Mailshot_Mode__c' : 'yes'
			};


			var oExpectedLookingGlassRecord =  {
				  'id'	                : 'Id1'
				, 'name'                : 'Name1'
				, 'brand_code'          : 'Brand__c1'
				, 'email'		 	    : 'Email1'
				, 'work_phone' 	        : 'Work_Phone__c1'
				, 'status'	 	        : 'Status__c1'
				, 'business_name'	    : 'Business_Name__c1'
				, 'business_industry'   : 'Business_Industry__c1'
				, 'country'			    : 'Country__c1'
				, 'last_met_date'	    : '2013/06/01'
				, 'last_emailed_date'   : '2014/05/01'
				, 'last_contacted_date' : '2015/11/01'
				, 'entered_date'        : '2015/06/02'
				, 'last_placement_date' : '2015/07/02'
				, 'last_interview_date' : '2015/08/02'
				, 'last_job_date'       : '2015/09/02'
				, 'apollo_codes'		: ['PHP', 'NODE']
				, 'consultant_ownership': ['Owner_Alias__c1']
				, 'postcode'			: 'Postcode__c1'
				, 'job_title'			: 'Job_Title__c1'
				, 'real_job_title'	    : 'Real_Title__c1'
				, 'county'			    : 'County__c1'
				, 'location'			: '10,50'
				, 'activities'		    : ['PLACED WITH']
				, 'content_updated_date': '2013/06/01'
                , 'mailshot_mode'       : 'yes'
			};

			var oManagerBulkLoad = new ManagerBulkLoad( {}, {}, {}, config, oArgumentParser );		

			oManagerBulkLoad.GetLookingGlassRecord( oInputObject, function( oLgRecord ) {
				assert.deepEqual( oLgRecord, oExpectedLookingGlassRecord );
				done();
			} );
			

		} );
		test( 'With invalid dates', function( done ) {
			var oInputObject = {
				  'Id': 'Id1'
				, 'Name': 'Name1'
				, 'Country__c': 'Country__c1'
				, 'Brand__c': 'Brand__c1'
				, 'Status__c': 'Status__c1'
				, 'Postcode__c': 'Postcode__c1'
				, 'Email': 'Email1'
				, 'Job_Title__c': 'Job_Title__c1'
				, 'Real_Title__c': 'Real_Title__c1'
				, 'Postcode_Location__Latitude__s': '10'
				, 'Postcode_Location__Longitude__s': '50'
				, 'Work_Phone__c': 'Work_Phone__c1'
				, 'Business_Name__c': 'Business_Name__c1'
				, 'Business_Industry__c': 'Business_Industry__c1'
				, 'Last_Contact__c': ''
				, 'Last_Email__c': 'INVALID'
				, 'Last_Meeting__c': '2013/06/01'
				, 'CreatedDate': 'NULLL'
				, 'Owner_Alias__c': 'Owner_Alias__c1'
				, 'County__c': 'County__c1'
				, 'Manager_Placement_Count__c': 5
				, 'Manager_Interview_Count__c': 0
				, 'codes': ['PHP', 'NODE']
				, 'LastModifiedDate': 'invalid'
                , 'Mailshot_Mode__c' : 'no'
			};


			var oExpectedLookingGlassRecord =  {
				'id'	                : 'Id1'
				, 'name'                : 'Name1'
				, 'brand_code'          : 'Brand__c1'
				, 'email'		 	    : 'Email1'
				, 'work_phone' 	        : 'Work_Phone__c1'
				, 'status'	 	        : 'Status__c1'
				, 'business_name'	    : 'Business_Name__c1'
				, 'business_industry'   : 'Business_Industry__c1'
				, 'country'			    : 'Country__c1'
				, 'last_met_date'	    : '2013/06/01'
				, 'apollo_codes'		: ['PHP', 'NODE']
				, 'consultant_ownership': ['Owner_Alias__c1']
				, 'postcode'			: 'Postcode__c1'
				, 'job_title'			: 'Job_Title__c1'
				, 'real_job_title'	    : 'Real_Title__c1'
				, 'county'			    : 'County__c1'
				, 'location'			: '10,50'
				, 'activities'		    : ['PLACED WITH']
				, 'content_updated_date': null
				, 'entered_date'        : null
				, 'last_emailed_date'   : null
				, 'last_contacted_date' : null
				, 'last_placement_date' : null
				, 'last_interview_date' : null
				, 'last_job_date' 		: null
                , 'mailshot_mode'       : 'no'
			};

			var oManagerBulkLoad = new ManagerBulkLoad( {}, {}, {}, config, oArgumentParser );

			oManagerBulkLoad.GetLookingGlassRecord( oInputObject, function( oLgRecord ) {
				assert.deepEqual( oLgRecord, oExpectedLookingGlassRecord );
				done();
			} );

		} );
	} );

} );