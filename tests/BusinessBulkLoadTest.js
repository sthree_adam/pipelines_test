var BusinessBulkLoad = require( '../lib/BusinessBulkLoad' );
var should 			 = require( 'should' );
var assert			 = require( 'assert' );
var sinon			 = require( 'sinon' );
var config 			 = { get: function() { return { collections: { business: 'test_business' }, queues: { business: 'business_queue' } }; } };

suite( 'Unit Test for BusinessBulkLoad class', function() {
	var fakes;
    var oArgumentParser;
    
	setup( function() {
		fakes = sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
	} );

    var mockArgumentParser = function(sBrand, sStep, sStop, sStartDate){
        oArgumentParser.getArgumentValue = function(arg){
            if(arg === 'step') return sStep;
            if(arg === 'stop') return sStop;
        };
        oArgumentParser.getBrandCode = function(){ return sBrand }
        oArgumentParser.getArgumentValueDate = function(arg){
            if(arg === 'startDate') return sStartDate;
        };
        return oArgumentParser;
    };

	suite( 'GetSteps', function( ) {		
		test( 'Topics step', function( ) {
			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, mockArgumentParser('Brand Code', 'queryTopic', null, null ) );
			var aSteps = oBusinessBulkLoad.GetSteps();

			aSteps[0].onRecord({Id:123, Name:'PHP'});
			assert.equal(oBusinessBulkLoad.oCodes[123], 'PHP');
		} );

		test( 'Address step', function( ) {
			var oMongoServer = {'AddAddressToQueue':function(){}};
			fakes.mock( oMongoServer ).expects( 'AddAddressToQueue' ).once().withArgs({Id:'addressId'}, "something", 'cache', 'test_businessBrand Code');

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, oMongoServer, {}, config, mockArgumentParser('Brand Code', 'queryAddress', null, null ) );
			var aSteps = oBusinessBulkLoad.GetSteps();

			oBusinessBulkLoad.oBusinessAddress['addressId'] = "something"

			aSteps[0].onRecord({Id:'addressId'});
		} );

		test( 'Business Address step', function( ) {
			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, mockArgumentParser('Brand Code', 'queryBusinessAddress', null )  );
			
			oBusinessBulkLoad.GetSteps()[0].onRecord( { Account__c:'businessId', Address__c: 'addressId' } );

			assert( oBusinessBulkLoad.oBusinessAddress['addressId'].indexOf('businessId') !== -1 );
		} );

		test( 'Details step', function( ) {
			var oBusiness = {
				Id: 'BusinessId',
				Name:'Business Name',
				Other:'SomeOtherRubbish'
			};
            
			var oMongoServer = {AddToQueue: function(){}};
            fakes.mock( oMongoServer ).expects('AddToQueue').once().withArgs( oBusiness, 'cache', 'test_businessBrandCode_HA' );


			var oBusinessBulkLoad = new BusinessBulkLoad( {}, oMongoServer, {}, config,  mockArgumentParser('BrandCode_HA', 'queryObject', null, 'StartDate-0123') );
			
			oBusinessBulkLoad.GetSteps()[0].onRecord( oBusiness );

			oBusinessBulkLoad.GetSteps()[0].query.should.containEql('StartDate-0123');
			oBusinessBulkLoad.GetSteps()[0].query.should.containEql('BrandCode_HA');

			fakes.verify();

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config,  mockArgumentParser('BrandCode_HA', 'queryObject', null, null ) );		
			oBusinessBulkLoad.GetSteps()[0].query.should.not.containEql('AND LastModifiedDate');
			
		} );

	} );

	suite( 'GetLookingGlassRecord', function( ) {
		test( 'Creates a looking glass ready object from a salesforce object', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Status__c: 'status'
				, codes: ['code1', 'code2']
				, address: {
					  Country__c:'country'
					, Postcode__c:'postcode'
					, Postcode_Location__Latitude__s: 'lat'
					, Postcode_Location__Longitude__s: 'long'
					, County__c: 'county'
				}
                , 'Tier__c': 'tier'
                , 'Industry': 'industry'
			};

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, oArgumentParser );
			
			var oExpected = {
				  id 				: 'id'
				, brand_code		: 'brand'
				, name      		: 'name'
				, status			: 'status'
				, country 			: 'country'
				, postcode 			: 'postcode'
				, county            : 'county'
				, codes				: ['code1', 'code2']
				, location          : 'lat,long'
				, tier              : 'tier'
				, industry          : 'industry'
			};

			oBusinessBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Handles Businesses with no address', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Status__c: 'status'
				, codes: ['code1', 'code2']
                , 'Tier__c': 'tier'
                , 'Industry': 'industry'
			};

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, name 				: 'name'
				, status			: 'status'
				, country 			: null
				, postcode 			: null
				, county            : null
				, codes				: ['code1', 'code2']
                , tier              : 'tier'
                , industry          : 'industry'
		 	};

		 	oBusinessBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
		 		assert.deepEqual( oActual, oExpected );
		 		done();
		 	} );
		 } );

		test( 'Handles Businesses with no codes', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Status__c: 'status'
                , Tier__c: 'tier'
                , Industry: 'industry'
				, codes: []
			};

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, name 				: 'name'
				, status			: 'status'
				, country 			: null
				, postcode 			: null
				, county            : null
				, codes				: []
				, tier              : 'tier'
				, industry          : 'industry'
			};

			oBusinessBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Handles Businesses with no tier or industry', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Status__c: 'status'
				, codes: ['code1', 'code2']
				, address: {
					  Country__c:'country'
					, Postcode__c:'postcode'
					, County__c:'county'
					, Postcode_Location__Latitude__s: 'lat'
					, Postcode_Location__Longitude__s: 'long'
				}
                , 'Tier__c': null
                , 'Industry': null
			};

			var oBusinessBulkLoad = new BusinessBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				  id 				: 'id'
				, brand_code		: 'brand'
				, name      		: 'name'
				, status			: 'status'
				, country 			: 'country'
				, postcode 			: 'postcode'
				, county            : 'county'
				, codes				: ['code1', 'code2']
				, location          : 'lat,long'
				, tier              : null
				, industry          : null
			};

			oBusinessBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

	} );

} );
