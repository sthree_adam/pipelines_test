var ArgumentsParser  = require( '../lib/ArgumentsParser' );
var assert			 = require( 'assert' );

suite( 'Unit Test for Arguments Parser', function(){

	suite( 'validate', function(){

		test( 'returns true for a list of valid arguments', function(){

			var oArgumentsParser = new ArgumentsParser(
				  ['job', 'business', 'candidate', 'manager']
				, ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
				, ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
			);

			oArgumentsParser.validate([]);
		} );

        test( 'throws exception for invalid bulk load type', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'INVALID', 'HA', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );
            assert.throws(function(){oArgumentsParser.validate([])}, Error);
        });

        test( 'throws exception for invalid brand', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HAS', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );
            
            assert.throws(function(){oArgumentsParser.validate([])}, Error);
        });

        test( 'throws exception for invalid optional argument', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--stepBUG', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );

            assert.throws(function(){oArgumentsParser.validate([])}, Error);
        });

        test( 'throws exception when there arguments that should have values but dont', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );

            assert.throws(function(){oArgumentsParser.validate(['getSteps'])}, Error);
        });
	} );
    
    
    suite('getArgumentValue', function(){
       
        test('returns the argument value', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );

            assert.equal(oArgumentsParser.getArgumentValue( 'step' ), 'stepName');
        });

        test('returns null for non existent argument', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep', 'stopStepName', '--getSteps' ]
            );

            assert.equal(oArgumentsParser.getArgumentValue( 'doesntExist' ), null);
        });

        test('throws exception for argument with missing value', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep', '--getSteps' ]
            );

            assert.throws(function(){oArgumentsParser.getArgumentValue( 'sStopStep' )}, Error);
        });

        test('throws exception for argument with missing value at the end', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'stepName', '--sStopStep' ]
            );

            assert.throws(function(){oArgumentsParser.getArgumentValue( 'sStopStep' )}, Error);
        });

        test('throws exception for argument with invalid step', function(){
            var oArgumentsParser = new ArgumentsParser(
                ['job', 'business', 'candidate', 'manager']
                , ['step', 'getSteps', 'sStopStep']
                , [ 'stepName' ,  'stopStepName' ]
                , ['node.exe', '/path/to/file.js', 'job', 'HA', '--step', 'notAStepName' ]
            );

            assert.throws(function(){oArgumentsParser.getArgumentValue( 'notAStepName' )}, Error);
        });
        
    });

});