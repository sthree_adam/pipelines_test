var CandidateBulkLoad = require( '../lib/CandidateBulkLoad' );
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var moment			= require( 'moment' );
var config 			= { get: function() { return { collections: { candidate: 'test_candidate' }, queues: { candidate: 'candidate_queue' } }; } };

suite( 'Unit Test for CandidateBulkLoad class', function() {

	var fakes;
    var oArgumentParser;

	setup( function() {
		fakes 		= sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
		fakes.restore();
	} );

    var mockArgumentParser = function(sBrand, sStep, sStop, sStartDate){
        oArgumentParser.getArgumentValue = function(arg){
            if(arg === 'step') return sStep;
            if(arg === 'stop') return sStop;
        };
        oArgumentParser.getBrandCode = function(){ return sBrand }
        oArgumentParser.getArgumentValueDate = function(arg){
            if(arg === 'startDate') return sStartDate;
        };
        return oArgumentParser;
    };

	suite( 'GetSteps', function( ) {		
		test( 'Topics step', function( ) {
			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BrandCode_HA', 'start', null, null), {}, {}, {}, config );
			var aSteps = oCandidateBulkLoad.GetSteps();

			aSteps[3].onRecord({Id:123, Name:'PHP'});
			assert.equal(oCandidateBulkLoad.oCodes[123], 'PHP');
		} );
		
		test( 'Candidate Details step', function( ) {

			var oCandidate = {
				Id: 'candidateId',
				title:'PHP Developer job'
			};
            
            var oMongoServer = {AddToQueue: function(){}};
            fakes.mock( oMongoServer ).expects('AddToQueue').once().withArgs( oCandidate, 'cache', 'test_candidateBrandCode_HA' );

			var oCandidateBulkLoad = new CandidateBulkLoad( {}, oMongoServer, {}, mockArgumentParser('BrandCode_HA', 'start', null, 'StartDate-0123'), {}, {}, {}, config );
			
			oCandidateBulkLoad.GetSteps()[1].onRecord( oCandidate );

			oCandidateBulkLoad.GetSteps()[1].query.should.containEql('StartDate-0123');
			oCandidateBulkLoad.GetSteps()[1].query.should.containEql('BrandCode_HA');

			fakes.verify();


			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BrandCode_HA', 'start', null, null), {}, {}, {}, config );	
			oCandidateBulkLoad.GetSteps()[1].query.should.not.containEql('AND LastModifiedDate');
			
		} );

	} );

	suite( 'AllLoaded', function( ) {
		test( 'Calls onAllIndexed on lookingglass', function( ) {

			var oLookingGlass = {'onAllIndexed':function(){}};
			fakes.mock( oLookingGlass ).expects('onAllIndexed').once();
			var oCandidateBulkLoad = new CandidateBulkLoad( oLookingGlass, {}, {}, mockArgumentParser('BrandCode_HA', 'start', null, null), {}, {}, {}, config );

			oCandidateBulkLoad.AllLoaded();
		} );
	} );

	suite( 'GetLookingGlassRecord', function( ) {

		var oInputObject, oParsedCv, oExpectedLookingGlassRecord, sCvm, oConfig, oAwsS3, oXmlParser, oRollup;

		setup( function() {
			oInputObject = {
	             'Id' : 'Id1'
	           , 'Name' : 'Name1'
	           , 'Country__c' : 'Country__c1'
	           , 'Last_CV_Update__c' : '2015-05-02'
	           , 'Brand__c' : 'Brand__c1'
	           , 'Candidate_Type__c' : 'Candidate_Type__c1'
	           , 'City__c' : 'City__c1'
	           , 'Available__c' : 'INVALID'
	           , 'Status__c' : 'ACCREDITED'
	           , 'Postcode__c' : 'Postcode__c1'
	           , 'Email' : 'Email1'
	           , 'Real_Title__c' : 'Real_Title__c1'
	           , 'Job_Title_Updated__c' : '2013-05-12'
	           , 'Postcode_Location__Latitude__s' : 20
	           , 'Postcode_Location__Longitude__s' : -0.5
	           , 'Candidate_Placement_Count__c' : 0
	           , 'Candidate_Interview_Count__c' : 2
	           , 'Candidate_Sendout_Count__c' : 20
	           , 'Birthdate' : '1985/05/02'
	           , 'Last_CV_Source__c' : 'Last_CV_Source__c1'
	           , 'Candidate_Last_Placement__c' : '2015-05-12'
	           , 'Last_Sendout__c' : null
	           , 'Candidate_Last_Interviewed__c' : 'INVALID DATE'
	           , 'Mailshot_Mode__c' : 'yes'
	           , codes: ['PHP', 'NODE']
			};

			oParsedCv = {
				  cv: 'my xml cv'
				, CurrentJobTitle: 'current title'
				, LastKnownJobTitle: 'previous title'
				, RawEmployers: [ 'mi6', 'cia', 'kgb' ]
				, Employers: []
				, CurrentEmployer: 'mi6'
				, JobTitles: [ 'previous title', 'current title' ]
				, Skills: [ 'I', 'can', 'do', 'something' ]
				, CurrentSkills: [ 'I', 'am', 'too', 'lazy' ]
				, CareerStartDate: '2015-03-05'
			}

			oExpectedLookingGlassRecord =  {
	             'id'                    : 'Id1'
	           , 'cv'                    : oParsedCv.cv
	           , 'name'                  : 'Name1'
	           , 'country'               : 'Country__c1'
	           , 'parsing_engine'        : 'SOVREN'
	           , 'cv_last_updated'       : '2015/05/02'
	           , 'brand_code'            : 'Brand__c1'
	           , 'skills'                : oParsedCv.Skills
	           , 'candidate_type'        : 'Candidate_Type__c1'
	           , 'town'                  : 'City__c1'
	           , 'candidate_status'      : 'ACCREDITED'
	           , 'home_postcode'         : 'Postcode__c1'
	           , 'current_job_title'     : 'current title'
	           , 'last_known_job_title'  : 'previous title'
	           , 'cv_source'             : 'Last_CV_Source__c1'
	           , 'job_titles'            : [ 'previous title', 'current title', 'Real_Title__c1' ]
	           , 'employers'             : [ oParsedCv.RawEmployers[0] + '_rolledup' 
	           							   , oParsedCv.RawEmployers[1] + '_rolledup' 
	           							   , oParsedCv.RawEmployers[2] + '_rolledup' 
	           							   ]
	           , 'employers_raw'         : oParsedCv.RawEmployers
	           , 'current_employer'      : oParsedCv.CurrentEmployer + '_rolledup'
	           , 'current_skills'        : oParsedCv.CurrentSkills
	           , 'location'              : '20,-0.5'
	           , 'highlight_class'       : 'accredited'
	           , 'candidate_activities'  : ['SENTOUT', 'INTERVIEWED']
	           , 'career_start_date'     : '2015/03/05'
	           , 'apollo_codes'          : ['PHP', 'NODE']
	           , 'candidate_birth_date'  : '1985/05/02'
	           , 'last_placement'        : '2015/05/12'
	           , "availability_date"	 : null
	           , "last_interview"		 : null
	           , "last_sendout"			 : null
	           , "mailable"			 	 : 'yes'
			};

			sCv = 'xml cv buffer';
			oConfig = {
				get: function() {}
			}

			oAwsS3 = {
				getObject: function() {}
			}

			oXmlParser = {
				getParsedCv: function() {}
			};

			oRollup = {
				getEmployer: function() {}
			};
		} );

		var validate = function( done ) {

			var confMock = fakes.mock( oConfig );
			confMock.expects( 'get' ).exactly(2).withArgs( 'salesforce' ).returns( { collections: { candidate: 'cc' }, queues: { candidate: 'cq' } } );
			confMock.expects( 'get' ).once().withArgs( 'aws:bucket' ).returns( 'awsBucket' );
			confMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 1 );

			fakes.mock( oAwsS3 ).expects( 'getObject' ).once().withArgs( {
				  Bucket: 'awsBucket'
				, Key: 'Id1_Brand__c1.xml'
			} ).yields( null, { Body: sCv } );

			
			fakes.mock( oXmlParser ).expects( 'getParsedCv' ).once().withArgs( sCv, oConfig ).yields( oParsedCv );
			
			oRollupMock = fakes.mock( oRollup );

			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[0] ).yields( oParsedCv.RawEmployers[0] + '_rolledup' );
			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[1] ).yields( oParsedCv.RawEmployers[1] + '_rolledup' );
			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[2] ).yields( oParsedCv.RawEmployers[2] + '_rolledup' );

			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BC', 'step', 'stop', 'date'), oAwsS3, oXmlParser, oRollup, oConfig );

			oCandidateBulkLoad.GetLookingGlassRecord( oInputObject, function( oActual ) {
				assert.deepEqual( oActual, oExpectedLookingGlassRecord );
				fakes.verify();
				done();
			} );
		}

		test( 'All good - Job_Title_Updated__c not in range', function( done ) {
			validate( done );
		} );	

		test( 'All good - Job_Title_Updated__c in range, Last_CV_Update__c > Job_Title_Updated__c', function( done ) {
			oInputObject.Job_Title_Updated__c = moment().subtract( 7, 'days' ).format( 'YYYY/MM/DD' );
			oInputObject.Last_CV_Update__c = moment().subtract( 5, 'days' ).format( 'YYYY/MM/DD' );
			oExpectedLookingGlassRecord.cv_last_updated = oInputObject.Last_CV_Update__c;

			validate( done );
		} );	

		test( 'All good - Job_Title_Updated__c in range, Last_CV_Update__c < Job_Title_Updated__c', function( done ) {
			oInputObject.Job_Title_Updated__c = moment().subtract( 5, 'days' ).format( 'YYYY/MM/DD' );
			oInputObject.Last_CV_Update__c = moment().subtract( 7, 'days' ).format( 'YYYY/MM/DD' );
			oExpectedLookingGlassRecord.cv_last_updated = oInputObject.Last_CV_Update__c;
			oExpectedLookingGlassRecord.current_job_title = oInputObject.Real_Title__c;
			oExpectedLookingGlassRecord.last_known_job_title = oInputObject.Real_Title__c;

			validate( done );
		} );

		test( 'All good - Job_Title_Updated__c in range, Last_CV_Update__c > Job_Title_Updated__c, no job title on cv', function( done ) {
			oInputObject.Job_Title_Updated__c = moment().subtract( 7, 'days' ).format( 'YYYY/MM/DD' );
			oInputObject.Last_CV_Update__c = moment().subtract( 5, 'days' ).format( 'YYYY/MM/DD' );
			oParsedCv.CurrentJobTitle = '';
			oParsedCv.LastKnownJobTitle = '';

			oExpectedLookingGlassRecord.cv_last_updated = oInputObject.Last_CV_Update__c;
			oExpectedLookingGlassRecord.cv_last_updated = oInputObject.Last_CV_Update__c;
			oExpectedLookingGlassRecord.current_job_title = oInputObject.Real_Title__c;
			oExpectedLookingGlassRecord.last_known_job_title = oInputObject.Real_Title__c;
			validate( done );
		} );

		test( 'Error from Aws', function( done ) {
			var confMock = fakes.mock( oConfig );
			confMock.expects( 'get' ).exactly(2).withArgs( 'salesforce' ).returns( { collections: { candidate: 'cc' }, queues: { candidate: 'cq' } } );
			confMock.expects( 'get' ).once().withArgs( 'aws:bucket' ).returns( 'awsBucket' );
			confMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 1 );

			fakes.mock( oAwsS3 ).expects( 'getObject' ).once().withArgs( {
				  Bucket: 'awsBucket'
				, Key: 'Id1_Brand__c1.xml'
			} ).yields( { message: 'Error!!!' }, null );

			fakes.mock( oXmlParser ).expects( 'getParsedCv' ).never();

			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BrandCode', 'step', 'stop', 'date'), oAwsS3, oXmlParser, oRollup, oConfig );

			oCandidateBulkLoad.GetLookingGlassRecord( oInputObject, function( oActual, oError ) {
				assert.deepEqual( oError, 'Error!!!' );
				done();
			} );

		} );

		test( 'Error from xml parser', function( done ) {
			var confMock = fakes.mock( oConfig );
			confMock.expects( 'get' ).exactly(2).withArgs( 'salesforce' ).returns( { collections: { candidate: 'cc' }, queues: { candidate: 'cq' } } );
			confMock.expects( 'get' ).once().withArgs( 'aws:bucket' ).returns( 'awsBucket' );
			confMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 1 );

			fakes.mock( oAwsS3 ).expects( 'getObject' ).once().withArgs( {
				  Bucket: 'awsBucket'
				, Key: 'Id1_Brand__c1.xml'
			} ).yields( null, { Body: sCv } );

			
			fakes.mock( oXmlParser ).expects( 'getParsedCv' ).once().withArgs( sCv, oConfig ).yields( null, 'Error!' );

			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BrandCode', 'step', 'stop', 'date'), oAwsS3, oXmlParser, oRollup, oConfig );

			oCandidateBulkLoad.GetLookingGlassRecord( oInputObject, function( oActual, oError ) {
				assert.deepEqual( oError, 'Error!' );
				done();
			} );

		} );	

		test( 'Does not call callback multiple times when rolling up employer errors', function( done ) {
			var confMock = fakes.mock( oConfig );
			confMock.expects( 'get' ).exactly(2).withArgs( 'salesforce' ).returns( { collections: { candidate: 'cc' }, queues: { candidate: 'cq' } } );
			confMock.expects( 'get' ).once().withArgs( 'aws:bucket' ).returns( 'awsBucket' );
			confMock.expects( 'get' ).once().withArgs( 'time_limit_for_current_employer' ).returns( 1 );

			fakes.mock( oAwsS3 ).expects( 'getObject' ).once().withArgs( {
				  Bucket: 'awsBucket'
				, Key: 'Id1_Brand__c1.xml'
			} ).yields( null, { Body: sCv } );

			
			fakes.mock( oXmlParser ).expects( 'getParsedCv' ).once().withArgs( sCv, oConfig ).yields( oParsedCv );
			
			oRollupMock = fakes.mock( oRollup );

			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[0] ).yields( oParsedCv.RawEmployers[0] + '_rolledup' );
			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[1] ).yields( null, 'Error!' );
			oRollupMock.expects( 'getEmployer' ).once().withArgs( oParsedCv.RawEmployers[2] ).yields( oParsedCv.RawEmployers[2] + '_rolledup' );

			var oCandidateBulkLoad = new CandidateBulkLoad( {}, {}, {}, mockArgumentParser('BrandCode', 'step', 'stop', 'date'), oAwsS3, oXmlParser, oRollup, oConfig );

			oCandidateBulkLoad.GetLookingGlassRecord( oInputObject, function( oActual, oError ) {
				assert.deepEqual( oError, 'Error!' );
				setTimeout( function() { done(); }, 100 );
			} );

		} );

	} );

} );