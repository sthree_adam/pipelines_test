var BulkLoadFactory = require( '../lib/BulkLoadFactory' );
var JobBulkLoad     = require( '../lib/JobBulkLoad' );
var ManagerBulkLoad = require( '../lib/ManagerBulkLoad' );
var CandidateBulkLoad = require( '../lib/CandidateBulkLoad' );
var BusinessBulkLoad = require( '../lib/BusinessBulkLoad' );
var LookingGlass    = require( '../lib/LookingGlass' );
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var Http            = require( 'http' );
var Https           = require( 'https' );
var Moment          = require( 'moment' );

suite( 'Unit Test for BulkLoadFactory class', function() {
	var oBulkLoadFactory;
	var fakes;
    var oArgumentParser = {};
    
	setup( function() {
		oBulkLoadFactory = new BulkLoadFactory( __dirname + '/../conf/config.template.json' );
		fakes = sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
	} );

    var mockArgumentParser = function(sType){
        oArgumentParser.getBulkLoadType = function(){ return sType }
        return oArgumentParser;
    };

	suite( 'newBulkLoad', function() {		
		var oConf = { get: function(){
						return { "rabbitBatchSize"   : 
									{ "candidate": 10
									, "manager": 10
		 							, "job": 5
		 							, "business": 20
		 							},
		 						  "collections":{ "candidate": 'abc', "manager": 'abc', "job": 'abc', "business": 'abc'},
		 						  "queues":{ "candidate": 'abc', "manager": 'abc', "job": 'abc', "business": 'abc'}
			 					}
			 				}
			 			};

		test( 'Success JobBulkLoad', function() {
			fakes.mock( oBulkLoadFactory ).expects( 'getMongo' ).returns( "mongoObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getRabbit' ).returns( "rabbitObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getConf' ).returns( oConf );
			fakes.mock( oBulkLoadFactory ).expects( 'newLookingGlass' ).once().returns( 'lookingGlassObject' );
			var oBulkLoad = oBulkLoadFactory.newBulkLoad( mockArgumentParser( 'job' ));
			assert( oBulkLoad instanceof JobBulkLoad );
			assert.equal( oBulkLoad.oLookingGlass, 'lookingGlassObject' );

			fakes.verify();
		} );

		test( 'Success BusinessBulkLoad', function() {
			fakes.mock( oBulkLoadFactory ).expects( 'getMongo' ).returns( "mongoObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getRabbit' ).returns( "rabbitObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getConf' ).returns( oConf );
			fakes.mock( oBulkLoadFactory ).expects( 'newLookingGlass' ).once().returns( 'lookingGlassObject' );
			var oBulkLoad = oBulkLoadFactory.newBulkLoad( mockArgumentParser( 'business' ));
			assert( oBulkLoad instanceof BusinessBulkLoad );
			assert.equal( oBulkLoad.oLookingGlass, 'lookingGlassObject' );

			fakes.verify();
		} );

		test( 'Success ManagerBulkLoad', function() {
			fakes.mock( oBulkLoadFactory ).expects( 'getMongo' ).returns( "mongoObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getRabbit' ).returns( "rabbitObject" );
			fakes.mock( oBulkLoadFactory ).expects( 'getConf' ).returns( oConf );
			fakes.mock( oBulkLoadFactory ).expects( 'newLookingGlass' ).once().returns( 'lookingGlassObject' );
			var oBulkLoad = oBulkLoadFactory.newBulkLoad( mockArgumentParser( 'manager' ));
			assert( oBulkLoad instanceof ManagerBulkLoad );
			assert.equal( oBulkLoad.oLookingGlass, 'lookingGlassObject' );

			fakes.verify();
		} );

		test( 'Success CandidateBulkLoad', function() {
			var oBulkLoadFactoryMock = fakes.mock(oBulkLoadFactory);
			fakes.mock( oBulkLoadFactory ).expects( 'newLookingGlass' ).once().returns( 'lookingGlassObject' );
			oBulkLoadFactoryMock.expects( 'getRollup' ).returns( 'mongo' );
			oBulkLoadFactoryMock.expects( 'getRabbit' ).once().returns( 'rabbit' );
			
			var oBulkLoad = oBulkLoadFactory.newBulkLoad( mockArgumentParser( 'candidate' ) );
			assert( oBulkLoad instanceof CandidateBulkLoad );
			assert.equal( oBulkLoad.oLookingGlass, 'lookingGlassObject' );

			fakes.verify();
		} );

		test( 'Error', function( ) {
			assert.throws(
				function() { oBulkLoadFactory.newBulkLoad('invalid') },
				'Invalid bulk load type: invalid'
				);
		} );
	} );

	suite( 'newLookingGlass', function( ) {		
		test( 'Success Creates looking glass on http', function( ) {
			var lookinglassConf = {'url':'http://domainName', 'concurrentrequests': 10};

			var oConf = {get:function(){}};
			fakes.mock(oConf).expects('get').once().withArgs('lookinglass').returns(lookinglassConf);
			fakes.mock(oBulkLoadFactory).expects('getConf').once().returns(oConf);
			fakes.mock(oBulkLoadFactory).expects('getLogger').once().returns('oLogger'); 			
			fakes.mock(oBulkLoadFactory).expects('newQueue').twice();
            fakes.mock(oBulkLoadFactory).expects('getRabbit').once().returns('oRabbit');

			fakes.mock(oBulkLoadFactory).expects('getHttp').once();
			fakes.mock(oBulkLoadFactory).expects('getHttps').never();

			var oLookingGlass = oBulkLoadFactory.newLookingGlass( 'RabbitQueueName' );

			assert(oLookingGlass instanceof LookingGlass);
 			assert.equal(oLookingGlass.oLogger,'oLogger');
 			assert.equal(oLookingGlass.oRabbit,'oRabbit');

		} );

		test( 'Success Creates looking glass on https', function( ) {
			var lookinglassConf = {'url':'https://domainName', 'concurrentrequests': 10};

			var oConf = {get:function(){}};
			fakes.mock(oConf).expects('get').once().withArgs('lookinglass').returns(lookinglassConf);
			fakes.mock(oBulkLoadFactory).expects('getConf').once().returns(oConf);
			fakes.mock(oBulkLoadFactory).expects('getLogger').once().returns('oLogger');
            fakes.mock(oBulkLoadFactory).expects('newQueue').twice();
            fakes.mock(oBulkLoadFactory).expects('getRabbit').once().returns('oRabbit');

			fakes.mock(oBulkLoadFactory).expects('getHttp').never();
			fakes.mock(oBulkLoadFactory).expects('getHttps').once();

			var oLookingGlass = oBulkLoadFactory.newLookingGlass();

			assert(oLookingGlass instanceof LookingGlass);
 			assert.equal(oLookingGlass.oLogger,'oLogger');
            assert.equal(oLookingGlass.oRabbit,'oRabbit');
		} );
	} );
} );