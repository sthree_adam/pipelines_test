var LookingGlass	= require( '../lib/LookingGlass' );
var Queue           = require( 'promise-queue');
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var Promise         = require( 'promise' );
var Http            = require( 'http' );

suite( 'Unit Test for LookingGlass class', function() {

	var fakes 		= sinon.sandbox.create();
	var oQueue;
    var oRecordsQueue;
	var oLogger;
    var oRabbit;

	var oLookingGlass;	

	var promisseMatch = sinon.match( function( value ) {
    	return value() instanceof Promise;
	}, "promisseMatch" );

	var oConf = {
				url: 'http://hostname:9999',
				accesskey : 'lookinglass_accesskey',
				retryLimit : 2
			};

	var fMoment = function( oDate ) {
				return {
					format: function( sFormat ) {
						return '2015/01/20';
					},
					unix: function(){
						return '99999999999';
					}
				}
			};

	setup( function() {
		oQueue        = new Queue();
        oRabbit       = {addMessage:function(){}}
        oRecordsQueue = new Queue();
		oLogger = { error: fakes.spy(), info: fakes.spy(), log:  fakes.spy(), logMem: fakes.spy(), logMemStats:  fakes.spy(), logQueueStats:  fakes.spy()};
	} );

	teardown( function() { 
		fakes.restore();
	} );

	suite( 'AddToQueue', function( ) {
		test( 'Success Check that it returns a function with a promise', function( ) {
			var aSfObjectData = [{ Id: 'object id', look__c: 'beautiful' }];
			var oLgObjectData = { id: 'object id', look: 'beautiful' };
			
		    fakes.mock( oQueue ).expects( 'add' ).once().yields();

		    var BulkLoadObject = { type: 'object', GetLookingGlassRecord: function() {},sQueue: 'RabbitQueue'  };
			fakes.mock( BulkLoadObject ).expects( 'GetLookingGlassRecord' ).once().yields( oLgObjectData );
        
		    oLookingGlass  = new LookingGlass( oConf, oLogger, oQueue, Http, fMoment, oRecordsQueue )
        
			oLookingGlass.AddToQueue( aSfObjectData, BulkLoadObject, function() {} );

            fakes.verify();
		} );
      
		test( 'Success', function( done ) {
			fakes.mock( oQueue ).expects( 'add' ).once().yields();        
			var aSfDataObjects = [{ Id: 'object id1', look__c: 'beautiful' },
                                  { Id: 'object id2', look__c: 'beautiful' } ];
            
			var aLgDataObjects = [{ id: 'object id1', look: 'beautiful' },
                                  { id: 'object id2', look: 'beautiful' }  ];
        
			var expectedOptions = {
				hostname: 'hostname',
				port: '9999',
				path: '/object',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
	  			 	'Content-Length': Buffer.byteLength( JSON.stringify( {data:aLgDataObjects} ) ),
	  			 	'AccessKey': 'lookinglass_accesskey99999999999000'
				}
			};
        
            var fGetLookingGlassRecordStub = sinon.stub();
            fGetLookingGlassRecordStub
                .onFirstCall().yields(aLgDataObjects[0])
                .onSecondCall().yields(aLgDataObjects[1]);
            var BulkLoadObject = { type: 'object', GetLookingGlassRecord: fGetLookingGlassRecordStub, sQueue: 'RabbitQueue' };
        
			var result = { on: function() {} };
			var resultMock = fakes.mock( result );
        
			resultMock.expects( 'on' ).withArgs( 'data' ).yields( 'OK' );
			resultMock.expects( 'on' ).withArgs( 'end' ).yields( );
        
			var request = { on: function(){}, write: function(){}, end: function(){} };
			var requestMock = fakes.mock(request);
        
			requestMock.expects( 'on' ).once().withArgs('error');
			requestMock.expects( 'write' ).once().withArgs( JSON.stringify( {data:aLgDataObjects} ) );
			requestMock.expects( 'end' ).once();
        
			fakes.mock( Http ).expects( 'request' ).once().withArgs( expectedOptions ).returns( request ).yields( result );			
        
			new LookingGlass( oConf, oLogger, oQueue, Http, fMoment )
				.AddToQueue( aSfDataObjects, BulkLoadObject, function( error ) {
					should.not.exist( error );
					oLogger.error.called.should.be.false;
					done();	
				} );
            
			fakes.verify();
        
		} );
    
		test( 'Error - Connection error', function( done ) {
			fakes.mock( oQueue ).expects( 'add' ).once().yields();
        
			var request = { on:function(){} };
			fakes.mock( request ).expects('on').withArgs('error').yields('some error');
		    fakes.mock( Http ).expects( 'request' ).once().returns(request);
        
			var BulkLoadObject = { type: 'object', GetLookingGlassRecord: function() {}, sQueue: 'RabbitQueue' };
			fakes.mock( BulkLoadObject ).expects( 'GetLookingGlassRecord' ).once().withArgs( {id:'sfObj1'} ).yields( { id: 'id' } );
        
			new LookingGlass( oConf, oLogger, oQueue, Http, fMoment )
				.AddToQueue( [{id:'sfObj1'}], BulkLoadObject, function( error ) {
					error.should.equal( 'some error' );
					oLogger.error.called.should.be.true;
					done();	
				} );
			
			fakes.verify();
		} );
        
		test( 'Error not OK response', function( done ) {
			fakes.mock( oQueue ).expects( 'add' ).once().yields();
        
			var result = {on:function(){}};
			var resultMock = fakes.mock(result);
			resultMock.expects('on').withArgs('data').yields('NOT OK');
			resultMock.expects('on').withArgs('end').yields();
        
		    fakes.mock( Http ).expects( 'request' ).once().yields( result );		    
        
		    var BulkLoadObject = { type: 'object', GetLookingGlassRecord: function() {}, sQueue: 'RabbitQueue' };
			fakes.mock( BulkLoadObject ).expects( 'GetLookingGlassRecord' ).once().withArgs( {id:'sfObj1'} ).yields( { id: 'id' } );
        
			new LookingGlass( oConf, oLogger, oQueue, Http, fMoment, oRabbit )
				.AddToQueue( [{id:'sfObj1'}], BulkLoadObject, function( error ) {
					oLogger.error.called.should.be.true;
					done();	
				} );
			
			fakes.verify();
        
		} );
        
		test( 'Error from GetLookingGlassRecord', function( done ) {
			fakes.mock( oQueue ).expects( 'add' ).once().yields();
        
			var BulkLoadObject = { type: 'object', GetLookingGlassRecord: function() {}, sQueue: 'RabbitQueue' };
			fakes.mock( BulkLoadObject ).expects( 'GetLookingGlassRecord' ).once().withArgs( {id:'sfObj1'} ).yields( null, 'error' );
        
			new LookingGlass( oConf, oLogger, oQueue, Http, fMoment )
				.AddToQueue( [{id:'sfObj1'}], BulkLoadObject, function( error ) {
					oLogger.error.called.should.be.true;
					done();	
				} );
					
				fakes.verify();
		} );
        
        var testForbiddenRequest = function(code, done){
            fakes.mock( oQueue ).expects( 'add' ).exactly(2).yields();
        
            var aSfDataObjects = [{ Id: 'object id1', look__c: 'beautiful' },
                { Id: 'object id2', look__c: 'beautiful' } ];
        
            var aLgDataObjects = [{ id: 'object id1', look: 'beautiful' },
                { id: 'object id2', look: 'beautiful' }  ];
        
            var expectedOptions = {
                hostname: 'hostname',
                port: '9999',
                path: '/object',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength( JSON.stringify( {data:aLgDataObjects} ) ),
                    'AccessKey': 'lookinglass_accesskey99999999999000'
                }
            };
        
            var fGetLookingGlassRecordStub = sinon.stub();
            fGetLookingGlassRecordStub
                .onCall(0).yields(aLgDataObjects[0])
                .onCall(1).yields(aLgDataObjects[1])
                .onCall(2).yields(aLgDataObjects[0])
                .onCall(3).yields(aLgDataObjects[1])
        
            var BulkLoadObject = { type: 'object', GetLookingGlassRecord: fGetLookingGlassRecordStub, sQueue: 'RabbitQueue' };
            var request = { on: function(){}, write: function(){}, end: function(){} };
            var requestMock = fakes.mock(request);
            requestMock.expects( 'on' ).exactly(1).withArgs('error');
            requestMock.expects( 'write' ).exactly(1).withArgs( JSON.stringify( {data:aLgDataObjects} ) );
            requestMock.expects( 'end' ).exactly(1);
        
            var result = { on: function() {}, statusCode: code };
            var resultMock = fakes.mock( result );
            resultMock.expects( 'on' ).exactly(2).withArgs( 'data' ).yields( 'Forbidden' );
            resultMock.expects( 'on' ).exactly(2).withArgs( 'end' ).yields( );
        
            fakes.mock( Http ).expects( 'request' ).exactly(2).withArgs( expectedOptions ).returns( request ).yields( result );
        
            var oLookingGlass = new LookingGlass( oConf, oLogger, oQueue, Http, fMoment );
        
            oLookingGlass.AddToQueue( aSfDataObjects, BulkLoadObject, function( error ) {
                error.should.equal( "Failed to send records to looking glass after 2 retries" );
                oLogger.log.called.should.be.true;
                oLogger.error.called.should.be.true;
                done();
            } );
        
            fakes.verify();
        }
        
		test( 'Retries only a configurable number of times for the same 401 request', function( done ) {
            testForbiddenRequest(401, done)
		} );
        
		test( 'Retries only a configurable number of times for the same 403 request', function( done ) {
            testForbiddenRequest(403, done)
		} );
        
        
        test( 'Queues one message per record on the rabbit transient queue if the whole batch fails while sending to looking glasse', function( done ){
            fakes.mock( oQueue ).expects( 'add' ).exactly(1).yields();

            var aSfDataObjects = [{ Id: 'object id1', look__c: 'beautiful' },
                { Id: 'object id2', look__c: 'beautiful' } ];

            var aLgDataObjects = [{ id: 'object id1', look: 'beautiful' },
                { id: 'object id2', look: 'beautiful' }  ];

            var expectedOptions = {
                hostname: 'hostname',
                port: '9999',
                path: '/object',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength( JSON.stringify( {data:aLgDataObjects} ) ),
                    'AccessKey': 'lookinglass_accesskey99999999999000'
                }
            };

            var fGetLookingGlassRecordStub = sinon.stub();
            fGetLookingGlassRecordStub
                .onCall(0).yields(aLgDataObjects[0])
                .onCall(1).yields(aLgDataObjects[1])

            var BulkLoadObject = { type: 'object', GetLookingGlassRecord: fGetLookingGlassRecordStub, sQueue: 'RabbitQueue' };
            

            var result = { on: function() {} };
            var resultMock = fakes.mock( result );
            resultMock.expects( 'on' ).exactly(1).withArgs( 'data' ).yields( 'Some error other than forbidden' );
            resultMock.expects( 'on' ).exactly(1).withArgs( 'end' ).yields( );

            fakes.mock( Http ).expects( 'request' ).exactly(1).withArgs( expectedOptions ).yields( result );
            
            fakes.mock( oRabbit ).expects( 'addMessage').twice().withArgs(sinon.match.any, 'RabbitQueueTransient');

            var oLookingGlass = new LookingGlass( oConf, oLogger, oQueue, Http, fMoment, oRabbit );

            oLookingGlass.AddToQueue( aSfDataObjects, BulkLoadObject, function( error ) {
                oLogger.error.called.should.be.true;
                done();
            } );

            fakes.verify();
            
        } );


	} );

	suite( 'onAllIndexed', function( ) {
		test( 'Success Adds something to the queue', function( done ) {
		    fakes.mock( oQueue ).expects( 'add' ).once().yields();
    
			oLookingGlass  = new LookingGlass( {}, oLogger, oQueue, {} );
    
			oLookingGlass.onAllIndexed();
    
			setTimeout(function(){ 
				oLogger.info.called.should.be.true; 
				done(); 
			}, 600);
    
			fakes.verify();
		} );
    
		test( 'Success Adds to the queue a functions that returns a promise', function( ) {
		    fakes.mock( oQueue ).expects( 'add' ).once().withArgs( promisseMatch );
    
			oLookingGlass  = new LookingGlass( {}, oLogger, oQueue, {} );
    
			oLookingGlass.onAllIndexed();
    
			fakes.verify();
		} );
	} ); 
} );