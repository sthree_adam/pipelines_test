var Rabbit	= require( '../lib/Rabbit' );
var should 	= require( 'should' );
var assert	= require( 'assert' );
var sinon	= require( 'sinon' );
var nconf 	= require( 'nconf' );
var oConf 	= nconf.argv().env().file( { file: __dirname + '/../conf/config.json' } ).get();

suite( 'Unit Test for Rabbit class', function() {
	var fakes, oLogger;
	setup( function() {
		fakes = sinon.sandbox.create();
		oLogger = { info: fakes.spy(), log: fakes.spy(), error: fakes.spy() };
	} );

	teardown( function() { 
		fakes.restore();
	} );

	suite( 'Adding and receiving messages', function( ) {	

		this.timeout(5000);
		var rabbit;

		setup( function( done ) {
			rabbit = new Rabbit( oConf.rabbit, oLogger );

			rabbit.connect( function() {
				rabbit.initQueues( oConf.salesforce.queues.job, function() {
					rabbit.purgeQueue( oConf.salesforce.queues.job, function() {
						rabbit.purgeQueue( oConf.salesforce.queues.job + 'Error', function() {
							rabbit.purgeQueue( oConf.salesforce.queues.job + 'Transient', function() {
								done();
							} );
						} );
					} );
				} );
			} );
		} );

		teardown( function( done ) { 
			rabbit.disconnect();
			setTimeout( done, 500 );
		} );

		test( 'add and get messages', function( done ) {
			var messages = [ 'first message', [ 'second', 'message' ], { third: 'message' } ];
			
			for ( var i = 0; i < messages.length; i++ ) {
				rabbit.addMessage( messages[i], oConf.salesforce.queues.job, function(){} );
			}
			setTimeout( function() {
				var iMessages = 0;
				rabbit.getMessages( oConf.salesforce.queues.job, function( oMessage, fGetNext, fAck ) {
					if ( oMessage === null ) {
						iMessages.should.be.equal( messages.length );
						done();
						return;
					}
					iMessages++;
					assert.deepEqual( oMessage, messages[iMessages-1] );
					fAck();
					fGetNext();
				} );
			}, 600 );

		} );

		test( 'error messages', function( done ) {
			var messages = [ 'first message', [ 'second', 'message' ], { third: 'message' } ];
			
			for ( var i = 0; i < messages.length; i++ ) {
				rabbit.addMessage( messages[i], oConf.salesforce.queues.job, function(){} );
			}

			setTimeout( function() {
				var iMessages = 0;
				rabbit.getMessages( oConf.salesforce.queues.job, function( oMessage, fGetNext, fAck ) {
					
					if ( oMessage === null ) {
						iMessages.should.be.equal( messages.length );

						iMessages = 0;
						setTimeout( function() {
							rabbit.getMessages( oConf.salesforce.queues.job + 'Error', function( oErrorMessage, fGetNextError, fErrAck ) {
								if ( oErrorMessage === null ) {
									iMessages.should.be.equal( 1 );

									done();
									return;
								}

								iMessages++;
								assert.deepEqual( oErrorMessage.message, 'this is error' );
								assert.deepEqual( oErrorMessage.ids, { third: 'message' } );
								fErrAck();
								fGetNextError();
							} );
						}, 300 );

						return;
					}

					iMessages++;
					assert.deepEqual( oMessage, messages[iMessages-1] );
					if ( iMessages == 3 ) { 
						fAck( 'this is error' );
						fGetNext();
					} else {
						fAck();
						fGetNext();
					}
				} );
			}, 300 );

		} );

		test( 'transient messages', function( done ) {
			var messages = [ 'first message', [ 'second', 'message' ], { third: 'message' } ];

			for ( var i = 0; i < messages.length; i++ ) {
				rabbit.addMessage( messages[i], oConf.salesforce.queues.job, function(){} );
			}
			var iMessages = 0;
			setTimeout( function() {
				rabbit.getMessages( oConf.salesforce.queues.job, function( oMessage, fGetNext, fAck ) {
					if ( oMessage === null ) {
						iMessages.should.be.equal( messages.length );

						iMessages = 0;
						setTimeout( function() {
							rabbit.getMessages( oConf.salesforce.queues.job + 'Transient', function( oTransientMessage, fGetNextTransient, fTrErrAck ) {
								if ( oTransientMessage === null ) {
									iMessages.should.be.equal( 1 );

									rabbit.queueCount( oConf.salesforce.queues.job + 'Error', function( iCount ) {
										iCount.should.be.equal( 0 );
										done();
									} );

									return;
								}

								iMessages++;
								assert.deepEqual( oTransientMessage.message, 'this is transient' );
								assert.deepEqual( oTransientMessage.ids, { third: 'message' } );
								fTrErrAck();
								fGetNextTransient();
							} );
						}, 400 );

						return;

					}
					iMessages++;
					assert.deepEqual( oMessage, messages[iMessages-1] );
					if ( iMessages == 3 ) { 
						fAck( 'this is transient' );
						fGetNext();
					} else {
						fAck();
						fGetNext();
					}
				} );
			}, 300 );

		} );

		test( 'addErrorMessage - permanent error', function( done ) {

			var testMessage = { 'test': 'message'};
			rabbit.addErrorMessage( testMessage, 'this is error', oConf.salesforce.queues.job );

			var iMessages = 0;
			setTimeout( function() {
				rabbit.getMessages( oConf.salesforce.queues.job + 'Error', function( oMessage, fGetNext, fAck ) {
					if ( oMessage === null ) {
						iMessages.should.be.equal( 1 );

						rabbit.queueCount( oConf.salesforce.queues.job + 'Transient', function( iCount ) {
							iCount.should.be.equal( 0 );
							done();
						} );

						return;
					}

					iMessages++;
					assert.deepEqual( oMessage.message, 'this is error' );
					assert.deepEqual( oMessage.ids, testMessage );
					fAck();
					fGetNext();

				} );

			}, 300 );

		} );

		test( 'addErrorMessage - transient error', function( done ) {

			var testMessage = { 'test': 'transient message'};
			rabbit.addErrorMessage( testMessage, 'this is transient', oConf.salesforce.queues.job );

			var iMessages = 0;
			setTimeout( function() {
				rabbit.getMessages( oConf.salesforce.queues.job + 'Transient', function( oMessage, fGetNext, fAck ) {
					if ( oMessage === null ) {
						iMessages.should.be.equal( 1 );

						rabbit.queueCount( oConf.salesforce.queues.job + 'Error', function( iCount ) {
							iCount.should.be.equal( 0 );
							done();
						} );

						return;
					}

					iMessages++;
					assert.deepEqual( oMessage.message, 'this is transient' );
					assert.deepEqual( oMessage.ids, testMessage );
					fAck();
					fGetNext();

				} );

			}, 300 );

		} );

	} );
} );