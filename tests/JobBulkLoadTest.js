var JobBulkLoad	    = require( '../lib/JobBulkLoad' );
var should 			= require( 'should' );
var assert			= require( 'assert' );
var sinon			= require( 'sinon' );
var config 			= { get: function() { return { collections: { job: 'test_job' }, queues: { job: 'job_queue' } }; } };

suite( 'Unit Test for JobBulkLoad class', function() {
	var fakes;
    var oArgumentParser;
    
	setup( function() {
		fakes = sinon.sandbox.create();
        oArgumentParser = {getBrandCode: function(){}, getArgumentValue: function(){}, getArgumentValueDate: function(){}};
	} );

	teardown( function() { 
	} );

    var mockArgumentParser = function(sBrand, sStep, sStop, sStartDate){
        oArgumentParser.getArgumentValue = function(arg){
            if(arg === 'step') return sStep;
            if(arg === 'stop') return sStop;
        };
        oArgumentParser.getBrandCode = function(){ return sBrand }
        oArgumentParser.getArgumentValueDate = function(arg){
            if(arg === 'startDate') return sStartDate;
        };
        return oArgumentParser;
    };

	suite( 'GetSteps', function( ) {		
		test( 'Topics step', function( ) {
			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, mockArgumentParser('Brand Code', 'queryTopic', null, null ));
			var aSteps = oJobBulkLoad.GetSteps();

			aSteps[0].onRecord({Id:123, Name:'PHP'});
			assert.equal(oJobBulkLoad.oCodes[123], 'PHP');
		} );

		test( 'Address step', function( ) {
			var oMongoServer = {'AddAddressToQueue':function(){}};
			fakes.mock( oMongoServer ).expects( 'AddAddressToQueue' ).once().withArgs({Id:'addressId'}, "something", 'cache', 'test_jobBrand Code');

			var oJobBulkLoad = new JobBulkLoad( {}, oMongoServer, {}, config, mockArgumentParser('Brand Code', 'queryAddress', null, null ) );
			var aSteps = oJobBulkLoad.GetSteps();

			oJobBulkLoad.oJobAddress['addressId'] = "something"

			aSteps[0].onRecord({Id:'addressId'});
		} );

		test( 'Job Address step', function( ) {
			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, mockArgumentParser('Brand Code', 'queryJobAddress', null, null ) );
			
			oJobBulkLoad.GetSteps()[0].onRecord( { Job__c:'jobId', Address__c: 'addressId' } );

			assert( oJobBulkLoad.oJobAddress['addressId'].indexOf('jobId') !== -1 );
		} );

		test( 'Job Details step', function( ) {
			var oJob = {
				Id: 'JobId',
				title:'Job Title'
			};

            var oMongoServer = {AddToQueue: function(){}};
            fakes.mock( oMongoServer ).expects('AddToQueue').once().withArgs( oJob, 'cache', 'test_jobBrandCode_HA' );

			var oJobBulkLoad = new JobBulkLoad( {}, oMongoServer, {}, config, mockArgumentParser('BrandCode_HA', 'queryObject', null, 'StartDate-0123' ) );
			
			oJobBulkLoad.GetSteps()[0].onRecord( oJob );

			oJobBulkLoad.GetSteps()[0].query.should.containEql('StartDate-0123');
			oJobBulkLoad.GetSteps()[0].query.should.containEql('BrandCode_HA');

			fakes.verify();

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, mockArgumentParser('BrandCode_HA', 'queryObject', null, null ) );		
			oJobBulkLoad.GetSteps()[0].query.should.not.containEql('AND LastModifiedDate');
			
		} );

	} );

	suite( 'AllLoaded', function( ) {		
		test( 'Calls onAllIndexed on lookingglass', function( ) {
			var fakes 		= sinon.sandbox.create();
			
			var oLookingGlass = {'onAllIndexed':function(){}};
			fakes.mock( oLookingGlass ).expects('onAllIndexed').once();	
			var oJobBulkLoad = new JobBulkLoad( oLookingGlass, {}, {}, config, oArgumentParser );

			oJobBulkLoad.AllLoaded();
		} );
	} );

	suite( 'GetLookingGlassRecord', function( ) {
		test( 'Creates a looking glass ready object from a salesforce object', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Associated_Users__c : 'ADMZ,RBBI'
				, Owner_Alias__c : 'JMST'
				, address: {
					  Country__c:'country'
					, Postcode__c:'postcode'
					, Postcode_Location__Latitude__s: 'lat'
					, Postcode_Location__Longitude__s: 'long'
				}
                , LastModifiedDate: '2015-05-25'
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );
			
			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: 'country'
				, postcode 			: 'postcode'
				, codes				: ['code1', 'code2']
				, location          : 'lat,long'
				, consultants 		: ['ADMZ','RBBI','JMST']
                , content_updated_date: '2015/05/25'
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Handles jobs with no address', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Associated_Users__c : 'ADMZ,RBBI'
				, Owner_Alias__c : 'JMST'
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: ['ADMZ','RBBI','JMST']
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Handles jobs with no Associated Consultants', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Owner_Alias__c : 'JMST'
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: ['JMST']
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'dedupes Associated Consultants with Owner Alias', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Owner_Alias__c : 'JMST'
				, Associated_Users__c : 'JMST,RBBI'
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: ['JMST','RBBI']
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Upper cases and dedupes Associated Consultants with Owner Alias', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Owner_Alias__c : 'jmst'
				, Associated_Users__c : 'JmSt,RBbi'
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: ['JMST','RBBI']
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Strips silly white space and ignores empty stuff from consultants', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
				, Owner_Alias__c : ' '
				, Associated_Users__c : 'JmSt ,RBbi,, '
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: ['JMST','RBBI']
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );

		test( 'Handles no consultants', function( done ) {
			var oSalesforceObject = {
				  Id: 'id'
				, Brand__c: 'brand'
				, Name: 'name'
				, Business_Name__c : 'businessname'
				, Manager_Name__c : 'managername'
				, Job_Type__c: 'jobtype'
				, Status__c: 'status'
				, CreatedDate: '2015-01-20'
				, Commitment__c: 'commitment'
				, Fillability__c: 'fillability'
				, codes: ['code1', 'code2']
			};

			var oJobBulkLoad = new JobBulkLoad( {}, {}, {}, config, oArgumentParser );

			var oExpected = {
				 id 				: 'id'
				, brand_code		: 'brand'
				, description 		: 'name'
				, business_name		: 'businessname'
				, manager_name		: 'managername'
				, type 				: 'jobtype'
				, status			: 'status'
				, created_date		: '2015/01/20'
				, commitment 		: 'commitment'
				, fillability 		: 'fillability'
				, country 			: null
				, postcode 			: null
				, codes				: ['code1', 'code2']
				, consultants 		: []
                , content_updated_date: null
			};

			oJobBulkLoad.GetLookingGlassRecord( oSalesforceObject, function( oActual ) {
				assert.deepEqual( oActual, oExpected );
				done();
			} );
		} );
	} );
} );
