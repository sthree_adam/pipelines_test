#!/usr/bin/ruby.exe

unless ARGV.length == 1
  puts "Usage: gen-dns.rb DNS_SERVER"
  exit(1)
end

require 'json'

dns_server = ARGV[0]

Dir.glob( "data_bags/awssync-zone*" ).each do |entry|

  next if entry.match(/environment/)
  next if entry.match(/CVS/)

  config = JSON.parse( File.read( entry ) )

  printf( "dnscmd %s /recordadd s3ms.com %s A %s\n\n",
          dns_server,
          config["deployment-network"]["server_name"].sub( /.s3ms.com/, '' ),
          config["deployment-network"]["address"]
        )
end