# TODO

* deploy_code - possibly lock down permissions more on config as it has passwords in it

* Check production credentials and buckets (using UAT creds for now whilst we test)

* Samba data_bags and users?
  - possibly rename the samba-qa data_bag folder to samba-live and use same for QA, UAT, PROD


* Remove ['awssync']['unc-path'] and use ['deployment-network']['server_name'] + path

* Rename the following host databags to follow standard convention:
  * qa-awssync.json to awssync-qa.json
  * uat1-awssync.json to awssync-uat1.json
  * uat2-awssync.json to awssync-uat2.json

* Delete live1 and live2 databags (replaced by awssync-zone* files)

* Move release.bat and any other requirements into chef folder.

* Remove release folder

* `/home/awsync/* owned by root:root, not awssync:awssync
  Possibly use something like:

    execute "Set owner of webroot and sub folders" do
      command "chown -R #{node['apache']['user']}:#{node['apache']['group']} #{node['cat']['home']}"
    end

* The roles are currently being used like pseudo-environments.
  Need to add environments, and have only two roles: dev and prod

* Vagrantfile dev and prod builds could probably be merged together into a function

