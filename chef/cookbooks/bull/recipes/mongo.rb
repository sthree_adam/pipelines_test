#
# Cookbook Name:: bull
# Recipe:: mongo
#
# Copyright 2015, Sthree Plc
#
# All rights reserved - Do Not Redistribute
#

directory '/home/mongodb' do
  owner node['bull']['user']
  group node['bull']['group']
  mode "755"
  recursive true
  action :create
end
