#
# Cookbook Name:: bull
# Recipe:: default
#

bash "Configure npm for the proxy" do
  user "root"
  code <<-EOH
	npm config set strict-ssl false --global
	npm config set proxy http://dev-scheduler:devscripting@web.proxy.s3ms.com:8080/ --global
  EOH
end

#service 'bull' do
#  provider Chef::Provider::Service::Upstart
#  action :nothing
#end

bash "npm install modules" do
  cwd node['bull']['home']
  user "root"
  code <<-EOH
    npm install --no-bin-links
  EOH
end

directory node['bull']['log'] do
  owner node['bull']['user']
  group node['bull']['group']
  mode "755"
  recursive true
  action :create
end
