#
# Cookbook Name:: bull
# Attributes:: default
#

default['bull']['home']                 = '/home/bull'
##default['bull']['temp']                 = '/vagrant'
default['bull']['log']                  = '/var/log/bull'
default['bull']['user']                 = 'bull'
default['bull']['group']                = 'bull'
