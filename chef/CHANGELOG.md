# CHANGELOG

2015-10-15

* Test deploy using UAT credentials

* deploy_code - Deploy application code - use a resource
  - make sure it doesn't copy VM images or chef code (release and chef folders)
  - prob don't need tests either

* Remove duplication in data_bag files - should only really have deployment specifics for network and vsphere

* HTTPS Certificates - looks like they are used by the node code, so no further deployment required.

2015-10-14

* Add snmp and config for Solrwinds monitoring