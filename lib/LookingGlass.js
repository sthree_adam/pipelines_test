var Promise 	= require( 'promise' );
var url         = require( 'url'     );

var LookingGlass = function( oConf, oLogger, oQueue, oHttp, fMoment, oRabbit ) {
	this.oLogger 	  = oLogger;
	this.sUrl		  = oConf.url;
	this.sAccessKey	  = oConf.accesskey
	this.iRetryLimit  = oConf.retryLimit;
	this.oQueue 	  = oQueue;
	this.oHttp        = oHttp;
	this.iDone		  = 0;
	this.fMoment      = fMoment;
    this.oRabbit      = oRabbit;
};

LookingGlass.prototype.MakeRequest = function( oObject, aLgRecords, aOriginalRecords, fCallback, iRetries ) {
    var that = this;

    aLgRecords.map( function( record ) {
        Object.keys(record).map( function( recordKey ) {
            record[recordKey] = ( record[recordKey] === null ) ? undefined : record[recordKey];
        } );
        return record;
    } );
    
    var postBody = JSON.stringify( {data:aLgRecords} );
    var parsedUrl = url.parse( this.sUrl );
    var options = {
        hostname: parsedUrl.hostname,
        port: parsedUrl.port,
        path: '/' + oObject.type,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength( postBody ),
            'AccessKey': this.sAccessKey + this.fMoment().unix() * 1000
        }
    };
    
    var request = this.oHttp.request( options, function( result ) {
        var allData = '';
        result.on( 'data', function( data ) {
            allData += data.toString();
        } );

        result.on( 'end', function() {
            if( allData != 'OK' ) {
                if( [401,403].indexOf( result.statusCode ) !== -1 ) {
                    that.oLogger.log( 'verbose', result.statusCode + ' => Requeing batch: after ' + iRetries + " retries" );
                    that.reQueue( aOriginalRecords, oObject, fCallback, iRetries );
                } else { 
                    aOriginalRecords.forEach( function( oRecord ) {
                        that.oRabbit.addMessage( { ids : [oRecord.Id], message : allData } , oObject.sQueue + 'Transient', function () {} );
                    } ); 
                    that.oLogger.error( allData );
                    fCallback( allData );
                }
                return;
            }

            that.iDone++;
            if( that.iDone==1 || that.iDone % 1000 == 0 ) {
                that.oLogger.logQueueStats( that.iDone, that.oQueue );
            }

            fCallback();
        } );
    } );

    request.on('error', function( error ) {
        that.oLogger.error( error + ' For id: ' + aLgRecords.id );
        fCallback( error );
        return;
    } );

    request.write( postBody );
    request.end();
};

LookingGlass.prototype.AddToQueue = function ( aRecords, oObject, fCallback, iRetries ) {
    if( aRecords.length == 0 ) {
        this.oLogger.error( 'No Records retrieved from mongoDb' );
        fCallback( 'No Records retrieved from mongoDb' );
        return;
    }

    if( iRetries === undefined ) {
        iRetries = 0;
    }
    
    var that = this;

    this.oQueue.add( function() {
        return new Promise(
            function( resolve, reject ) {
                var aLgRecords = [];
                var iDone      = 0;
                var fDone = function( error ) {
                    resolve();
                    fCallback( error );
                };

                aRecords.forEach( function( oSFRecord ) {
                    oObject.GetLookingGlassRecord( oSFRecord, function( oLgRecord, oError ) {
                        if (oError) { 
                            that.oLogger.error( oError );
                        } else {
                            aLgRecords.push( oLgRecord );
                        }
                        iDone++;

                        if( iDone == aRecords.length ) {
                            if( aLgRecords.length > 0 ) {
                                that.MakeRequest( oObject
                                                , aLgRecords
                                                , aRecords
                                                , fDone
                                                , iRetries
                                                );
                            } else {
                                fDone();
                            }
                        }
                    } );
                } );
            } );
    } );
};

LookingGlass.prototype.reQueue = function( aRecords, oObject, fCallback, iRetries ) {
	iRetries++;
	if ( iRetries < this.iRetryLimit ) {
		this.AddToQueue( aRecords, oObject, fCallback, iRetries );
		return;
	}
    
	this.oLogger.error("Failed to send records to looking glass after " + iRetries + " retries");
	fCallback( "Failed to send records to looking glass after " + iRetries + " retries" );
}

LookingGlass.prototype.onAllIndexed = function( ) {
	var that = this;
	this.oQueue.add( function() {
		return new Promise( function( resolve, reject ) {
			setTimeout(function(){
				that.oLogger.info( 'All Documents queued for sending to LookingGlass' );
				that.oLogger.logMem();
			}, 500);
			resolve();
		} );
	} );
};


exports = module.exports = LookingGlass;