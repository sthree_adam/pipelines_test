var jsforce			= require( 'jsforce'	);
var Promise			= require( 'promise'	);
var fs				= require( 'fs'			);
var moment			= require( 'moment' );
var https 			= require( 'https' );
var memlog = __dirname + '/../memory_' + new Date().getTime() + '.csv';

var sFilePath = __dirname + '/../tmp/';

require('promise/lib/rejection-tracking').enable();

var BulkApi = function( oConf, oLogger, oQueue, oRequestQueue, oMongoServer, oRabbit ) {
	this.oConf 		       = oConf.get('salesforce');
	this.oConfLookingGlass = oConf.get('lookinglass');
	this.oLogger	= oLogger;
	this.oConn 		= {};
	this.oQueue     = oQueue;
	this.oRequestQueue = oRequestQueue;
	this.oCollections = {};
	this.oMongoServer = oMongoServer;
	this.oRabbit 	  = oRabbit;
	this.https 		  = https;
	this.fs 		  = fs;

	if ( this.oConf.tmpdir ) {
		sFilePath = this.oConf.tmpdir;
	}
};

BulkApi.prototype.connect = function() {
	this.oConn = new jsforce.Connection( { 'loginUrl' : this.oConf.loginurl } );
}
BulkApi.prototype.login = function() {
	return this.oConn.login( this.oConf.username , this.oConf.password + this.oConf.securitytoken );
}

BulkApi.prototype.process = function( oBulkLoadConfig ) {
	var that = this;

	return new Promise( function( methodresolve, methodreject ) {

		oBulkLoadConfig.init( function( error ) {
			if ( error ) {
				that.oLogger.error( 'Could not initialize bulk load' );
				that.oLogger.error( error );
				methodreject( error );
				return;
			}

			var aSteps = oBulkLoadConfig.GetSteps();

			that.oLogger.log( 'verbose', 'Steps to execute: ' + aSteps.map( function( val ) { return val.name; } ).join( ', ' ) );

			aSteps.forEach( function( oQueryConfig ) {
				that.oQueue.add( function() { 
					if( oQueryConfig.source == 'custom' ) {
						return that.customQuery( oQueryConfig ); 
					}

					if( oQueryConfig.source == 'salesforce' ) {
                        try {
                            var jobAndBatchId = oBulkLoadConfig.getExistingJobBatchId( oQueryConfig.batchArgName );    
                        } catch(e) {
                            that.oLogger.error( 'error', e.message);
                            that.oLogger.info( 'Will continue by loading the data from salesforce' );
                        }


                        if( jobAndBatchId && jobAndBatchId.length === 2 ) {
                            that.oLogger.info('Using existing job id and batch id combination (' + jobAndBatchId[0] + ', ' + jobAndBatchId[1] + ')');
                            return new Promise(function (resolve, reject) {
                                that.queryBatch(jobAndBatchId[0], jobAndBatchId[1], oQueryConfig, resolve, null);
                            } );
                        }
						return that.query( oQueryConfig ); 
					}

					if( oQueryConfig.source == 'mongo' ) {
						return that.mongoQuery( oQueryConfig ); 
					}

					if( oQueryConfig.source == 'rabbitmongo' ) {
						return that.rabbitQuery( oQueryConfig ); 
					}

					that.oLogger.error( 'Illegal source: ' + oQueryConfig.source );
					that.exit();
				} );
			} );

			that.oQueue.add( function() { 	
				return new Promise( function( resolve, reject ) {
					oBulkLoadConfig.AllLoaded();
					//that.oMongoServer.close();
					resolve();
					methodresolve();
				} );
			} );
		} );
	} );
};

BulkApi.prototype.customQuery = function( oQueryConfig ) {
	return new Promise( function( resolve, reject ) { 
							if ( oQueryConfig.onRecord.length > 0 ) {
								oQueryConfig.onRecord( function() {
									resolve();
								} ); 
								return;
							}

							oQueryConfig.onRecord(); 
							resolve();
						} );
};

BulkApi.prototype.rabbitQuery = function( oQueryConfig ) {
	var that = this;

	return new Promise( function( resolve, reject ) {
		that.oLogger.info( 'Bulk Loading: ' + oQueryConfig.object );
		
		var fBulkLoad = function() {
			var collection = that.oMongoServer.getCollection( 'cache', oQueryConfig.collection );
			var bFailed = false;
			

			that.oRabbit.getMessages( oQueryConfig.queue, function( aMessage, fGetNext, fAck ) {
				if ( bFailed ) return;

				var nextProcess;
				var checkQueue = function() {
					if ( bFailed ) return;
					if ( oQueryConfig.queueSize()[0] < 10 ) {
						fGetNext();
					} else {
						setTimeout( function() { checkQueue() }, 100 );
					}
				}
				
				if ( aMessage === null ) {
					that.oLogger.info( 'Finished getting all documents from rabbit queue: ' + oQueryConfig.queue );
					clearTimeout( nextProcess );
					resolve();
					return;
				}

				if ( aMessage.ids != undefined ) {
					aMessage = aMessage.ids;
				}

                var oMongoQuery = {Id: {$in: aMessage}};
                var allRecords 	= [];

                collection.find( oMongoQuery ).each( function( error, record ) {
					if ( record === null ){
                        oQueryConfig.onRecord( allRecords, function( error ) {
                            fAck( error );
                        } );
                        checkQueue();
                        return;
                    }
                    allRecords.push( record );
					if( error ) {
						that.oLogger.log( 'verbose', 'Failed while reading data from mongo. Collection: ' + oQueryConfig.collection + ' Query: '+ JSON.stringify( oMongoQuery ) );
						bFailed = true;
						that.oMongoServer.reconnect( 'cache', function() { 
							fAck( 'Failed while reading data from mongo' );
							fBulkLoad(); 
						} );
						return;
					}
				} );
			} );
		};

		fBulkLoad();
	} );
};

BulkApi.prototype.mongoQuery = function( oQueryConfig ) {
	var that 				= this;
	var iLGRabbitBatchSize 	= that.oConfLookingGlass.rabbitBatchSize[oQueryConfig.object];
	var iDone 				= 0;

	return new Promise( function( resolve, reject ) {
		that.oLogger.info( 'Bulk Loading: ' + oQueryConfig.object );
		
		var fBulkLoad = function() {
			var collection 	= that.oMongoServer.getCollection( 'cache', oQueryConfig.collection );
			var bFailed 	= false;

			collection.count( function( err, totalCount ) {
				if ( err ){
					that.oLogger.log( 'verbose', 'Warning: Failed while reading data count from mongo. Collection: ' + oQueryConfig.collection + ' Query: '+ oQueryConfig.query );

					bFailed = true;
					that.oMongoServer.reconnect( 'cache', function() { fBulkLoad(); });
					return;
				}

				that.oLogger.info( totalCount + ' of ' + oQueryConfig.object + ' records found' );

				var nextProcess;
				var checkQueue = function() {
					if ( bFailed ) return;
					nextProcess = setTimeout( function() { getResults() }, 1 );
				}

				var oQueueMonitor = setInterval( function() { that.oLogger.log( 'verbose', 'Mongo Done: ' + Math.round( iDone ) ); }, 1000 );
				var getResults = function() {
					if ( bFailed ) return;
					collection.find( oQueryConfig.query, { Id: 1 }, function( err, oCursor ) {
						oCursor.batchSize(1000);

						var allItems = [];
						var fProcessNext = function( err, oItem ) {
							if( err ) {
								that.oLogger.error( err );
								return;
							}

							if( oItem == null ) {
								that.oLogger.info( 'Finished getting all document from mongo collection: ' + oQueryConfig.collection );
								clearInterval( oQueueMonitor );
								if( allItems.length > 0 ){
									oQueryConfig.onRecord( allItems, function() {
										allItems = []
									} );
								}
								
								clearTimeout( nextProcess );
								resolve();
								return;
							}

							allItems.push(oItem.Id);

							if( allItems.length == iLGRabbitBatchSize ){
								oQueryConfig.onRecord( allItems, function() {
									allItems = []
									oCursor.nextObject( fProcessNext );
									iDone++;
								} );
							}else if( allItems.length > iLGRabbitBatchSize ){
								throw Error("Batch size already bigger than the configured max batch size");
							}else{
								oCursor.nextObject( fProcessNext );
								iDone++;
							}							
						};

						oCursor.nextObject( fProcessNext );
					} );
				};

				that.oLogger.info( 'Query Executed' );
				checkQueue();
			} );
		};
		
		fBulkLoad();
	} );
};

BulkApi.prototype.queueSfRequest = function( sFile, sJobId, endBatch, resultId ) {
	var that = this;
	var url = '/services/async/' + that.oConn.version + '/job/' + sJobId + '/batch/' + endBatch.id + '/result/' + resultId;
	var options =  {
		hostname: that.oConn.instanceUrl.replace( 'https://', '' ),
		port: 443,
		path: url,
		method: 'GET',
		headers: {
			'X-SFDC-Session': that.oConn.accessToken,
			'Accept': 'application/xml',
			'User-Agent': 'Bull processor'
		}
	}
	
	this.oRequestQueue.add( function() {
		return new Promise( function( reqResolve, reqReject ) {
			var req = that.https.request( options, function(res) {
				that.oLogger.info( 'Downloading query results' );

				res.on( 'data', function( data ) {
					that.fs.appendFileSync( sFile, data );
				} ).on( 'end', function() {
					reqResolve();
				} );
			} );

			req.end();

			req.on('error', function(e) {
			  that.oLogger.error( e );
			  process.exit( 1 );
			} );
		} );
	} );
};

BulkApi.prototype.queueFileRead = function( sFile, oQueryConfig, fResolve ) {
	var that = this;

	this.oRequestQueue.add( function() {
		new Promise( function( reqResolve, reqReject ) {
			that.oLogger.info( 'Processing query results' );

			var iDone = 0;
			var oFile = that.fs.openSync( sFile, 'r' );
			var iTotalBytesRead = 0;

			var readLine = function( bOnlyOne ) {
				var oBuffer = new Buffer(20000),
					oBytesRead = 0,
					iOffset = 0, 
					iNlPos;

				oBuffer.fill(0);

				do {
					try {
						oBytesRead = that.fs.readSync( oFile, oBuffer, iOffset, 5000, iTotalBytesRead + iOffset);
					}catch(e) {
						return null;
					}

					iOffset += oBytesRead;
					iNlPos = ( bOnlyOne ) ? oBuffer.indexOf( '\n' ) : oBuffer.lastIndexOf( 10 );

				} while ( oBytesRead > 0 && iNlPos == -1);

				iNlPos++;
				var sLine = oBuffer.toString( 'utf8', 0 , iNlPos );

				iTotalBytesRead += iNlPos;
				return sLine.trim();
			};

			var firstLine 	= readLine( true );
			var mapping 	= firstLine.replace( /[\"]*/g, '').split( ',' );

			var mapLine = function( line ) {
				var fields = line.replace( /^\"|\"$/g, '' ).replace( /([^,]{1})\"\"([^,]{1})/g, '$1(!$%)$2' ).split( '","' ),
					out = {};
				for ( var iIdx in mapping ) {
					out[mapping[iIdx]] = ( fields[iIdx] ) ? fields[iIdx].replace( /\(\!\$\%\)/g, '"' ) : null;
				}

				return out;
			};

			var nextProcess;
			var checkQueue = function() {
				if ( oQueryConfig.queueSize()[0] < 200 ) {
					nextProcess = setTimeout( function() { processLine() }, 1 );
				} else {
					setTimeout( function() { checkQueue() }, 100 );
				}
			};

			var processLine = function() {
				
				var lines = readLine();											

				if ( lines === null ) return;
				if ( lines == '' ) {
					clearTimeout( nextProcess );
					that.fs.closeSync( oFile );
					that.fs.unlink( sFile, function() {} );
					that.oLogger.info( 'Retrieved ' + iDone + ' ' + oQueryConfig.object ); 
					reqResolve(); 
					fResolve();
				}

				var aLines = lines.split( '\n' );

				for ( var i in aLines ) {

					var line = aLines[i].trim();
					if ( line != firstLine ) {

						var map = mapLine( line );
						oQueryConfig.onRecord( map );

						iDone++;
						if ( iDone % 10000 == 0 ) {		
							that.oLogger.info( 'Processed ' + iDone + ' ' + oQueryConfig.object ); 
						}
					}
				}

				checkQueue();
			};

			checkQueue();
		} );
	} );	
}

BulkApi.prototype.query = function( oQueryConfig ) {
	var that = this;

	return new Promise( function( resolve, reject ) {

		that.oLogger.info( 'Bulk Loading: ' + oQueryConfig.object );

		var job 	= that.oConn.bulk.createJob( oQueryConfig.object, 'query' );
		var batch 	= job.createBatch();

		that.oLogger.log( 'verbose', 'Querying ', oQueryConfig.query );

		batch.execute( oQueryConfig.query );

		batch.on( 'queue', function( msg ) { 
			that.oLogger.log( 'verbose', 'Job Queued ', job.id.substring( 0, 15 ) );
			batch.poll( that.oConf.bulkapi.pollfrequency * 1000, that.oConf.bulkapi.timeout * 1000 );
		} );

		batch.on( 'progress', function( response ) { 
			that.oLogger.log( 'verbose', 'Polling: ', response.state ); 
		} );

		batch.on( 'error', function( err ) { 
			that.oLogger.error( err ); 
			reject( err ); 
			job.close();
			that.exit();
		} );

		batch.on( 'response', function( resp ) {
			that.oLogger.log( 'verbose', 'Response' );		

			if( Object.keys( resp ).length === 0 ) {
			 	return;
			}
			return that.queryBatch(job.id ,batch.id, oQueryConfig, resolve, job);
		} );
	} );
};

BulkApi.prototype.queryBatch = function(sJobId, sBatchId, oQueryConfig, fResolve, oJob) {     
    var endBatch 	= this.oConn.bulk.job( sJobId ).batch( sBatchId );
    var that 		= this;

    return endBatch.retrieve( function( err, results ) {
        var resultId;

        if ( err ) {
            if(oJob){
                oJob.close();   
            }
            
            that.oLogger.error( err );
            that.exit();
            return;
        }

        var sFile = sFilePath + oQueryConfig.object + '.csv';

        that.fs.writeFileSync( sFile, '' );

        var requestQueued = 0;
        while ( requestQueued < results.length ) {
            resultId = results[requestQueued].id;

            that.queueSfRequest( sFile, sJobId, endBatch, resultId );
            requestQueued++;

            if( requestQueued == results.length ) {
                that.queueFileRead( sFile, oQueryConfig, fResolve );
            }
        }

        if(oJob){
            oJob.close();
        }
    } );
};

BulkApi.prototype.exit = function( ) {
	process.exit(1);
};
 
exports = module.exports = BulkApi;