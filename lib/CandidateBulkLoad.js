var Promise     = require( 'promise' );
var Moment      = require( 'moment' );
var BulkLoad        = require( __dirname + '/BulkLoad' );
var oAwsS3, oConfig;

var CandidateBulkLoad = function( oLookingGlass, oMongoServer, oRabbit, oArgumentParser, oPAwsS3, oXmlParser, oRollup, oPConfig ) {
    BulkLoad.call( this, oArgumentParser );
    
    this.oLookingGlass  = oLookingGlass;
    this.oCodes         = {};
    this.currentEntityId = '';
    this.aCodeCache     = [];
    this.oXmlParser     = oXmlParser;
    this.oRollup        = oRollup;
    this.oMongoServer   = oMongoServer;
    this.oRabbit        = oRabbit;
    oAwsS3              = oPAwsS3;
    oConfig             = oPConfig;
    this.sCollection    = oConfig.get( 'salesforce' ).collections[ this.type ] + this.sBrand;
    this.sQueue         = oConfig.get( 'salesforce' ).queues[ this.type ] + this.sBrand;
}

CandidateBulkLoad.prototype = Object.create(BulkLoad.prototype);

CandidateBulkLoad.prototype.type = 'candidate';

CandidateBulkLoad.prototype.init = function( fCallback ) {
    var that = this;

    this.oMongoServer.connect( 'cache', function( error ) {
        if ( error ) {
            fCallback( error );
            return;
        };
        that.oRollup.connect( function( error ) {
            if ( error ) {
                fCallback( error );
            }
            that.oRabbit.connect( function( error ) {
                if ( error ) {
                    fCallback( error );
                }
                that.oRabbit.initQueues( that.sQueue, function() {
                    fCallback();
                } );
            } );
        } );
    } );
};

CandidateBulkLoad.prototype.waitForCompletion = function( fCallback ) {
    this.oRabbit.waitForMessagesQueued( fCallback );
};

CandidateBulkLoad.prototype.GetSteps = function() {
    var that = this;
    var GetQueueSize = function() {
        return [ that.oMongoServer.oQueue.getQueueLength(), that.oMongoServer.oQueue.getPendingLength(), 'n/a' ];
    }

    var aSteps = [ { source    : 'custom'
                , name      : 'start'
                , onRecord  : function( fCallback ) {
                        that.oMongoServer.deleteCollection( 'cache', that.sCollection );
                        fCallback(); 
                    }
                }
            ,   { query     : (function(){
                              var sQuery = 'SELECT Id,Name,Country__c,Last_CV_Update__c,Brand__c,Candidate_Type__c,City__c,Available__c,Status__c,Postcode__c,Email,Real_Title__c,Job_Title_Updated__c,Postcode_Location__Latitude__s,Postcode_Location__Longitude__s,Candidate_Placement_Count__c,Candidate_Interview_Count__c,Candidate_Sendout_Count__c,Birthdate,Record_Type_Name__c,Last_CV_Source__c,Candidate_Last_Placement__c,Last_Sendout__c,Candidate_Last_Interviewed__c,Mailshot_Mode__c '+
                                           'FROM Contact WHERE Record_Type_Name__c IN (\'Candidate\') AND Brand__c = \'' + that.sBrand + '\' AND Last_CV_Update__c <> NULL ';
                              if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate;
                              return sQuery;
                              })()
                , object    : 'Contact'      
                , fileName  : 'Contact_cand' + that.sBrand
                , source    : 'salesforce'        
                , name      : 'queryObject'
                , batchArgName: 'objectBatch'
                , onRecord  : function( oCandidateDetails ) {
                    if ( oCandidateDetails && oCandidateDetails.Id ) {
                        that.oMongoServer.AddToQueue( oCandidateDetails, 'cache', that.sCollection );
                    }
                } 
                , queueSize : function() {
                        return GetQueueSize();
                    }
                }
            ,   { source    : 'custom'
                , name      : 'addObjectIndex'
                , onRecord  : function() {
                        that.oMongoServer.addIndex( 'cache', that.sCollection, 'Id' );
                    }
                }
            ,   { query     : 'SELECT Id, Name FROM Topic'
                , object    : 'Topic'
                , fileName  : 'Topic_cand' + that.sBrand
                , source    : 'salesforce'    
                , name      : 'queryTopic'
                , batchArgName: 'topicBatch'
                , onRecord  : function( oCode ) {
                                that.oCodes[oCode.Id] = oCode.Name 
                            }
                , queueSize : function() {
                        return GetQueueSize();
                    }
                }
            ,   { query     : (function(){
                        var sQuery = 'SELECT EntityId, TopicId FROM TopicAssignment ' +
                                      'WHERE EntityType = \'Contact\' '+
                                      'AND EntityId IN ( '+ 
                                         'SELECT Id '+
                                         'FROM Contact WHERE Record_Type_Name__c IN (\'Candidate\') AND Brand__c = \'' + that.sBrand + '\' AND Last_CV_Update__c <> NULL ' ;
                                         if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate + ' ';
                                         sQuery += ') ';
                        
                        sQuery += 'ORDER BY EntityId';
                        return sQuery;
                    })()
                , object    : 'TopicAssignment'
                , fileName  : 'TopicAssignment_cand' + that.sBrand
                , source    : 'salesforce'    
                , name      : 'queryTA'
                , batchArgName: 'topicAssignmentBatch'
                , onRecord  : function( oAssignment ) {
                        if ( that.currentEntityId != '' && oAssignment.EntityId != that.currentEntityId ) {
                            that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
                            that.aCodeCache = [];
                        }
                        
                        that.currentEntityId = oAssignment.EntityId;
                        that.aCodeCache.push( that.oCodes[oAssignment.TopicId] );
                    }
                , queueSize : function() {
                        return GetQueueSize();
                    }
                }
            ,   { source    : 'custom'
                , name      : 'flushTACache'
                , onRecord  : function() {
                        if ( that.currentEntityId ) {
                            that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
                        }
                    }
                }
            ,   { source    : 'custom'
                , name      : 'clearMemory'
                , onRecord  : function() {
                        that.oCodes = {};
                    }
                }
            ,   { source    : 'custom'
                , name      : 'queue'
                , onRecord  : function( fCallback ) {
                        that.oRabbit.purgeQueue( that.sQueue, function() { fCallback(); } );
                    }
                }
            ,   { query     : {}
                , object    : 'candidate'
                , source    : 'mongo'
                , name      : 'requeue'
                , brand     : that.sBrand
                , collection: that.sCollection
                , onRecord  : function( aCandidates, fDone ) {
                        that.oRabbit.addMessage( aCandidates, that.sQueue, fDone );
                    }
                }
            ,   { query     : {}
                , object    : 'candidate'
                , source    : 'rabbitmongo'
                , name      : 'process'
                , queue     : that.sQueue
                , collection: that.sCollection
                , onRecord  : function( aCandidates, fCallback ) {
                        that.oLookingGlass.AddToQueue( aCandidates, that, fCallback );
                    }
                , queueSize : function() {
                        return [ that.oLookingGlass.oQueue.getQueueLength(), that.oLookingGlass.oQueue.getPendingLength(), 'n/a' ];
                    }
                }
            ];

    var aToExecute = [], bAddSteps = false;
    for ( var i = 0; i < aSteps.length; i++ ) {
        if ( aSteps[i].name == this.sStep ) {
            bAddSteps = true;
        }

        if ( aSteps[i].name == this.sStop ) {
            bAddSteps = false;
        }

        if ( bAddSteps && !( aSteps[i].skip && aSteps[i].name != this.sStep ) ) {
            aToExecute.push( aSteps[i] );
        }
    }

    return aToExecute;
};

var getCv = function( oObject, fCallback ) {
    var sCandidateCvLocation = oObject.id + '_' + oObject.brand_code + '.' + 'xml';

    oAwsS3.getObject( { Bucket : oConfig.get( 'aws:bucket' )
                      , Key  : sCandidateCvLocation
                      }
                    , function( oError, oS3Object ) {
                        if ( oError ) {
                            fCallback( null, oError );
                            return;
                        };

                        fCallback( oS3Object.Body );
                    } );
};


var GetLookingglassDate = function( sInput ) {
    if( !sInput || isNaN( Date.parse( sInput ) ) ) {
        return null;
    }

    return Moment( new Date( sInput ) ).format( 'YYYY/MM/DD' );
};

CandidateBulkLoad.prototype.GetLookingGlassRecord = function( oCandidateDetails, fCallback ) {
    var that = this;
    var oInputRecord = { id: oCandidateDetails.Id, codes: oCandidateDetails.codes };
    oInputRecord.details = oCandidateDetails;

    var oParams =   { 'id'                    : oInputRecord.details.Id
                    , 'cv'                    : ''
                    , 'name'                  : oInputRecord.details.Name
                    , 'country'               : oInputRecord.details.Country__c
                    , 'parsing_engine'        : 'SOVREN'
                    , 'cv_last_updated'       : GetLookingglassDate(oInputRecord.details.Last_CV_Update__c)
                    , 'brand_code'            : oInputRecord.details.Brand__c
                    , 'skills'                : []
                    , 'candidate_type'        : oInputRecord.details.Candidate_Type__c
                    , 'town'                  : oInputRecord.details.City__c
                    , 'availability_date'     : GetLookingglassDate(oInputRecord.details.Available__c)
                    , 'candidate_status'      : oInputRecord.details.Status__c
                    , 'home_postcode'         : oInputRecord.details.Postcode__c
                    , 'current_job_title'     : oInputRecord.details.Real_Title__c
                    , 'last_known_job_title'  : oInputRecord.details.Real_Title__c
                    , 'cv_source'             : oInputRecord.details.Last_CV_Source__c
                    , 'job_titles'            : []
                    , 'employers'             : []
                    , 'employers_raw'         : []
                    , 'current_employer'      : ''
                    , 'current_skills'        : []
                    , 'career_start_date'     : null
                    , 'apollo_codes'          : oInputRecord.codes
                    , 'candidate_birth_date'  : GetLookingglassDate(oInputRecord.details.Birthdate)
                    , 'last_placement'        : GetLookingglassDate(oInputRecord.details.Candidate_Last_Placement__c)
                    , 'last_sendout'          : GetLookingglassDate(oInputRecord.details.Last_Sendout__c)
                    , 'last_interview'        : GetLookingglassDate(oInputRecord.details.Candidate_Last_Interviewed__c)
                    , 'mailable'              : oInputRecord.details.Mailshot_Mode__c
                    };


    if( oInputRecord.details.Postcode_Location__Latitude__s && oInputRecord.details.Postcode_Location__Longitude__s ) {
        oParams.location = oInputRecord.details.Postcode_Location__Latitude__s + ',' + oInputRecord.details.Postcode_Location__Longitude__s;
    }

    var aActivities = [];
    if ( parseInt( oInputRecord.details.Candidate_Placement_Count__c , 10 ) > 0 ) {
                    aActivities.push( 'PLACED' );
    }
    if ( parseInt( oInputRecord.details.Candidate_Sendout_Count__c, 10 ) > 0 ) {
                    aActivities.push( 'SENTOUT' );
    }
    if ( parseInt( oInputRecord.details.Candidate_Interview_Count__c, 10 ) > 0 ) {
                    aActivities.push( 'INTERVIEWED' );
    }
    oParams.candidate_activities = aActivities;

    oParams.highlight_class = ''
    if( oInputRecord.details.Status__c ) {
        if ( ['CAUTION - SEE COMMENTS','HEADHUNTED','PLACED','UNDER OFFER'].indexOf( oInputRecord.details.Status__c.toUpperCase() ) > -1 ) {
            oParams.highlight_class = 'protected';
        } else if ( ['ACCREDITED'].indexOf( oInputRecord.details.Status__c.toUpperCase() ) > -1 ) {
            oParams.highlight_class = 'accredited';
        }
    }

    getCv( oParams, function( oCv, oError ) {
        if ( oError ) {
            fCallback( null, oError.message );
            return;
        }
        
        that.oXmlParser.getParsedCv( oCv, oConfig, function( oParsedCv, oError ) {
            if ( oError ) {
                fCallback( null, oError );
                return;
            }

            oParams.cv = oParsedCv.cv;

            var timeLimit   = new Date().getTime() - ( oConfig.get( 'time_limit_for_current_employer' ) * 60000 * 60 * 24 * 30.67 );  //months to ms
            var iCvDate     = new Date( oParams.cv_last_updated ).getTime();
            var iSfDate = new Date( oInputRecord.details.Job_Title_Updated__c ).getTime();

            if ( iSfDate < timeLimit ) {
                oParams.current_job_title = '';
            }

            if ( ( iSfDate < iCvDate || oParams.current_job_title == '' ) && oParsedCv.CurrentJobTitle ) {
                oParams.current_job_title = oParsedCv.CurrentJobTitle;
            }

            if ( ( iSfDate < iCvDate || oParams.last_known_job_title == '' ) && oParsedCv.LastKnownJobTitle ) {
                oParams.last_known_job_title = oParsedCv.LastKnownJobTitle;
            }

            oParams.employers_raw    = oParsedCv.RawEmployers;
            oParams.current_employer = oParsedCv.CurrentEmployer;
            oParams.job_titles       = oParsedCv.JobTitles;
            if ( oParams.current_job_title && oParams.job_titles.indexOf( oParams.current_job_title ) == -1 ) {
                oParams.job_titles.push( oParams.current_job_title );
            }
            if ( oInputRecord.details.Real_Title__c && oParams.job_titles.indexOf( oInputRecord.details.Real_Title__c ) == -1 ) {
                oParams.job_titles.push( oInputRecord.details.Real_Title__c );
            }
            oParams.skills           = oParsedCv.Skills;
            oParams.current_skills   = oParsedCv.CurrentSkills;
            oParams.career_start_date = GetLookingglassDate( oParsedCv.CareerStartDate );

            var employerCnt = oParams.employers_raw.length;
            var employersProcessed = 0;
            var errored = false;

            if ( oParams.employers_raw.length == 0 ) {
                fCallback( oParams );
                return;
            }

            oParams.employers_raw.forEach( function( employer ) {
                that.oRollup.getEmployer( employer, function( name, error ) {
                    employersProcessed++;
                    if ( errored ) return;
                    if ( error ) {
                        errored = true;
                        fCallback( null, error );
                        return;
                    };

                    if ( name ) oParams.employers.push( name );
                    if ( oParams.current_employer == employer ) {
                        oParams.current_employer = name;
                    }

                    if( employersProcessed == employerCnt ){
                        fCallback( oParams );
                    }
                } );
            } );
        } );
    } );
};

CandidateBulkLoad.prototype.AllLoaded = function() {
    this.oLookingGlass.onAllIndexed();
};

exports = module.exports = CandidateBulkLoad;