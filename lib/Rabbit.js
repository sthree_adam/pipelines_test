var amqp 	  = require('amqplib/callback_api');
var constants = require('constants');

var Rabbit = function( oConf, oLogger ) {
	this.iUnconfirmed  	 = 0;
	this.oConf      	 = oConf;
	this.connected  	 = false;
	this.connectionWrite = null;
	this.connectionRead  = null;
	this.oLogger    	 = oLogger;
	this.oTimeout   	 = {};
	this.aQueue 		 = {};
	var trans 			 = require( __dirname + '/../conf/' + oConf.transientConfig );

	this.transientErrors = [];
	for ( var tr in trans ) {
		this.transientErrors.push( { regexp: new RegExp( '^' + tr + '$', 'i' ), isTransient: trans[tr] } );
	}
}

Rabbit.prototype.connect = function( fCallback ) {
	var that = this;
	var options = {rejectUnauthorized : false };

	if ( this.oConf.ciphers ) {
		options['ciphers'] = this.oConf.ciphers;
	}

	amqp.connect( this.oConf.write_tx_url
				, options
				, function( err, connOut ) {
					if ( err ) {
						that.oLogger.error( err ); 
  						throw err;
					}

					connOut.createConfirmChannel( function( error, channelOut ) {
						that.connectionWrite = channelOut;

						amqp.connect( that.oConf.read_rx_url
									, options
									, function( err, connIn ) {
										if ( err ) {
											that.oLogger.error( err ); 
					  						throw err;
										}

										connIn.createChannel( function( error, channelIn ) {
											that.connectionRead = channelIn;
											that.connected = true; 
						  					fCallback();
										} );

									} );

					} );

				} );
}

Rabbit.prototype.disconnect = function() {
	if ( !this.connected ) return;

	this.connectionWrite.close();
	this.connectionRead.close();
}

Rabbit.prototype.initQueues = function( sQueue, fCallback ) {
	var that = this;

	this.getQueue( sQueue, function ( q, iCount ) {
		that.getQueue( sQueue + 'Error', function ( qe, iCountE ) {
			that.getQueue( sQueue + 'Transient', function ( qt, iCountT ) {
				fCallback();
			} );
		} );
	});
};

Rabbit.prototype.getQueue = function( sQueue, fCallback, reset ) {

	if ( !this.connected ) {
		this.oLogger.error( 'Get message: No RabbitMQ connection!' ); 
		return;
	}

	if ( !reset && this.aQueue[sQueue] ) return fCallback( this.aQueue[sQueue] );

	var that = this;
	this.connectionRead.assertQueue( sQueue, { durable: true, autoDelete: false }, function ( err, ok ) {
		that.aQueue[sQueue] = ok.queue;
		fCallback( ok.queue, ok.messageCount );
	});

}

Rabbit.prototype.queueCount = function( sQueue, fCallback ) {

	this.getQueue( sQueue, function ( q, iCount ) {
		fCallback( iCount );
	}, true);

}

Rabbit.prototype.purgeQueue = function( sQueue, fCallback ) {

	this.connectionWrite.purgeQueue( sQueue, function( err, ok ) {
		fCallback();
	} );

}

Rabbit.prototype.getMessages = function( sQueue, fCallback ) {
	var that = this;

	this.connectionRead.get( sQueue, {}, function ( err, message ) {
		if ( !message ) {
			fCallback( null );
			return;
		}
			
		that.processMessage( message, fCallback, sQueue );
	} );
}

Rabbit.prototype.isTransient = function( error ) {
	if ( typeof error != 'string' ) {
		return false;
	}
	var tr = this.transientErrors;
	for ( var i in tr ) {
		if ( error.search( tr[i].regexp ) != -1 ) {
			return tr[i].isTransient;
		}
	}
	return false;
}

Rabbit.prototype.processMessage = function( message, fCallback, sQueue ) {
	var oMessage;
	var that = this;

	try {
		oMessage = JSON.parse( message.content.toString() );
	} catch( e ) {
		this.oLogger.error( e );
		return;
	}

	fCallback( oMessage
			 , function() { 
					that.getMessages( sQueue, fCallback );
			   }
			 , function( error ) {
					that.connectionRead.ack( message );

					if ( error ) {
						that.addErrorMessage( oMessage, error, sQueue );
					}
			 } );
}

Rabbit.prototype.addErrorMessage = function( oMessage, error, sQueue ) {
	if ( oMessage.ids == undefined ) {
		oMessage = { ids : oMessage, message : error };
	}
	if ( this.isTransient( error ) ) {
		this.addMessage( oMessage, sQueue + 'Transient', function() {} );
	} else {
		this.addMessage( oMessage, sQueue + 'Error', function() {} );
	}
}

Rabbit.prototype.addMessage = function( message, sQueue, fDone ) {
	var that = this;
	this.connectionWrite.sendToQueue( sQueue
							   , new Buffer( JSON.stringify( message ) )
							   , {}
							   , function( err, ok) {
                     				if (err !== null ) {
                       					that.oLogger.log( 'verbose', 'Message nacked!' );
                     				}
                     				that.iUnconfirmed--;
                     			} );
	this.iUnconfirmed++;
	if( this.iUnconfirmed > 1000 ) {
		that.oLogger.log( 'verbose', 'Waiting for rabbit to catch up: ' + this.iUnconfirmed );
		this.waitForMessagesQueued( fDone );
	} else {
		fDone();
	}

	return true;
}

Rabbit.prototype.waitForMessagesQueued = function( fCallback ) {
	this.connectionWrite.waitForConfirms( fCallback );
}

exports = module.exports = Rabbit;