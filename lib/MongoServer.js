var Promise 	= require( 'promise' );
var url         = require( 'url'     );
var MongoClient = require( 'mongodb' ).MongoClient;

var MongoServer = function( oConf, oLogger, oQueue ) {
	this.oLogger 	  = oLogger;
	this.oQueue 	  = oQueue;
	this.iDone		  = 0;
	this.oConnections = {};
	this.oConf 		  = oConf;
	this.aObjectCache = [];
	this.reconnecting = false;
};

MongoServer.prototype.connect = function( sConnection, fCallback ) {
	var that = this;
	if ( this.oConnections[sConnection] ) {
		this.close( sConnection );
		this.oConnections[sConnection] = false;
	}

	MongoClient.connect( this.oConf[ sConnection ], {}, function( error, db ) {
		if ( error ) {
			fCallback( error );
		}

		that.oConnections[ sConnection ] = { oDb: db, oCollections: {} };
		fCallback( null );
	} );
}

MongoServer.prototype.close = function( sConnection ) {
	try {
		this.oConnections[sConnection].oDb.close();
	} catch (e) {

	}
}

MongoServer.prototype.closeAll = function( sConnection ) {
	for ( var sConn in this.oConnections ) {
		this.close( sConn );
	}
}

MongoServer.prototype.callback = function( fCallback ) {
	var that = this;
	if ( this.reconnecting ) {
		setTimeout( function() { that.callback( fCallback ); }, 200 );
		return;
	}

	fCallback();
}

MongoServer.prototype.reconnect = function( sConnection, fCallback, no ) {
	var that = this;
	if ( no == undefined ) no = 0;

	if ( this.reconnecting ) {
		this.callback( fCallback );
		return;
	}

	this.reconnecting = true;
	
	this.close( sConnection );

	if ( no == 10 ) {
		this.oLogger.error( 'Unable to reconnect to Mongo db after 10 attempts' );
		process.exit(1);
	}

	this.oLogger.log( 'verbose', 'Reconnecting to mongo server. DB: ' + sConnection );
	this.connect( sConnection, function( err ) {
		if ( err ) {
			that.reconnect( no + 1, fCallback );
			return;
		}

		that.oLogger.log( 'verbose', 'Reconnected to mongo server. DB: ' + sConnection );
		that.reconnecting = false;
		fCallback();
	} );
}

MongoServer.prototype.getCollection = function( sConnection, sCollection ) {
	if ( !this.oConnections[sConnection].oCollections[sCollection] ) {
		this.oConnections[sConnection].oCollections[sCollection] = this.oConnections[sConnection].oDb.collection( sCollection );
	}
	return this.oConnections[sConnection].oCollections[sCollection];
}

MongoServer.prototype.addIndex = function( sConnection, sCollection, sIndex ) {
	var that = this,
		oCollection = this.getCollection( sConnection, sCollection ),
		options = {};

	options[sIndex] = 1;
	oCollection.ensureIndex( options, {}, function( error, indexName ) { 
		if ( error ) {
			that.oLogger.log( 'error', 'Failed while creating index for ' + sCollection );
			that.oLogger.log( 'error', error );
			process.exit(1);
		}
	});
}

MongoServer.prototype.deleteCollection = function( sConnection, sCollection ) {
	var that = this;

	this.oConnections[sConnection].oDb.dropCollection( sCollection, function( error, result ) {
		if ( error && JSON.stringify( error ).indexOf( 'ns not found') == -1 ) {
			that.oLogger.log( 'error', 'Failed while droping collection ' + sCollection );
			that.oLogger.log( 'error', error );
			process.exit(1);
		}
		that.oConnections[sConnection].oCollections[sCollection] = false;
	} );
}

MongoServer.prototype.AddToQueue = function( oDetails, sConnection, sCollection ) {
	var that = this;

	this.oQueue.add( function() {
		return new Promise(
			function( resolve, reject ) {
				that.getCollection( sConnection, sCollection ).insertMany( [ oDetails ], function( error, result ) {
					if( error ) {
						that.oLogger.log( 'error', 'Failed while inserting to mongo, cannot continue process. Collection: ' + sCollection );
						that.oLogger.log( 'error', error );
						process.exit(1);
					}
					resolve();
				});

			}
		);
	} );
};

MongoServer.prototype.AddAssignmentToQueue = function( sEntityId, aTopics, sConnection, sCollection ){
	var that = this;

	this.oQueue.add( function() {
		return new Promise(
			function( resolve, reject ) {
				that.getCollection( sConnection, sCollection ).updateOne( { Id : sEntityId }, { $set: { codes : aTopics } }, function( error, result) {
					if( error ) {
						that.oLogger.log( 'error', 'Failed while updating mongo with codes. Collection: ' + sCollection );
						that.oLogger.log( 'error', error );
					}
					resolve();
				});
			}
		);
	} );

}

MongoServer.prototype.AddAddressToQueue = function( oAddress, aJobs, sConnection, sCollection ){
	var that = this;
	this.oQueue.add( function() {
		return new Promise(
			function( resolve, reject ) {
				
				var collection = that.getCollection( sConnection, sCollection ),
					iRecords = aJobs.length,
					iDone = 0;

				for ( var i = 0; i < iRecords; i++ ) {
					collection.updateOne( { Id : aJobs[i] }, { $set: { address : oAddress } }, function(error, result) {
						if( error ) {
							that.oLogger.log( 'error', 'Failed while updating mongo with address. Collection: ' + sCollection );
							that.oLogger.log( 'error', error );
						}
						iDone++;
						if ( iDone == iRecords ) {
							resolve();
						}
					});
				}
			}
		);
	} );
}

MongoServer.prototype.onAllIndexed = function( ) {
	var that = this;
	this.oQueue.add( function() {
		return new Promise( function( resolve, reject ) {
			setTimeout(function(){
				that.oLogger.info( 'All Documents queued for sending to MongoServer' );
				that.oLogger.logMem();
			}, 500);
			resolve();
		} );
	} );
};

exports = module.exports = MongoServer;