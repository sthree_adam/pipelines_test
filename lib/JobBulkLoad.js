var Promise 		= require( 'promise' );
var Moment          = require( 'moment' );
var BulkLoad        = require( __dirname + '/BulkLoad' );

var JobBulkLoad = function( oLookingGlass, oMongoServer, oRabbit, oConfig, oArgumentParser ) {
    BulkLoad.call( this, oArgumentParser );
    
	this.oLookingGlass 	= oLookingGlass;
	this.oCodes 		= {};
	this.oMongoServer	= oMongoServer;
	this.currentEntityId = '';
	this.aCodeCache		= [];
	this.oJobAddress	= {};
	this.sCollection    = oConfig.get( 'salesforce' ).collections[ this.type ] + this.sBrand;
	this.sQueue    		= oConfig.get( 'salesforce' ).queues[ this.type ] + this.sBrand;
	this.oRabbit		= oRabbit;
}

JobBulkLoad.prototype = Object.create( BulkLoad.prototype );

JobBulkLoad.prototype.type = 'job';

JobBulkLoad.prototype.init = function( fCallback ) {
	var that = this;
	this.oMongoServer.connect( 'cache', function( error ) {
		if ( error ) {
			fCallback( error );
			return;
		};
		that.oRabbit.connect( function( error ) {
			if ( error ) {
				fCallback( error );
			}
			that.oRabbit.initQueues( that.sQueue, function() {
				fCallback();
			} );
		} );
	} );
}

JobBulkLoad.prototype.waitForCompletion = function( fCallback ) {
    this.oRabbit.waitForMessagesQueued( fCallback );
};

JobBulkLoad.prototype.GetSteps = function() {
	var that = this;
	var GetQueueSize = function() {
		return [ that.oMongoServer.oQueue.getQueueLength(), that.oMongoServer.oQueue.getPendingLength(), 'n/a' ];
	}

	var aSteps =  [	{ source	: 'custom'
				, name		: 'start'
				, onRecord 	: function( fCallback ) {
						that.oMongoServer.deleteCollection( 'cache', that.sCollection );
						fCallback(); 
					}
				}
			,	{ query 	: (function(){
					var sQuery = 'SELECT Id, Brand__c, Name, Business_Name__c, Manager_Name__c, Job_Type__c, Status__c, CreatedDate, Commitment__c, Fillability__c, Owner_Alias__c, Associated_Users__c, LastModifiedDate FROM Job__c WHERE Brand__c = \'' + that.sBrand + '\' ';
					if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate;
					return sQuery;
				})()			
			    , object    : 'Job__c'
                , fileName  : 'Job' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryObject'
                , batchArgName: 'objectBatch'
				, onRecord	: function( oJobDetails ) {
						if ( oJobDetails && oJobDetails.Id ) {
							that.oMongoServer.AddToQueue( oJobDetails, 'cache', that.sCollection );
						}
                	}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ source	: 'custom'
				, name		: 'queryObjectEnd'
				, onRecord 	: function() {
						that.oMongoServer.addIndex( 'cache', that.sCollection, 'Id' );
					}
				}
			,	{ query 	: 'SELECT Id, Name FROM Topic'
	            , object    : 'Topic'
                , fileName  : 'Topic' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryTopic'
                , batchArgName: 'topicBatch'
				, onRecord	: function( oCode ) {
						that.oCodes[oCode.Id] = oCode.Name 
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ query 	: (function(){
								var sQuery = 'SELECT EntityId, TopicId FROM TopicAssignment ' +
									          'WHERE EntityType = \'Job__c\' '+
									          'AND EntityId IN ( '+ 
									          	 'SELECT Id '+
									          	 'FROM Job__c WHERE Brand__c = \'' + that.sBrand + '\' ' ;
												 if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate + ' ';
									          	 sQuery += ') ';
								
							    sQuery += 'ORDER BY EntityId';
							    return sQuery;
							   })()
			    , object    : 'TopicAssignment'
                , fileName  : 'TopicAssignmentJob' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryTA'
                , batchArgName: 'topicAssignmentBatch'
				, onRecord 	: function( oAssignment ) {
						if ( that.currentEntityId != '' && oAssignment.EntityId != that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
							that.aCodeCache = [];
						}
						
						that.currentEntityId = oAssignment.EntityId;
						that.aCodeCache.push( that.oCodes[oAssignment.TopicId] );
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ source	: 'custom'
				, name		: 'flushTACache'
				, onRecord 	: function() {
						if ( that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
						}
					}
				}
			,	{ source	: 'custom'
				, name		: 'clearMemory'
				, onRecord 	: function() {
						that.oCodes = {};
					}
				}
			,	{ query 	: (function(){
					var sQuery = 'SELECT Job__c, Address__c FROM Job_Address__c WHERE Is_Primary__c = true AND Brand__c = \'' + that.sBrand + '\' ';
					if( that.sStartDate ) sQuery += 'AND Job__c IN (SELECT Id FROM Job__c WHERE LastModifiedDate >= ' + that.sStartDate + ' AND Brand__c = \'' + that.sBrand + '\' )';
					return sQuery;
				})()
			    , object    : 'Job_Address__c'
                , fileName  : 'Job_Address' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryJobAddress'
                , batchArgName: 'jobAddressBatch'
				, onRecord	: function( oJobAddress ) {
						if ( !that.oJobAddress[oJobAddress.Address__c] ) {
							that.oJobAddress[oJobAddress.Address__c] = [];
						}
						that.oJobAddress[oJobAddress.Address__c].push( oJobAddress.Job__c );
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ query 	: 'SELECT Id, Country__c, Postcode_Location__Latitude__s, Postcode_Location__Longitude__s, Postcode__c FROM Address__c WHERE Id IN ( SELECT Address__c FROM Job_Address__c WHERE Is_Primary__c = true AND Brand__c = \'' + that.sBrand + '\')'
			    , object    : 'Address__c'
                , fileName  : 'Address' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryAddress'
                , batchArgName: 'addressBatch'
				, onRecord 	: function( oAddress ) {
						if( that.oJobAddress[oAddress.Id] ){
							that.oMongoServer.AddAddressToQueue( oAddress, that.oJobAddress[oAddress.Id], 'cache', that.sCollection );
						}						
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ source	: 'custom'
				, name		: 'clearMemoryJob'
				, onRecord 	: function() {
						that.oJobAddress = {};
					}
				}
			,	{ source	: 'custom'
				, name		: 'queue'
				, onRecord 	: function( fCallback ) {
						that.oRabbit.purgeQueue( that.sQueue, function() { fCallback(); } );
					}
				}
			,	{ query 	: {}
			    , object    : 'job'
                , source	: 'mongo'
				, name		: 'requeue'
                , brand 	: that.sBrand
                , collection: that.sCollection
				, onRecord 	: function( aJobs, fDone ) {
						that.oRabbit.addMessage( aJobs, that.sQueue, fDone );
					}
				}
			,	{ query 	: {}
			    , object    : 'job'
                , source	: 'rabbitmongo'
				, name		: 'process'
				, queue 	: that.sQueue
                , collection: that.sCollection
				, onRecord 	: function( aJobs, fCallback ) {
						that.oLookingGlass.AddToQueue( aJobs, that, fCallback );
					}
				, queueSize : function() {
						return [ that.oLookingGlass.oQueue.getQueueLength(), that.oLookingGlass.oQueue.getPendingLength(), 'n/a' ];
					}
				}
			];

	var aToExecute = [], bAddSteps = false;
    for ( var i = 0; i < aSteps.length; i++ ) {
        if ( aSteps[i].name == this.sStep ) {
            bAddSteps = true;
        }

        if ( aSteps[i].name == this.sStop ) {
            bAddSteps = false;
        }

        if ( bAddSteps && !( aSteps[i].skip && aSteps[i].name != this.sStep ) ) {
            aToExecute.push( aSteps[i] );
        }
    }

    return aToExecute;
};

var GetLookingglassDate = function( sInput ) {
	if( !sInput || isNaN( Date.parse( sInput ) ) ) {
		return null;
	}
	return Moment(new Date(sInput)).format('YYYY/MM/DD');
}

var ParseConsultants = function( oInputRecord ) {
	var aConsultants = [];
	if( oInputRecord.Associated_Users__c ) {
		aConsultants = oInputRecord.Associated_Users__c.split( ',' );
	}
	if( oInputRecord.Owner_Alias__c ) {
		aConsultants.push( oInputRecord.Owner_Alias__c );
	}

	var aNormalisedConsultants = [];
	aConsultants.forEach( function( sConsultantCode ) {
		var sCode = sConsultantCode.toUpperCase().trim();
		if( sCode.length > 0 && aNormalisedConsultants.indexOf( sCode ) == -1 ) {
			aNormalisedConsultants.push( sCode );
		}
	} );
	return aNormalisedConsultants;
};

JobBulkLoad.prototype.GetLookingGlassRecord = function( oJobDetails, fCallback ) {
	var oInputRecord = { id: oJobDetails.Id, address: oJobDetails.address, codes: oJobDetails.codes };
	oInputRecord.details = oJobDetails;
	
	var oParams 	= {
		  id 				: oInputRecord.id
		, brand_code		: oInputRecord.details.Brand__c
		, description 		: oInputRecord.details.Name
		, business_name		: oInputRecord.details.Business_Name__c
		, manager_name		: oInputRecord.details.Manager_Name__c
		, type 				: oInputRecord.details.Job_Type__c
		, status			: oInputRecord.details.Status__c
		, created_date		: GetLookingglassDate(oInputRecord.details.CreatedDate)
		, commitment 		: oInputRecord.details.Commitment__c
		, fillability 		: oInputRecord.details.Fillability__c
		, country 			: (oInputRecord.address) ? oInputRecord.address.Country__c  : null
		, postcode 			: (oInputRecord.address) ? oInputRecord.address.Postcode__c : null
		, codes				: oInputRecord.codes
		, consultants 		: ParseConsultants( oJobDetails )
        , content_updated_date : (oInputRecord.details.LastModifiedDate) ? GetLookingglassDate(oInputRecord.details.LastModifiedDate) : null
	};

	if( oInputRecord.address && oInputRecord.address.Postcode_Location__Latitude__s && oInputRecord.address.Postcode_Location__Longitude__s ) {
		oParams.location = oInputRecord.address.Postcode_Location__Latitude__s + ',' + oInputRecord.address.Postcode_Location__Longitude__s;
	}

	fCallback( oParams );
};

JobBulkLoad.prototype.AllLoaded = function() {
	this.oLookingGlass.onAllIndexed();
};

exports = module.exports = JobBulkLoad;