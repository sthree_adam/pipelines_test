var nconf             = require( 'nconf' );
var winston			  = require( 'winston' );
var Queue 			  = require( 'promise-queue' );
var Promise			  = require( 'promise' );
var Moment            = require( 'moment' );
var Http              = require( 'http' );
var Https             = require( 'https' );
var url               = require( 'url' );
var AWSSdk 			  = require( 'aws-sdk' );
var BulkApi 		  = require( __dirname + '/BulkApi' );
var JobBulkLoad 	  = require( __dirname + '/JobBulkLoad' );
var ManagerBulkLoad	  = require( __dirname + '/ManagerBulkLoad' );
var CandidateBulkLoad = require( __dirname + '/CandidateBulkLoad' );
var BusinessBulkLoad  = require( __dirname + '/BusinessBulkLoad' );
var LookingGlass 	  = require( __dirname + '/LookingGlass' );
var MongoServer 	  = require( __dirname + '/MongoServer' );
var Rabbit 	  		  = require( __dirname + '/Rabbit' );
var oXmlCvParser	  = require( __dirname + '/XmlCvParser' );
var Rollup	  		  = require( __dirname + '/Rollup' );

var BulkLoadFactory = function( sConfFile ){
	this.sConfFile = sConfFile;
	this.oConf     = null;
	this.oRollup   = null;
}

BulkLoadFactory.prototype.newBulkApi = function(){
	var oBulkApi = new BulkApi( this.getConf()
					  , this.getLogger()
					  , this.newQueue(1)
					  , this.newQueue(1)
					  , this.getMongo()
					  , this.getRabbit()
					  );

	oBulkApi.connect();
	return oBulkApi;
}

Queue.configure( Promise );

BulkLoadFactory.prototype.newQueue = function( iConcurrentProcesses ){		
	return new Queue( iConcurrentProcesses, Infinity );
};

BulkLoadFactory.prototype.getConf = function(){
	if(!this.oConf){
		this.oConf = nconf.argv().env().file( { file: this.sConfFile } );
	}
	return this.oConf;
};

BulkLoadFactory.prototype.getLogger = function(){
	if(!this.oLogger){
		this.oLogger = new (winston.Logger)({
	    	transports: [ new (winston.transports.Console)( this.getConf().get( 'logger' ) ) ]
		} );
		this.oLogger.logMem = function() {
			this.info( 'Memory Used: ' + Math.ceil( process.memoryUsage().heapTotal / ( 1024 * 1024 ) ) + 'Mb' );
		};

		this.oLogger.logMemStats = function( iDone, oQueue ) {
			this.log( 'debug', 'heapTotal: ' + Math.round(process.memoryUsage().heapTotal / ( 1024 * 1024 ) ) + 'Mb' );
			this.log( 'debug', 'heapUsed:  ' + Math.round(process.memoryUsage().heapUsed  / ( 1024 * 1024 ) ) + 'Mb' );
			this.log( 'debug', 'rss:       ' + Math.round(process.memoryUsage().rss       / ( 1024 * 1024 ) ) + 'Mb' );
		};

		this.oLogger.logQueueStats = function( iDone, oQueue ) {
			this.log( 'verbose', 'Done: ' + Math.round( iDone ) );
			this.log( 'debug'   , 'Queue Length:  ' + oQueue.getQueueLength() );
			this.log( 'debug'   , 'Queue Pending: ' + oQueue.getPendingLength() );
		};
	}
	return this.oLogger;
}

BulkLoadFactory.prototype.newBulkLoad = function( oArgumentParser ){
	var that   = this;
    var rabbit = this.getRabbit();
    var conf   = this.getConf();

	switch( oArgumentParser.getBulkLoadType().toLowerCase() ){
		case 'job':
			return new JobBulkLoad( that.newLookingGlass(), this.getMongo(), rabbit, conf, oArgumentParser );
		case 'business':
			return new BusinessBulkLoad( that.newLookingGlass(), this.getMongo(), rabbit, conf, oArgumentParser );
		case 'manager':
			return new ManagerBulkLoad( that.newLookingGlass(), this.getMongo(), rabbit, conf, oArgumentParser );
		case 'candidate':
			return new CandidateBulkLoad( that.newLookingGlass()
										, this.getMongo()
										, rabbit
										, oArgumentParser
										, new AWSSdk.S3( conf.get( 'aws' ) )
										, oXmlCvParser
										, this.getRollup()
										, conf
										);
		default: throw new Error( 'Invalid bulk load type: ' + sType );
	}	
}

BulkLoadFactory.prototype.newLookingGlass = function( ){
	var oLookingGlassConf = this.getConf().get('lookinglass');

	var oHttp;
	if( url.parse(oLookingGlassConf.url).protocol === 'https:' ){
		oHttp = this.getHttps();
	}else{
		oHttp = this.getHttp();
	}

	return new LookingGlass( oLookingGlassConf
                           , this.getLogger()
						   , this.newQueue( oLookingGlassConf.concurrentrequests )
						   , oHttp
						   , Moment
                           , this.getRabbit()
						   );
}

BulkLoadFactory.prototype.getRollup = function( fCallback ) {
	if ( this.oRollup ) return this.oRollup;
	var conf = this.getConf().get('rollup');

	this.oRollup = new Rollup( conf, this.getMongo() );
		
	return this.oRollup;
}

BulkLoadFactory.prototype.getRabbit = function() {
	if ( this.oRabbit ) return this.oRabbit;
	var conf = this.getConf().get('rabbit');
	
	this.oRabbit = new Rabbit( conf
							 , this.getLogger() );
	return this.oRabbit;
}

BulkLoadFactory.prototype.getMongo = function() {
	if ( this.oMongo ) return this.oMongo;
	var conf = this.getConf().get('mongo');
	
	this.oMongo = new MongoServer( conf
								 , this.getLogger()
						   		 , this.newQueue( conf.concurrentrequests ) );
	return this.oMongo;
}

BulkLoadFactory.prototype.getHttp = function(){
	return Http;
}

BulkLoadFactory.prototype.getHttps = function(){
	return Https;
}

exports = module.exports = BulkLoadFactory;