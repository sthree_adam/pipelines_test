var oConf, rollupConf, sSuffixes, oMongo;

String.prototype.ctrim = function ( sChars ) {
    if ( !sChars ) {
        return this.trim();
    }
    return this.replace( new RegExp( '^[' + sChars + ']+|[' + sChars + ']+$', 'g' ), '' );
};

 var generateKey = function( sValue ) {
    res = sValue.toLowerCase(); 
    res = replaceAndStripWithSeparator( res, '~' );   
    res = stripSuffixes( res, '\\s' );        
    return stripWhiteSpace( res );
}

 var replaceAndStripWithSeparator = function( sValue, sSeparator ){
    sResult = replacePunctuation( sValue, sSeparator ).ctrim( sSeparator + " \n\r\t" );
    sResult = stripSuffixes( sResult, '"'+ sSeparator +'"' ).ctrim( sSeparator + " \n\r\t" );
    sResult = stripSuffixes( sResult, '\\s' ).ctrim( sSeparator + " \n\r\t" );
    sResult = stripSuffixes( sResult, '"'+ sSeparator +'"' ).ctrim( sSeparator + " \n\r\t" );
    return replacePunctuation( sResult, '' );
}

var replacePunctuation = function( sValue, sSeparator ) {
    return sValue.replace( /[^\u00BF-\u1FFF\u2C00-\uD7FF\w\d\s]+/g, sSeparator ).trim();
}

var stripWhiteSpace = function( sValue ) {
    return sValue.replace( /\s+/g, '' ).trim();
}

var stripSuffixes = function( sValue, sSeparator  ) {
    var sRegEx = '([' + sSeparator + ']+(' + sSuffixes + '))+$';
    return sValue.replace( new RegExp( sRegEx, "gi"), '' ).trim();
}

var Rollup = function( config, oMongoServer ) {
    oConf       = config;
    rollupConf  = require( __dirname + '/../conf/' + oConf.config + '.json' );
    sSuffixes   = rollupConf.country.join( '|' ) + '|' + rollupConf.company.join( '|' );
    oMongo      = oMongoServer;
}

Rollup.prototype.connect = function( fCallback ) {
    oMongo.connect( 'rollup', function( error ) {
        fCallback( error );
    } );
}

Rollup.prototype.getKey = function( sRawEmployer ) {
    return generateKey( sRawEmployer );
}

Rollup.prototype.reconnect = function( sRawEmployer, fCallback ) {
    var that = this;
    oMongo.reconnect( 'rollup', function() { 
        that.getEmployer( sRawEmployer, fCallback ); 
    });
}

Rollup.prototype.getEmployer = function( sRawEmployer, fCallback ) {
    var sKey = generateKey( sRawEmployer );
    var that = this;
    var oOverride = oMongo.getCollection( 'rollup', oConf.collections.override );
    oOverride.findOne( { key: sKey }, {}, function( err, doc ) {
        if ( err ) {
            that.reconnect( sRawEmployer, fCallback );
            return;
        }

        if ( doc && doc.name ) {
            fCallback( doc.name, null );
            return;
        };
        var oEmployer = oMongo.getCollection( 'rollup', oConf.collections.employer );
        oEmployer.findOne( { key: sKey }, {}, function( err, doc ) {
            if ( err ) {
                that.reconnect( sRawEmployer, fCallback );
                return;
            }

            if ( doc && doc.name ) {
                fCallback( doc.name, null );
                return;
            };

            fCallback( sRawEmployer );
        });
    });
}

exports = module.exports = Rollup;
    