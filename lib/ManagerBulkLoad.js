var Promise 		= require( 'promise'  );
var Moment          = require( 'moment'   );
var BulkLoad        = require( __dirname + '/BulkLoad' );

var ManagerBulkLoad = function( oLookingGlass, oMongoServer, oRabbit, oConfig, oArgumentParser ) {
    BulkLoad.call( this, oArgumentParser );
    
	this.oLookingGlass 	= oLookingGlass;
	this.oMongoServer	= oMongoServer;
	this.oCodes 		= {};
	this.currentEntityId = '';
	this.aCodeCache		= [];
	this.sCollection    = oConfig.get( 'salesforce' ).collections[ this.type ] + this.sBrand;
    this.sQueue    		= oConfig.get( 'salesforce' ).queues[ this.type ] + this.sBrand;
	this.oRabbit		= oRabbit;
}

ManagerBulkLoad.prototype = Object.create( BulkLoad.prototype );

ManagerBulkLoad.prototype.type = 'manager';

ManagerBulkLoad.prototype.init = function( fCallback ) {
	var that = this;
	this.oMongoServer.connect( 'cache', function( error ) {
		if ( error ) {
			fCallback( error );
			return;
		};
		that.oRabbit.connect( function( error ) {
			if ( error ) {
				fCallback( error );
			}
			that.oRabbit.initQueues( that.sQueue, function() {
				fCallback();
			} );
		} );
	} );
}

ManagerBulkLoad.prototype.waitForCompletion = function( fCallback ) {
    this.oRabbit.waitForMessagesQueued( fCallback );
};

ManagerBulkLoad.prototype.GetSteps = function() {
	var that = this;
	var GetQueueSize = function() {
		return [ that.oMongoServer.oQueue.getQueueLength(), that.oMongoServer.oQueue.getPendingLength(), 'n/a' ];
	}

	var aSteps = [ { source	: 'custom'
				, name		: 'start'
				, onRecord 	: function( fCallback ) {
						that.oMongoServer.deleteCollection( 'cache', that.sCollection );
						that.oRabbit.purgeQueue( that.sQueue, function() { 
							fCallback(); 
						} );
					}
				}
			,	{ query 	: (function() { 
								var sQuery = 'SELECT Id, Name, Country__c, Brand__c, Status__c, Postcode__c, Email, Job_Title__c, Real_Title__c, Postcode_Location__Latitude__s, Postcode_Location__Longitude__s, Work_Phone__c, Business_Name__c, Business_Industry__c, Last_Contact__c, Last_Email__c, Last_Meeting__c, CreatedDate, Owner_Alias__c, County__c, Manager_Placement_Count__c, Manager_Interview_Count__c, Record_Type_Name__c, LastModifiedDate, Mailshot_Mode__c, Manager_Last_Placement__c, Manager_Last_Interview__c, Manager_Last_Job__c '+
							  				 'FROM Contact WHERE Record_Type_Name__c IN (\'Manager\',\'Protected Manager\') AND Brand__c = \'' + that.sBrand + '\' '; 
							  	if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate;
							  	return sQuery;
							  } )()
                , object    : 'Contact'
                , fileName  : 'Contact' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryObject'
                , batchArgName: 'objectBatch'
				, onRecord	: function( oManagerDetails ) {
                        if ( oManagerDetails && oManagerDetails.Id ) {
							that.oMongoServer.AddToQueue( oManagerDetails, 'cache', that.sCollection );
						}
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ source	: 'custom'
				, name		: 'addObjectIndex'
				, onRecord 	: function() {
						that.oMongoServer.addIndex( 'cache', that.sCollection, 'Id' );
					}
				}
			,	{ query 	: 'SELECT Id, Name FROM Topic'
	            , object    : 'Topic'
                , fileName  : 'Topic' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryTopic'
                , batchArgName: 'topicBatch'
				, onRecord	: function( oCode ) {
						that.oCodes[oCode.Id] = oCode.Name 
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ query 	: (function(){
								var sQuery = 'SELECT EntityId, TopicId FROM TopicAssignment ' +
									          'WHERE EntityType = \'Contact\' '+
									          'AND EntityId IN ( '+ 
									          	 'SELECT Id '+
									          	 'FROM Contact WHERE Record_Type_Name__c IN (\'Manager\',\'Protected Manager\') AND Brand__c = \'' + that.sBrand + '\' ' ;
												 if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate + ' ';
									          	 sQuery += ') ';
								
							    sQuery += 'ORDER BY EntityId';
							    return sQuery;
							   })()
			    , object    : 'TopicAssignment'
                , fileName  : 'TopicAssignment' + that.sBrand
                , source	: 'salesforce'
				, name		: 'queryTA'
                , batchArgName: 'topicAssignmentBatch'
				, onRecord 	: function( oAssignment ) {
						if ( that.currentEntityId != '' && oAssignment.EntityId != that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
							that.aCodeCache = [];
						}
						
						that.currentEntityId = oAssignment.EntityId;
						that.aCodeCache.push( that.oCodes[oAssignment.TopicId] );
					}
				, queueSize : function() {
						return GetQueueSize();
					}
				}
			,	{ source	: 'custom'
				, name		: 'flushTACache'
				, onRecord 	: function() {
						if ( that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
						}
					}
				}
			,	{ source	: 'custom'
				, name		: 'clearMemory'
				, onRecord 	: function() {
						that.oCodes = {};
					}
				}
			,	{ source	: 'custom'
				, name		: 'queue'
				, onRecord 	: function( fCallback ) {
						that.oRabbit.purgeQueue( that.sQueue, function() { fCallback(); } );
					}
				}
			,	{ query 	: {}
			    , object    : 'manager'
                , source	: 'mongo'
				, name		: 'requeue'
                , brand 	: that.sBrand
                , collection: that.sCollection
				, onRecord 	: function( aManagers, fDone ) {
						that.oRabbit.addMessage( aManagers, that.sQueue, fDone );
					}
				}
			,	{ query 	: {}
			    , object    : 'manager'
                , source	: 'rabbitmongo'
				, name		: 'process'
				, queue 	: that.sQueue
                , collection: that.sCollection
				, onRecord 	: function( aManagers, fCallback ) {
						that.oLookingGlass.AddToQueue( aManagers, that, fCallback );
					}
				, queueSize : function() {
						return [ that.oLookingGlass.oQueue.getQueueLength(), that.oLookingGlass.oQueue.getPendingLength(), 'n/a' ];
					}
				}
			];

	var aToExecute = [], bAddSteps = false;
    for ( var i = 0; i < aSteps.length; i++ ) {
        if ( aSteps[i].name == this.sStep ) {
            bAddSteps = true;
        }

        if ( aSteps[i].name == this.sStop ) {
            bAddSteps = false;
        }

        if ( bAddSteps && !( aSteps[i].skip && aSteps[i].name != this.sStep ) ) {
            aToExecute.push( aSteps[i] );
        }
    }

    return aToExecute;
};

ManagerBulkLoad.prototype.GetLookingGlassRecord = function( oManagerDetails, fCallback ){
	if ( !oManagerDetails || !oManagerDetails.Id ) {
		fCallback( null, 'Incorrect data!' );
		return;
	}

	var oFromRecord = { id: oManagerDetails.Id, codes: oManagerDetails.codes };
	oFromRecord.details = oManagerDetails;

	var oParams = {
		  'id': oFromRecord.id
		, 'name': oFromRecord.details.Name
		, 'brand_code': oFromRecord.details.Brand__c
		, 'email': oFromRecord.details.Email
		, 'work_phone': oFromRecord.details.Work_Phone__c
		, 'status': oFromRecord.details.Status__c
		, 'business_name': oFromRecord.details.Business_Name__c
		, 'business_industry': oFromRecord.details.Business_Industry__c
		, 'country': oFromRecord.details.Country__c
		, 'last_met_date': GetLookingglassDate(oFromRecord.details.Last_Meeting__c)
		, 'last_emailed_date': GetLookingglassDate(oFromRecord.details.Last_Email__c)
		, 'last_contacted_date': GetLookingglassDate(oFromRecord.details.Last_Contact__c)
		, 'entered_date': GetLookingglassDate(oFromRecord.details.CreatedDate)
		, 'apollo_codes': oFromRecord.codes
		, 'consultant_ownership': [oFromRecord.details.Owner_Alias__c]
		, 'postcode': oFromRecord.details.Postcode__c
		, 'job_title': oFromRecord.details.Job_Title__c
		, 'real_job_title': oFromRecord.details.Real_Title__c
		, 'county': oFromRecord.details.County__c
		, 'content_updated_date': GetLookingglassDate(oFromRecord.details.LastModifiedDate)
        , 'mailshot_mode' : oFromRecord.details.Mailshot_Mode__c
        , 'last_placement_date' : GetLookingglassDate( oFromRecord.details.Manager_Last_Placement__c )
        , 'last_interview_date' : GetLookingglassDate( oFromRecord.details.Manager_Last_Interview__c )
        , 'last_job_date' : GetLookingglassDate( oFromRecord.details.Manager_Last_Job__c )
	};

	if( oFromRecord.details.Postcode_Location__Latitude__s && oFromRecord.details.Postcode_Location__Longitude__s ) {
		oParams.location = oFromRecord.details.Postcode_Location__Latitude__s + ',' + oFromRecord.details.Postcode_Location__Longitude__s;
	}

	var aActivities 	= [];
	if( parseInt(oFromRecord.details.Manager_Placement_Count__c) ){
		aActivities.push('PLACED WITH');
	}
	if( parseInt(oFromRecord.details.Manager_Interview_Count__c) ){
		aActivities.push('INTERVIEWED WITH');
	}
	oParams.activities = aActivities;

	fCallback( oParams );
};

var GetLookingglassDate = function(sInput){
	if(!sInput || isNaN(Date.parse(sInput))){
		return null;
	}

    return Moment(new Date(sInput)).format('YYYY/MM/DD');
}

ManagerBulkLoad.prototype.AllLoaded = function() {
	this.oLookingGlass.onAllIndexed();
};

exports = module.exports = ManagerBulkLoad;