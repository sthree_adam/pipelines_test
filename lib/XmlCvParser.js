var xml2js 	= require( 'xml2js' )
	moment  = require( 'moment' );

var oXmlParser = {};
var iCurrentTime = null;

var getCurrentTime = function() {
	return ( iCurrentTime ) ? iCurrentTime : new Date().getTime();
}

var parseDate = function( xmlDate ) {
	var sDate;

	if ( !xmlDate ) {
		return null;
	}

	if ( typeof xmlDate == 'number') {
		return xmlDate;
	}

	if ( typeof xmlDate == 'string' ) {
		sDate = xmlDate;
	}

	if ( xmlDate.AnyDate ) {
		sDate = xmlDate.AnyDate;
	}

	if ( xmlDate.YearMonth ) {
		sDate = xmlDate.YearMonth + '-01';
	}

	if ( xmlDate.Year ) {
		sDate = xmlDate.Year + '-01-01';
	}

	if ( xmlDate.StringDate ) {
		return getCurrentTime();
	}

	return new Date( sDate.toString().replace( /\-/g, '/' ) ).getTime();
}

var checkDate = function( xmlDate, iLimit ) {
	return ( getCurrentTime() - parseDate( xmlDate ) ) < iLimit;
}

exports.setCurrentTime = function( iCurrTime ) {
	iCurrentTime = ( iCurrTime ) ? iCurrTime : null;
}

exports.getParsedCv = function( oCv, oConfig, fCallback ) {
	xml2js.parseString( oCv.toString( 'utf8' ), function ( err, oParsed ) {
    	if ( err || !oParsed ) {
    		fCallback( null, 'Broken xml!' );
    		return;
    	}

    	if ( !oParsed.Resume || !oParsed.Resume.$ || !oParsed.Resume.$["xmlns:sov"] ) {
    		fCallback( null, 'Wrong xml cv type!' );
    		return;
    	}

    	var msInMonth = 60000 * 60 * 24 * 30.67;
    	var skillTimeLimit = oConfig.get( 'time_limit_for_current_skills' ) * msInMonth;  
    	var employerTimeLimit = oConfig.get( 'time_limit_for_current_employer' ) * msInMonth; 

    	var oStructured = oParsed.Resume.StructuredXMLResume[0];
    	var oParsedCv = {
    		  Skills: []
    		, CurrentSkills: []
    		, JobTitles: []
    		, CurrentJobTitle: null
    		, LastKnownJobTitle: null
    		, RawEmployers: []
    		, Employers: []
    		, CurrentEmployer: null
    		, CareerStartDate: null
    		, cv: ''
    	};

    	if ( oStructured.Qualifications
    	   && oStructured.Qualifications[0]
    	   && oStructured.Qualifications[0].Competency ) {
    		var oCompetency = oStructured.Qualifications[0].Competency;
    		for ( var iIdx in oCompetency ) {
    			var name = oCompetency[iIdx].$.name;

    			if ( oParsedCv.Skills.indexOf( name ) == -1 ) {

    				oParsedCv.Skills.push( name );
    			}

    			if ( oParsedCv.CurrentSkills.indexOf( name ) == -1
    			   && oCompetency[iIdx].CompetencyEvidence 
    			   && oCompetency[iIdx].CompetencyEvidence[0] 
    			   && oCompetency[iIdx].CompetencyEvidence[0].$ 
    			   && oCompetency[iIdx].CompetencyEvidence[0].$.lastUsed 
    			   && checkDate( oCompetency[iIdx].CompetencyEvidence[0].$.lastUsed, skillTimeLimit ) ) {

    				oParsedCv.CurrentSkills.push( name );
    			}
    		}
    	}

    	if ( oStructured.EmploymentHistory ) {
    		var oEmplHistory = oStructured.EmploymentHistory,
    			iLastDate 	 = 0;

    		for ( var iIdx in oEmplHistory ) {
    			if ( !oEmplHistory[iIdx].EmployerOrg ) {
    				continue;
    			}
    			   	
			   	for ( var iIdx_eo in oEmplHistory[iIdx].EmployerOrg ) {

			   		if ( !oEmplHistory[iIdx].EmployerOrg[iIdx_eo].PositionHistory ) {
			   			continue;
			   		}

    				var oPosHistory = oEmplHistory[iIdx].EmployerOrg[iIdx_eo].PositionHistory;
    				for ( var iIdx2 in oPosHistory ) {

    					var iStartDate = 0,
    						iEndDate = 0;

    					if ( oPosHistory[iIdx2].StartDate
    					   && oPosHistory[iIdx2].StartDate[0] ) {
    						iStartDate = parseDate( oPosHistory[iIdx2].StartDate[0] );
    					}

    					if ( oPosHistory[iIdx2].EndDate
    					   && oPosHistory[iIdx2].EndDate[0] ) {
    						iEndDate = parseDate( oPosHistory[iIdx2].EndDate[0] );
    					}

    					if ( iStartDate && ( !oParsedCv.CareerStartDate || oParsedCv.CareerStartDate > iStartDate ) && iStartDate < getCurrentTime() ) {
    						oParsedCv.CareerStartDate = iStartDate;
    					}

    					if ( oPosHistory[iIdx2].OrgName
    					   && oPosHistory[iIdx2].OrgName[0] 
    					   && oPosHistory[iIdx2].OrgName[0].OrganizationName
    					   && oPosHistory[iIdx2].OrgName[0].OrganizationName[0] ) {
    						if ( oParsedCv.RawEmployers.indexOf( oPosHistory[iIdx2].OrgName[0].OrganizationName[0] ) == -1 ) {
    							oParsedCv.RawEmployers.push( oPosHistory[iIdx2].OrgName[0].OrganizationName[0] );
    						}

							if ( iEndDate > iLastDate 
							   && checkDate( iEndDate, employerTimeLimit ) ) {
								oParsedCv.CurrentEmployer = oPosHistory[iIdx2].OrgName[0].OrganizationName[0];
							}
    					}

    					if ( oPosHistory[iIdx2].Title
    					   && oPosHistory[iIdx2].Title[0] ) {
    						if ( oParsedCv.JobTitles.indexOf( oPosHistory[iIdx2].Title[0] ) == -1 ) {
    							oParsedCv.JobTitles.push( oPosHistory[iIdx2].Title[0] );
    						}

							if ( iEndDate > iLastDate ) {
								oParsedCv.LastKnownJobTitle = oPosHistory[iIdx2].Title[0];
								oParsedCv.CurrentJobTitle   = '';
								if ( checkDate( iEndDate, employerTimeLimit ) ) {
									oParsedCv.CurrentJobTitle = oParsedCv.LastKnownJobTitle;
								}
							}
    					}

    					if ( iEndDate > iLastDate ) {
							iLastDate = iEndDate;
						}
    				}
    			}
    		}
    	}

    	oParsedCv.Employers = oParsedCv.RawEmployers;

    	if ( oParsed.Resume.NonXMLResume
    	   && oParsed.Resume.NonXMLResume[0]
    	   && oParsed.Resume.NonXMLResume[0].TextResume
    	   && oParsed.Resume.NonXMLResume[0].TextResume[0] ) {
			
			oParsedCv.cv = oParsed.Resume.NonXMLResume[0].TextResume[0];
		} else {
			oParsedCv.cv = oCv.toString( 'utf8' );
		}

		if ( oParsedCv.CareerStartDate ) {
			oParsedCv.CareerStartDate = moment( oParsedCv.CareerStartDate ).format( 'YYYY/MM/DD' );
            if ( oParsedCv.CareerStartDate == 'Invalid date' ) oParsedCv.CareerStartDate = null;
		}

    	fCallback( oParsedCv, null );

	});
}