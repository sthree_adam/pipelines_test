function BulkLoad( oArgumentParser ){
    this.oArgumentParser = oArgumentParser;
    this.sBrand 		= oArgumentParser.getBrandCode();
    this.sStep 			= oArgumentParser.getArgumentValue('step') ? oArgumentParser.getArgumentValue('step') : 'start';
    this.sStop 			= oArgumentParser.getArgumentValue('stop');
    this.sStartDate     = oArgumentParser.getArgumentValueDate('startDate');
}

BulkLoad.prototype.getExistingJobBatchId = function( sBatchArgName ){
    if( !sBatchArgName ){
        return null;
    }

    var argumentValue = this.oArgumentParser.getArgumentValue(sBatchArgName);
    if( !argumentValue ){
        return null;
    }
    
    var aMatches = /(\w+),(\w+)/.exec( argumentValue );
    if( !aMatches || aMatches.length  != 3 ){
        throw Error("Wrong format for argument " + sBatchArgName + " should be jobId,batchId ( i.e. dfs323232,jk3j232323 )");
    }
    
    return [aMatches[1], aMatches[2]];
};

exports = module.exports = BulkLoad;