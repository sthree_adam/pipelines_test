var ArgumentsParser = function (validBulkLoads, validArgumentNames, steps, arguments) {
    this.validBulkLoads = validBulkLoads;
    this.validArgumentNames = validArgumentNames;
    this.arguments = arguments;
    this.steps = steps
}

ArgumentsParser.prototype.validate = function ( aArgumentsWithValues ) {
    var actualArguments = this.arguments.slice(2);

    if (this.validBulkLoads.indexOf(actualArguments[0]) === -1) {
        throw new Error("Invalid bulk load " + actualArguments[0]);
    }

    if (!actualArguments[1] || !actualArguments[1].match(/^\w\w$/)) {
        throw new Error("Invalid brand " + actualArguments[1]);
    }

    var extraArguments = actualArguments.slice(2); 
    var prevArg = false;
    var previousArgumentIsStep = false;
    for (var i in extraArguments) { 
        var argumentNameMatch = /^--(\w+)$/.exec(extraArguments[i]); 
        if (argumentNameMatch && this.validArgumentNames.indexOf(argumentNameMatch[1]) === -1) {
            throw new Error("Invalid argument name / parameter " + argumentNameMatch[1]);
        }
        if ( !prevArg && !argumentNameMatch ) {
            //must check if value ( without -- ) is preceeded by valid parameter with -- otherwise throw up
            throw new Error("Invalid argument name " + extraArguments[i] + " parameters need to be prefixed with -- ");   
        }
        if ( previousArgumentIsStep && this.steps.indexOf( extraArguments[i] ) === -1 ) {
            throw new Error("Invalid step " + extraArguments[i] );
        }
        previousArgumentIsStep = ( extraArguments[i] == '--stop' || extraArguments[i] == '--step' );
        prevArg = argumentNameMatch;
    }

    for(var i in aArgumentsWithValues){
        this.getArgumentValue(aArgumentsWithValues[i]);
    }

}

ArgumentsParser.prototype.getArgumentValue = function (sArgument) {
    var actualArguments = this.arguments.slice(2);
    for (var i in actualArguments) {
        if( actualArguments[i].indexOf( sArgument ) !== -1 ){
            if (!actualArguments[parseInt(i) + 1] || /^--\w+$/.test(actualArguments[parseInt(i) + 1])) {
                throw new Error( "Missing argument value for " + sArgument );
            }
            return actualArguments[parseInt(i) + 1];
        }
    }
    return null;
};

ArgumentsParser.prototype.getArgumentValueDate = function(sArgument){
    var sDate       = this.getArgumentValue( sArgument );
    if( sDate ){
        sDate = sDate + "T00:00:00.000Z";
    }
    return sDate;
}

ArgumentsParser.prototype.argumentExists = function (sArgument) {
    return this.arguments.indexOf( "--" + sArgument ) !== -1;
}

ArgumentsParser.prototype.usage = function(){
    return "\nUSAGE: node bulkload.js <type (job|manager|candidate|business)> <brand (OT, EP, etc.)> <params>\n\nParams:\n" +
           "\n\t--getSteps - display list of available steps" +
           "\n\t--step        <step name DEFAULT null>" +
           "\n\t--stop        <step name DEFAULT null>" +
           "\n\t--startDate   <LastModifiedDate (YYYY-MM-DD) used in salesforce queries DEFAULT null - process everything>"+
           "\n\t--objectBatch <objecJobId,objectBatchId (i.e. 7508E000000QgYl,75124000003QHjM) if specified it uses this jobId and batchId and skips salesforce query>" +
           "\n\t--topicBatch  <topicJobId,topicBatchId (i.e. 7508E000000QgYl,75124000003QHjM) if specified it uses this jobId and batchId and skips salesforce query>" +
           "\n\t--topicAssignmentBatch  <topicAssignmentJobId,topicAssignmentBatchId (i.e. 7508E000000QgYl,75124000003QHjM) if specified it uses this jobId and batchId and skips salesforce query>" +
           "\n\t--jobAddressBatch  <jobAddressJobId,jobAddressBatchId (i.e. 7508E000000QgYl,75124000003QHjM) if specified it uses this jobId and batchId and skips salesforce query>" +
           "\n\t--addressBatch  <addressJobId,addressBatchId (i.e. 7508E000000QgYl,75124000003QHjM) if specified it uses this jobId and batchId and skips salesforce query>\n";
    
}

ArgumentsParser.prototype.getBulkLoadType = function(){
    return this.arguments[2];
}

ArgumentsParser.prototype.getBrandCode = function(){
    return this.arguments[3];
}

exports = module.exports = ArgumentsParser;