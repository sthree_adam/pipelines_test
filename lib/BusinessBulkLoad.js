var Promise 		= require( 'promise' );
var Moment          = require( 'moment' );
var BulkLoad        = require( __dirname + '/BulkLoad' );

var BusinessBulkLoad = function( oLookingGlass, oMongoServer, oRabbit, oConfig, oArgumentParser ) {
    BulkLoad.call( this, oArgumentParser );
    
	this.oLookingGlass 	= oLookingGlass;
	this.oCodes 		= {};
	this.oMongoServer	= oMongoServer;
	this.currentEntityId = '';
	this.aCodeCache		= [];
	this.oBusinessAddress	= {};
	this.sCollection    = oConfig.get( 'salesforce' ).collections[ this.type ] + this.sBrand;
	this.sQueue    		= oConfig.get( 'salesforce' ).queues[ this.type ] + this.sBrand;
	this.oRabbit		= oRabbit;
}

BusinessBulkLoad.prototype = Object.create( BulkLoad.prototype );

BusinessBulkLoad.prototype.type = 'business';

BusinessBulkLoad.prototype.init = function( fCallback ) {
	var that = this;
	this.oMongoServer.connect( 'cache', function( error ) {
		if ( error ) {
			fCallback( error );
			return;
		};
		that.oRabbit.connect( function( error ) {
			if ( error ) {
				fCallback( error );
			}
			that.oRabbit.initQueues( that.sQueue, function() {
				fCallback();
			} );
		} );
	} );
}

BusinessBulkLoad.prototype.waitForCompletion = function( fCallback ) {
    this.oRabbit.waitForMessagesQueued( fCallback );
};

BusinessBulkLoad.prototype.GetSteps = function() {
	var that = this;
	var getQueueSize = function() {
		return [ that.oMongoServer.oQueue.getQueueLength(), that.oMongoServer.oQueue.getPendingLength(), 'n/a' ];
	}

	var aSteps =  [	
/**********************************************************************************************/
				{ name		: 'start'
				, source	: 'custom'
				, onRecord 	: function( fCallback ) {
						that.oMongoServer.deleteCollection( 'cache', that.sCollection );
						fCallback(); 
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryObject'
				, query 	: (function(){
									var sQuery = 'SELECT Id, Name, Brand__c, Status__c, Industry, Tier__c FROM Account WHERE Brand__c = \'' + that.sBrand + '\' AND Is_Dummy_Business_For_Candidate__c = false ';
									if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate;
									return sQuery;
								})()
			    , object    : 'Account'
                , fileName  : 'Account' + that.sBrand
                , batchArgName: 'objectBatch'
                , source	: 'salesforce'
				, onRecord	: function( oDetails ) {
	                    if ( oDetails && oDetails.Id ) {
							that.oMongoServer.AddToQueue( oDetails, 'cache', that.sCollection );
						}
	            	}
				, queueSize : function() {
						return getQueueSize();
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryObjectEnd'
				, source	: 'custom'
				, onRecord 	: function() {
						that.oMongoServer.addIndex( 'cache', that.sCollection, 'Id' );
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryTopic'
			    , query 	: 'SELECT Id, Name FROM Topic'
                , batchArgName: 'topicBatch'
	            , object    : 'Topic'
                , fileName  : 'Topic' + that.sBrand
                , source	: 'salesforce'
				, onRecord	: function( oCode ) {
						that.oCodes[oCode.Id] = oCode.Name 
					}
				, queueSize : function() {
						return getQueueSize();
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryTA'
				, query 	: (function(){
								var sQuery = 'SELECT EntityId, TopicId FROM TopicAssignment ' +
									          'WHERE EntityType = \'Account\' '+
									          'AND EntityId IN ( '+ 
									          	 'SELECT Id '+
									          	 'FROM Account WHERE Brand__c = \'' + that.sBrand + '\' AND Is_Dummy_Business_For_Candidate__c = false ' ;
												 if( that.sStartDate ) sQuery += 'AND LastModifiedDate >= ' + that.sStartDate + ' ';
									          	 sQuery += ') ';
								
							    sQuery += 'ORDER BY EntityId';
							    return sQuery;
							   })()
			    , object    : 'TopicAssignment'
                , batchArgName: 'topicAssignmentBatch'
                , fileName  : 'TopicAssignmentAccount' + that.sBrand
                , source	: 'salesforce'
				, onRecord 	: function( oAssignment ) {
						if ( that.currentEntityId != '' && oAssignment.EntityId != that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
							that.aCodeCache = [];
						}
						
						that.currentEntityId = oAssignment.EntityId;
						that.aCodeCache.push( that.oCodes[oAssignment.TopicId] );
					}
				, queueSize : function() {
						return getQueueSize();
					}
				}
/**********************************************************************************************/
			,	{ name		: 'flushTACache'
				, source	: 'custom'
				, onRecord 	: function() {
						if ( that.currentEntityId ) {
							that.oMongoServer.AddAssignmentToQueue( that.currentEntityId, that.aCodeCache, 'cache', that.sCollection );
						}
					}
				}
/**********************************************************************************************/
			,	{ name		: 'clearMemory'
				, source	: 'custom'
				, onRecord 	: function() {
						that.oCodes = {};
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryBusinessAddress'
				, query 	: (function(){
									var sQuery = 'SELECT Account__c, Address__c FROM Account_Address__c WHERE Is_Primary__c = true AND Brand__c = \'' + that.sBrand + '\' ';
									if( that.sStartDate ) sQuery += 'AND Account__c IN (SELECT Id FROM Account WHERE LastModifiedDate >= ' + that.sStartDate + ' AND Brand__c = \'' + that.sBrand + '\' AND Is_Dummy_Business_For_Candidate__c = false )';
									return sQuery;
								})()
			    , object    : 'Account_Address__c'
                , fileName  : 'Business_Address' + that.sBrand
                , source	: 'salesforce'
                , batchArgName: 'jobAddressBatch'
				, onRecord	: function( oBusinessAddress ) {
						if ( !that.oBusinessAddress[oBusinessAddress.Address__c] ) {
							that.oBusinessAddress[oBusinessAddress.Address__c] = [];
						}
						that.oBusinessAddress[oBusinessAddress.Address__c].push( oBusinessAddress.Account__c );
					}
				, queueSize : function() {
						return getQueueSize();
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queryAddress'
				, query 	: 'SELECT Id, Country__c, County__c, Postcode_Location__Latitude__s, Postcode_Location__Longitude__s, Postcode__c FROM Address__c WHERE Id IN ( SELECT Address__c FROM Account_Address__c WHERE Is_Primary__c = true AND Brand__c = \'' + that.sBrand + '\')'
			    , object    : 'Address__c'
                , fileName  : 'Address' + that.sBrand
                , batchArgName: 'addressBatch'
                , source	: 'salesforce'
				, onRecord 	: function( oAddress ) {
						if( that.oBusinessAddress[oAddress.Id] ){
							that.oMongoServer.AddAddressToQueue( oAddress, that.oBusinessAddress[oAddress.Id], 'cache', that.sCollection );
						}						
					}
				, queueSize : function() {
						return getQueueSize();
					}
				}
/**********************************************************************************************/
			,	{ name		: 'clearMemoryJob'
				, source	: 'custom'
				, onRecord 	: function() {
						that.oBusinessAddress = {};
					}
				}
/**********************************************************************************************/
			,	{ name		: 'queue'
				, source	: 'custom'
				, onRecord 	: function( fCallback ) {
						that.oRabbit.purgeQueue( that.sQueue, function() { fCallback(); } );
					}
				}
/**********************************************************************************************/
			,	{ name		: 'requeue'
				, query 	: {}
			    , object    : 'business'
                , source	: 'mongo'
                , brand 	: that.sBrand
                , collection: that.sCollection
				, onRecord 	: function( aBusinesses, fDone ) {
						that.oRabbit.addMessage( aBusinesses, that.sQueue, fDone );
					}
				}
/**********************************************************************************************/
			,	{ name		: 'process'
				, query 	: {}
			    , object    : 'business'
                , source	: 'rabbitmongo'
				, queue 	: that.sQueue
                , collection: that.sCollection
				, onRecord 	: function( aBusinesses, fCallback ) {
						that.oLookingGlass.AddToQueue( aBusinesses, that, fCallback );
					}
				, queueSize : function() {
						return [ that.oLookingGlass.oQueue.getQueueLength(), that.oLookingGlass.oQueue.getPendingLength(), 'n/a' ];
					}
				}
/**********************************************************************************************/
			];

	var aToExecute = [], bAddSteps = false;
    for ( var i = 0; i < aSteps.length; i++ ) {
        if ( aSteps[i].name == this.sStep ) {
            bAddSteps = true;
        }

        if ( aSteps[i].name == this.sStop ) {
            bAddSteps = false;
        }

        if ( bAddSteps && !( aSteps[i].skip && aSteps[i].name != this.sStep ) ) {
            aToExecute.push( aSteps[i] );
        }
    }

    return aToExecute;
};

var GetLookingglassDate = function( sInput ) {
	if( !sInput || isNaN( Date.parse( sInput ) ) ) {
		return null;
	}
	return Moment(new Date(sInput)).format('YYYY/MM/DD');
}

var ParseConsultants = function( oInputRecord ) {
	var aConsultants = [];
	if( oInputRecord.Associated_Users__c ) {
		aConsultants = oInputRecord.Associated_Users__c.split( ',' );
	}
	if( oInputRecord.Owner_Alias__c ) {
		aConsultants.push( oInputRecord.Owner_Alias__c );
	}

	var aNormalisedConsultants = [];
	aConsultants.forEach( function( sConsultantCode ) {
		var sCode = sConsultantCode.toUpperCase().trim();
		if( sCode.length > 0 && aNormalisedConsultants.indexOf( sCode ) == -1 ) {
			aNormalisedConsultants.push( sCode );
		}
	} );
	return aNormalisedConsultants;
};

BusinessBulkLoad.prototype.GetLookingGlassRecord = function( oBusinessDetails, fCallback ) {
	
	var oParams 	= {
		  id 				: oBusinessDetails.Id
		, brand_code		: oBusinessDetails.Brand__c
		, name      		: oBusinessDetails.Name
		, status			: oBusinessDetails.Status__c
		, country 			: (oBusinessDetails.address) ? oBusinessDetails.address.Country__c  : null
		, postcode 			: (oBusinessDetails.address) ? oBusinessDetails.address.Postcode__c : null		
		, county            : (oBusinessDetails.address) ? oBusinessDetails.address.County__c : null
		, codes				: oBusinessDetails.codes
		, tier				: oBusinessDetails.Tier__c
		, industry			: oBusinessDetails.Industry
	};

	if( oBusinessDetails.address && oBusinessDetails.address.Postcode_Location__Latitude__s && oBusinessDetails.address.Postcode_Location__Longitude__s ) {
		oParams.location = oBusinessDetails.address.Postcode_Location__Latitude__s + ',' + oBusinessDetails.address.Postcode_Location__Longitude__s;
	}

	fCallback( oParams );
};

BusinessBulkLoad.prototype.AllLoaded = function() {
	this.oLookingGlass.onAllIndexed();
};

exports = module.exports = BusinessBulkLoad;