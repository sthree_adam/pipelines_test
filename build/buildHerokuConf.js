var env = process.argv[2],
	app = process.argv[3],
	update = process.argv[4],
	confFile,
	fs = require( 'fs' );

if ( !app || !env ) {
	console.log( 'USAGE:' );
	console.log( 'node buildHerocuConf env app [m - merge configs]' );
	console.log( 'Example:' );
	console.log( 'node buildHerocuConf d2 cao m' );
	process.exit(1);
}

if ( fs.existsSync( './conf/' + env + '.json' ) ) {
	confFile = './conf/' + env + '.json';
} else {
	confFile = './conf/ci.json';
	console.log( '\n\nNO CONFIGURATION FOR ' + env + ' FOUND! USING ci INSTEAD!\n\n' );
}
var oEnvConfig  = require( confFile );

var Heroku = require( 'heroku-client' ),
	conf   = require( './conf/deployConf.json' ),
    heroku = new Heroku( conf.heroku );

var getEntries = function( conf, node, result ) {
	var text = '', e;

	if ( typeof conf == 'object' ) {
		for ( e in conf ) {
			text = ( node ) ? node + ':' + e : e;
			getEntries( conf[e], text, result );
		}
	} else {
		result[node] = conf;
	}

	return;
};

var saveVars = function( vars ) {
	
	var newVars = {};
	getEntries( oEnvConfig, '', newVars );

	if ( update && update == 'm' ) {
		for ( var confV in newVars ) {
			if ( vars[confV] !== undefined ) {
				newVars[confV] = vars[confV];
			}
		}
	}

	heroku.apps( env + '-' + app ).configVars().update( newVars, function( error ) {
		if ( error ) {
			console.log( error );
		} else {
			console.log( 'Config updated.' );
			console.log( newVars );
		}
	});	
};

heroku.apps( env + '-' + app ).configVars().info( function ( err, vars ) {
	saveVars( vars );
});