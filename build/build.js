var fs = require( 'fs' );
var http = require( 'http' );
var saveFiles = true;
var outputTime = 0;
var outputTimeout = 6 * 60000; // max 6 minutes without any output
var envRoot = 'bull';

if ( process.argv[2] && process.argv[2] == 'heroku' ) {
	//saveFiles = false;
	envRoot = 'bull/heroku';
}

var options 	= 	{ host: 'PC-8005-W8'
					, port: 10101
					, path: '/' + envRoot + '/buildandruntests'
					, headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 5.1; rv:29.0) Gecko/20100101 Firefox/29.0' }
					};

console.log( 'Running build...' );

var getResults = function() {
	if ( !saveFiles ) return;

	console.log( 'getting results...' );
	options.path = '/' + envRoot + '/getresults';
	http.get( options, function( resp ) {
		var all_data = '';

		resp.on( 'data', function ( chunk ) {
			all_data += chunk.toString();
		})
		.on('end', function () {
			var logs;

			try {
			 	logs = JSON.parse( all_data.trim() );
			} catch( e ) {
			 	console.log( e.message );
			 	return;
			}

			for ( var log in logs ) {
			 	fs.writeFileSync( __dirname + '/logs/' + log, logs[log] );
			}
			
			console.log( 'files saved' );
			
		});
	});
};

var getOutput = function() {
	options.path = '/' + envRoot + '/getoutput';
	http.get( options, function( resp ) {
		var all_data = '';

		resp.on( 'data', function ( chunk ) {
			all_data += chunk.toString();
		})
		.on('end', function () {
			var data = all_data.trim();
			if ( data == '-- END OF OUTPUT --' ) {
				getResults();
				return;
			}
			
			if ( data != '' ) {
				outputTime = Date.now();
				console.log( data );
			} else {
				if ( Date.now() - outputTime > outputTimeout ) {
					console.log( '--- TIMEOUT ---' );
					return;
				}
			}

			setTimeout( getOutput, 2000 );
		});
	});
};

http.get(options, function(resp) {
	
	  var all_data = '';
	  
	  resp.on('data', function (chunk) {
		
		all_data += chunk.toString();
	  
	  } ).on('end', function () {

	  	console.log( all_data.trim() );
	  	outputTime = Date.now();
		getOutput();

	  });
	  
});
