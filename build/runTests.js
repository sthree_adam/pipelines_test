var fs = require( 'fs' );
var spawn = require('child_process').exec;
var env = process.argv[2];
var output = '';	
var nodeExec = ( process.argv[0] != 'node' && process.argv[0] != 'nodejs' ) ? 'node' : process.argv[0];
var job = spawn( nodeExec + ' herokuRun "cd /app && npm test && node tests/results/getResults.js" ' + env );

var regexMatch = function( sRegexp, sString  ) {
	var res = [];
	var match = sRegexp.exec( sString );
	while ( match != null ) {

		res.push( match );
	    match = sRegexp.exec( sString );
	}

	return res;
}

var getResults = function() {
	var results = {};
	try {
		var fullOutput = fs.readFileSync( __dirname + '/logs/tests.txt' ).toString();
	} catch (e) {
		console.log( 'no test results found');
		return;
	}

	var output = fullOutput.substr( fullOutput.indexOf( '\n' ) );

	var rxSearch = /---(.*?\.xml)---([\s\S]*?)---\/(.*?.xml)---/gmi
	var res = regexMatch( rxSearch, output );
	var result = '';

	for ( var idx in res ) {
		result = new Buffer( res[idx][2].trim(), 'base64' ).toString( 'ascii' ).trim();
		fs.writeFileSync( __dirname + '/logs/' + res[idx][1], result );
		fullOutput = fullOutput.replace( res[idx][0], '' );
	}

	console.log( fullOutput );
}

console.log( 'testing started' );

job.stdout.on('data', function (data) {
  output += data.toString();
});

job.stderr.on('data', function (data) {
	output += data.toString();
});

job.on('close', function (code) {
	fs.writeFileSync( __dirname + '/logs/tests.txt', output );  
	getResults();
});

job.on('error', function (error) {
  console.log( error );
});


