@ECHO OFF

IF "%1" == "" (
	echo "usage: deployHeroku.bat [env] [runTests=no] [update=no]"
	goto :end
)

call npm install
call node deployHeroku %*

:end
