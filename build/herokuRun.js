var Heroku  = require('heroku-client'),
    conf    = require( './conf/deployConf.json' ),
    heroku  = new Heroku( conf.heroku ),
    tls     = require('tls')
    url     = require( 'url' ),
    cmd     = process.argv[2],
    app     = process.argv[3];

if ( !cmd || !app ) {
    console.log( 'Usage:\nnode herokuRun "command" appname' );
    process.exit( 1 );
}

heroku.apps( app ).dynos().create( { "attach": true
                                        , "command": cmd
                                        , "env": { "COLUMNS": "80"
                                                 , "LINES": "24"
                                                 }
                                        , "size": "standard-1X" }
                                        , function( err, info ) {                     
                                            if ( err ) {
                                                console.log( err );
                                                process.exit( 1 );
                                            }
                                            console.log( 'One-off dyno created' );
                                            getOutput( info['attach_url'] );
                                        } );

var getOutput = function( sUrl ) {

    var oUrl = url.parse( sUrl );

    var socket = tls.connect( oUrl.port, oUrl.hostname, {}, function() {
        console.log( 'client connected',
                    socket.authorized ? 'authorized' : 'unauthorized');
        socket.write( oUrl.path.replace( '/', '' ) );
    });
    socket.setEncoding('utf8');

    socket.on('data', function(data) {
        process.stdout.write( data.toString() );
    });
    socket.on('end', function() {
        
    });
}