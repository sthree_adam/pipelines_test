var fs 			= require('fs-extended'),
	path    	= require('path');
	exec 		= require('child_process').exec,
	Heroku 		= require( 'heroku-client' ),
	conf 		= require( './conf/deployConf.json' ),
    heroku 		= new Heroku( conf.heroku ),
    baseDir 	= __dirname + path.sep + '..' + path.sep + conf.app.baseFolder,
    herokuDir 	= __dirname + path.sep + '..' + path.sep + '.herokuDeploy' + path.sep,
    herokuGit 	= 'https://user:' + conf.heroku.token + '@git.heroku.com/',
    env 		= process.argv[2],
    appName		= conf.app.name;
	tests 		= ( process.argv[3] && process.argv[3] == 'yes' ) ? true : false,
	update 		= ( process.argv[4] && process.argv[4] == 'yes' ) ? true : false,
	tag 		= '',
	herokuEnv   = env + '-' + appName,
	nodeExec	= ( process.argv[0] != 'node' && process.argv[0] != 'nodejs' ) ? 'node' : process.argv[0];

if ( !env ) {
	console.log( 'USAGE:' );
	console.log( 'usage: node deployHeroku.js [env] [runTests=no] [update=no]' );
	console.log( 'Example:' );
	console.log( 'node buildHerocuConf d2 no yes' );
	process.exit(1);
}

/* fix for removing read-only files */
fs.emptyDirSync = function (dir) {
	var files, file, filePath;

	try {
		files = fs.readdirSync(dir);
	} catch (err) {
		if (err.code === 'ENOENT') {
			return fs.createDirSync(dir);
		} else {
			throw err;
		}
	}

	for (var i = 0, l = files.length; i < l; i++) {
		file = files[i];
		filePath = path.join(dir, file);
		if (fs.statSync(filePath).isFile()) {
			fs.chmodSync( filePath, 0777 );
			fs.unlinkSync(filePath);
		} else {
			fs.deleteDirSync(filePath);
		}
	}
};

var printOutput = function( error, stdout, stderr, exit ) {
	if ( typeof exit == 'undefined' ) exit = true;
	if ( error ) {
		console.log( 'stderr: ' + stderr );
		console.log( error );
		if ( exit ) process.exit(1);
	}
	console.log( stdout );
}

var prepareFolders = function() {

	console.log( 'Create temp folder' );
	try {
		fs.deleteDirSync( herokuDir );
	} catch( e ) {
		console.log( 'unable to remove ' + herokuDir );
		console.log( e );
	}
	fs.createDirSync( herokuDir );
	var aClean = ( tests ) ? conf.app.ignoreFoldersTest : conf.app.ignoreFoldersDeploy;

	var files = fs.listFilesSync( baseDir, {
		recursive: true,
		filter: function( sName ) {
			var bOk = true;
			aClean.forEach( function( sExclName ) {
				sName += path.sep;
				if ( sName.indexOf( path.sep + sExclName + path.sep ) != -1 ) {
					bOk = false;
				}
			} );
			return bOk;
		}

	});

	files.forEach( function( sName ) {
		fs.copyFileSync( baseDir + sName, herokuDir + sName );
	});

	createGitRepo();
};

var createGitRepo = function() {
	console.log( 'Create temp repo' );
	exec( 'git describe --tags'
		, { 'cwd': baseDir }
		, function ( error, stdout, stderr ) {
			tag = stdout.trim();
			printOutput( error, stdout, stderr );

			var d = new Date();
			fs.writeFileSync( herokuDir + conf.app.webrootFolder + path.sep + 'version.txt'
							, tag + ' ' + d.getFullYear() + '-' + d.getDate() + '-' +  ( d.getMonth() + 1 ) + '-' + d.getHours() + '-' + d.getMinutes() );
		  	
		  	exec( 'git init .'
				, { 'cwd': herokuDir }
				, function ( error, stdout, stderr ) {
					printOutput( error, stdout, stderr );
					
					console.log( 'Set git user name' );

					exec( 'git config user.email "gitsdevser@itr1.co.uk" && git config user.name "Deployment Script"'
						, { 'cwd': herokuDir }
						, function ( error, stdout, stderr ) {
							printOutput( error, stdout, stderr );

							exec( 'git add *'
								, { 'cwd': herokuDir }
								, function ( error, stdout, stderr ) {
									printOutput( error, stdout, stderr );

									exec( 'git commit -m "CI build"'
										, { 'cwd': herokuDir }
										, function ( error, stdout, stderr ) {
											printOutput( error, stdout, stderr );

										  	exec( 'git remote add heroku ' + herokuGit + herokuEnv + '.git'
												, { 'cwd': herokuDir }
												, function ( error, stdout, stderr ) {
													printOutput( error, stdout, stderr );
													console.log( 'Heroku remote has been added' );
													addHerokuApp();
												} 
											);
										  	
										} 
									);
								  	
								} 
							);
						  }
					);	
				} 
			);
		  	
		} 
	);
};


var addHerokuApp = function() { 
	console.log( 'Prepare heroku app' );

	if ( !update ) {
		console.log( 'Deleting ' + herokuEnv );
		heroku.apps( herokuEnv ).delete( function( error ) {
			//printOutput( error, '', '', false );
			if ( !error ) {
				console.log( 'App ' + herokuEnv + ' has been deleted' );
			}
			console.log( 'Creating ' + herokuEnv );
			setTimeout( function() { createApp(); }, 10000 );
		} );
	} else {
		heroku.apps( herokuEnv ).info( function( err, info ) {
			if ( err ) printOutput( err, 'Application Error', '' );
			deployToHeroku();
		} ) ;
	}

}

var createApp = function() {
	heroku.organizations().apps().create( { name: herokuEnv, organization: 'sthree', personal: false }, function( error, app ) {
		printOutput( error, '', '' ); 
		console.log( 'Heroku app created: https://' + herokuEnv + '.herokuapp.com' );
		deployToHeroku();
	} );
}

var deployToHeroku = function() {
	console.log( 'deploying to heroku' );
	var mode = ( update ) ? 'm' : '';
	exec( nodeExec + ' buildHerokuConf.js ' + env + ' ' + appName + ' ' + mode
		, { 'cwd': __dirname }
		, function ( error, stdout, stderr ) {
			printOutput( error, stdout, stderr );
			console.log( 'pushing changes to heroku' );

			var job = exec( 'git push heroku master -f -v'
						  , { 'cwd': herokuDir, maxBuffer: 5000 * 1024 } );

			job.stdout.on('data', function (data) {
			  process.stdout.write( data.toString() );
			});

			job.stderr.on('data', function (data) {
				process.stdout.write( data.toString() );
			});

			job.on('close', function (code) {
				printOutput( null, 'done with exit code: ' + code, '' );
				if ( code == 0 ) cleanUp();
			});

			job.on('error', function (error) {
			  printOutput( error, '', '' );
			});
		  }
		);
}

var cleanUp = function() {
	console.log( 'removing temp folder' );
	try {
		fs.deleteDirSync( herokuDir );
	} catch( e ) {
		console.log( 'unable to remove ' + herokuDir );
	}
	if ( tests ) {
		runTests();
		return;
	}
	console.log( 'build done.' );
}

var runTests = function() {
	if ( !tests ) return;
	console.log( 'running tests' );
	try {
		fs.unlinkSync( path.join( __dirname, 'logs', 'tests.txt' ) );
		fs.unlinkSync( path.join( __dirname, 'logs', 'junit.xml' ) );
		fs.unlinkSync( path.join( __dirname, 'logs', 'clover.xml' ) );
	} catch( e ) {
		
	}

	var job = exec( nodeExec + ' runTests.js ' + herokuEnv
				  , { 'cwd': __dirname
				  	, maxBuffer: 5000 * 1024 
				  	} 
				  );

		job.stdout.on('data', function (data) {
		  console.log( data.toString() );
		});

		job.stderr.on('data', function (data) {
			console.log( data.toString() );
		});

		job.on('close', function (code) {
			printOutput( null, 'tests finished, scaling dynos...', '' );

			heroku.apps( herokuEnv ).formation().batchUpdate( { "updates": [ { "process":"web", "quantity":0 } ] }
				, function( err, resp ) {
					printOutput( err, '', '' );
					console.log( 'now running web at ' + resp[0]['quantity'] + ':' + resp[0]['size'] );
					console.log( 'build done.' );
				});
		});

		job.on('error', function (error) {
		  printOutput( error, '', '' );
		});
}

prepareFolders();
