var express         = require( 'express' ),
	nconf           = require( 'nconf' ).env(),
	app             = express();
//something to commit
app.use( express.static( __dirname + '/public' ) );
app.get( '/'
	, function( req, res ) {
		res.set( 'Content-Type', 'text/plain; charset=utf-8' )
		   .status( 200 )
		   .send( 'Nothing here!' );
	} );
	
app.listen( nconf.get( 'PORT' ), function() {
	console.log( 'Listening on ' + nconf.get( 'PORT' ) );
} );
