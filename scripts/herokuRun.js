var Heroku  = require('heroku-client'),
    conf    = require( __dirname + '/../conf/runConf.json' ),
    heroku  = new Heroku( conf.heroku ),
    tls     = require('tls')
    url     = require( 'url' ),
    cmd     = process.argv[2],
    app     = process.argv[3],
    size    = ( process.argv[4] ) ? process.argv[4] : 'standard-1X',
    dyno    = '';

if ( !cmd || !app ) {
    console.log( 'Usage:\nnode herokuRun ["command"] [appname] [size DEFAULT standard-1X]' );
    process.exit( 1 );
}

heroku.apps( app ).dynos().create( { "attach": true
                                        , "command": cmd
                                        , "env": { "COLUMNS": "80"
                                                 , "LINES": "24"
                                                 }
                                        , "size": size }
                                        , function( err, info ) {                     
                                            if ( err ) {
                                                console.log( err );
                                                process.exit( 1 );
                                            }
                                            console.log( 'One-off dyno created ' + info.name );
                                            
                                            dyno = info.id;
                                            getOutput( info['attach_url'] );
                                        } );

var getOutput = function( sUrl ) {

    console.log( 'Attaching output: ' + sUrl );

    var oUrl = url.parse( sUrl );

    var socket = tls.connect( oUrl.port, oUrl.hostname, {}, function() {
        console.log( 'output attached',
                    socket.authorized ? 'authorized' : 'unauthorized');
        socket.setTimeout( conf.timeout * 1000, checkTimeout );
        socket.write( oUrl.path.replace( '/', '' ) );
    } );

    socket.setEncoding('utf8');

    socket.on( 'error', function( error ) {
        console.log( error );
        process.exit( 1 );
    } );

    socket.on( 'data', function( data ) {
        process.stdout.write( data.toString() );
    } );

    socket.on( 'end', function( xx ) {
        console.log( xx );
            heroku.apps( app ).dynos( dyno ).info( function( err, info ) {
                console.log( 'dyno state: ' + info.state );
            } );
    } );

    var checkTimeout = function() {
        console.log( 'no output after ' + conf.timeout + 's' );
        heroku.apps( app ).dynos( dyno ).info( function( err, info ) {
            if ( err ) {
                console.log( 'error: dyno does not exist' );
                socket.destroy();
                process.exit( 1 );
            }

            if ( info.state != 'up' ) {
                console.log( 'dyno is not running' );
                socket.destroy();
                process.exit( 1 );
            }

            console.log( 'dyno is running' );
            socket.setTimeout( conf.timeout * 1000, checkTimeout );
        } );
    }
}