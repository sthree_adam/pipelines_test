var BulkLoadFactory = require( '../lib/BulkLoadFactory' );
var ArgumentParser  = require( '../lib/ArgumentsParser' );

var oArgumentParser 
    = new ArgumentParser(
          [ 'job' , 'manager' , 'candidate' , 'business' ]
        , [ 'step' ,  'stop' ,  'startDate', 'getSteps', 'objectBatch', 'topicBatch', 'topicAssignmentBatch', 'jobAddressBatch', 'addressBatch' ]
        , [ 'start' ,  'queryObject' ,  'queryTopic', 'queryJobAddress', 'queryBusinessAddress', 'queue', 'process' ]
        , process.argv
    );

if ( oArgumentParser.argumentExists( 'getSteps' ) ) {
    console.log("\nAvailable steps: \n" +
        "\n\tstart            - clear mongo cache and rabbit queue" +
        "\n\tqueryObject      - cache object data from SF" +
        "\n\tqueryTopic       - cache codes from SF" +
        "\n\tqueryJobAddress/queryBusinessAddress  - cache job address from SF (only for jobs or businesses)" +
        "\n\tqueue            - clear queue and add data to RabbitMQ " +
        "\n\t                   (needs to be called directly otherwise it's skipped)" +
        "\n\tprocess          - load cached data into LG\n" );
    process.exit(0);
}

try {
    oArgumentParser.validate(['step', 'stop', 'startDate', 'objectBatch', 'topicBatch', 'topicAssignmentBatch', 'jobAddressBatch', 'addressBatch']);
}catch(e){
    console.log(e.message);
    console.log( oArgumentParser.usage() );
    process.exit(1);
}

var oBulkLoadFactory = new BulkLoadFactory( __dirname + '/../conf/config.json' );
var oBulkApi         = oBulkLoadFactory.newBulkApi();
var oLogger          = oBulkLoadFactory.getLogger();

var oBulkLoad = oBulkLoadFactory.newBulkLoad( oArgumentParser );

oLogger.info( 'Start Bulk Load' );

var oMemDump = setInterval( function() { oLogger.logMemStats(); }, 10000 );

oBulkApi.login()
	.then( function() { return oBulkApi.process( oBulkLoad ); } )
	.then( function() {
			oLogger.info( 'All data loaded from Salesforce' );
            clearInterval( oMemDump );
			oLogger.logMem();
			oBulkLoad.waitForCompletion( function() { 
				setTimeout(function() {
					process.exit(0);	
				}, 20000);
			} );
			
			}
		 , oLogger.error 
		 );